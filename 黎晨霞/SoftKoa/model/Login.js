'use strict'
const {DataTypes}=require('sequelize');
let model={
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    digest:{
        type:DataTypes.STRING,
        allowNull:false
    },
    details:{
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    ptime:{
        type:DataTypes.TIME,
        allowNull:false
    }
}
module.exports=model;