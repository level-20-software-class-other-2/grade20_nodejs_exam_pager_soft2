const router = require('koa-router')()
const fs = require('fs')

let gen = __dirname

function sumFile(gen) {
    let file = fs.readdirSync(gen)
    let files = file.filter(item => {
        return item.endsWith('.js') && item != 'router.js'
    })
    return files
}

function urlPath(files) {
    files.filter(item => {
        let obj = require(__dirname + '/' + item)
        for (let key in obj) {
            let arr = key.split(' ')
            if (arr[0] == 'get') {
                router.get(arr[1], obj[key])
            } else if (arr[0] == 'post') {
                router.post(arr[1], obj[key])
            } else if (arr[0] == 'delete') {
                router.delete(arr[1], obj[key])
            }
            else if (arr[0] == 'put') {
                router.put(arr[1], obj[key])
            }

        }
    })
}

let fi = sumFile(gen)
urlPath(fi)
module.exports = router