const Koa=require('koa');
const routerMiddleWare=require('./router');
const bodyParser=require('koa-bodyparser');
const cors=require('koa-cors');
const{sync}=require('./model');

let app=new Koa();

app.use(routerMiddleWare());
app.use(bodyParser());
app.use(cors());

sync();

let port=8000;
app.listen(port);
console.log(`此应用运行在:http://localhost:${port}`);

