'use strict'

//导入koa
const koa = require('koa');
//导入使用路由
const router = require('koa-router')();
//导入koa-bodyparser模块
const bodyparser =require('koa-bodyparser');
//跨域
const cors = require('koa-cors');

//引入sequelize
const {Sequelize , Op , DataTypes}= require('sequelize');

//配置连接
const sequelize = new Sequelize("soft2","postgres","113",{
    host:"www.zrkeji.top",
    dialect:"postgres"
})
//模型实例
let blog=sequelize.define('blog',{
    title:{
        type:DataTypes.STRING
    },
    abstract:{
        type:DataTypes.STRING
    },
    content:{
        type:DataTypes.STRING
    },
    classify:{
        type:DataTypes.STRING
    },
    author:{
        type:DataTypes.STRING
    },
    dateline:{
        type:DataTypes.STRING
    },
});
//同步实例
sequelize.sync({force:true}).then(()=>{
    blog.bulkCreate([
        {
            title:"论EF Core的自我修养",
            abstract:"论EF Core的自我修养",
            content:"论EF Core的自我修养",
            classify:".Net",
            author:"InCerry",
            dateline:"2022-04-06 8：47"
        },
        {
            title:"DDD之我见",
            abstract:"DDD之我见",
            content:"DDD之我见",
            classify:"编程技术",
            author:"莫大神",
            dateline:"2022-04-03 23：47"
        },
        {
            title:"nginx负载平衡的几种策略",
            abstract:"nginx负载平衡的几种策略",
            content:"nginx负载平衡的几种策略",
            classify:"服务器",
            author:"老胡来也",
            dateline:"2022-04-06 08：47"
        },
        {
            title:"Linux用户创建的学习研究",
            abstract:"Linux用户创建的学习研究",
            content:"Linux用户创建的学习研究",
            classify:"Linux",
            author:"莫大神",
            dateline:"2022-04-06 08：47"
        },
        {
            title:"大数据仪表盘探讨",
            abstract:"大数据仪表盘探讨",
            content:"大数据仪表盘探讨",
            classify:"大数据",
            author:"居家博士",
            dateline:"2022-04-18 16：18"
        },
    ])


})
//使用koa
let app = new koa()

//显示标题内容
router.get('/head',(ctx)=>{
    let html =`
    <div id="all">
            <tr>
                <td id='query'>
                    <input type="text" name="keyword" id='keyword' placeholder="查找博客标题、摘要、内容、分类、作者">
                    <input type="button" value="查找"  onclick="query()" >
                </td>
                <td id='add'>
                    <input type="button" value="新增" onclick="add()" >
                </td>
            </tr>
            <table id="content">
                <tr>
                    <th>Id</th>
                    <th>标题</th>
                    <th>摘要</th>
                    <th>内容</th>
                    <th>分类</th>
                    <th>作者</th>
                    <th>发表时间</th>
                    <th>操作</th>
                </tr>
            </table>
        </div>
    `
    ctx.body=html;
})

//显示列表
router.get('/index',async(ctx)=>{
    await blog.findAll().then(res=>{
        ctx.body=res
    })
})
//删除
router.get('/delete',async(ctx)=>{
    let id = ctx.request.query.id;
     await blog.destroy({
        where: { id: id }
    })


     await blog.findAll().then(res => {
        ctx.body = res
    })
})

//新增
router.get('/add',(ctx)=>{
    let html=`
    <table>
    <h2>添加博客或者修改</h2>
    <tr>
        <td>标题:</td>
        <td><input type="text" name="title"></td>
    </tr>
    <tr>
        <td>摘要:</td>
        <td><input type="text" name="title"></td>
    </tr>
    <tr>
        <td>内容:</td>
        <td><input type="text" name="title"></td>
    </tr>
    <tr>
        <td>分类:</td>
        <td><input type="text" name="title"></td>
    </tr>
    <tr>
        <td>作者:</td>
        <td><input type="text" name="title"></td>
    </tr>
</table>
        <tr>    
            &nbsp; &nbsp; &nbsp; &nbsp; 
            <td><input type="button" value="保存" onclick="save()"></td>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            <td><input type="button" value="取消" onclick="cancel()"></td>
        </tr>ss
    `
    ctx.body=html;
})

//使用中间键
app.use(bodyparser());
app.use(cors());
app.use(router.routes());

//设置运行地址
let port = 1234;
app.listen(port)
console.log(`地址：http://localhost:${port}`)
