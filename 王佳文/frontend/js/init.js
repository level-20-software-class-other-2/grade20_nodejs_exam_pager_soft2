

//展示列表
$(function(){
    $.get('http://localhost:8000/blog',data=>{
        data.forEach(item => {
            let html=`
        <tr class="rowData" key='${item.id}'>
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.get}</td>
            <td>${item.text}</td>
            <td>${item.type}</td>
            <td>${item.author}</td>
            <td>${item.time}</td>
            <td><input type="button" value="编辑" onclick="btnEdit(${item.id})"></td>
            <td><input type="button" value="删除" onclick="btnDel(${item.id})"></td>
        </tr>
            `
        $('#tbData').append(html)
        });
    })
})

//搜索
function Query(){
    let keyword=$('#find').val();
    getBlogList(keyword).then(data=>{
        data.forEach(item => {
            let row=$('.rowData')
            row.remove();
            let html=`
        <tr class="rowData" key='${item.id}'>
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.get}</td>
            <td>${item.text}</td>
            <td>${item.type}</td>
            <td>${item.author}</td>
            <td>${item.time}</td>
        </tr>
            `
        $('#tbData').append(html)
    })
    })
}


//增加

function Add(obj){
    location.href='./addOrEdit.html'
    postBlog(obj).then(data=>{
        console.log(data);
    })
}

//保存
function Save(){
    let id= $('[name=id]').val()
    let title=$('[name=title]').val()
    let get=$('[name=get]').val()
    let text=$('[name=text]').val()
    let type=$('[name=type]').val()
    let author=$('[name=author]').val()
    let time=$('[name=time]').val()
    let obj={
        id,
        title,
        get,
        text,
        type,
        author,
        time
        
    }
    if(obj.id){
        putBlog(obj.id,obj).then(data=>{
            if(data.code===1000){
                location.href='./index.html'
            }else{
                alert(data.msg)
            }
        })
    }else
    {
        postBlog(obj).then(data=>{
            if(data.code===1000){
                location.href='./index.html'
            }else{
                alert(data.msg)
            }
        })
    }
}

//编辑
function btnEdit(id){
    location.href=`./addOrEdit.html?${id}`
}


//删除

function btnDel(id){
    let confirmDel=confirm('确定删除吗')
    if(confirmDel){
        deleteBlog(id).then(data=>{
            let id=data.data.id
            $(`[key=${id}]`).remove();
        })
    }
}

//取消
function Cancel(){
    location.href='./index.html'
}