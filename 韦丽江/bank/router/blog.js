'use strict'

const {blog , Op} =require('../model')


let fn_blog =async (ctx, next) => {

    let blog =await blog.findAll()

    ctx.body = blog

}




let fn_blogfind =async (ctx, next) => {


     let keyword =  ctx.request.body.keyword


    let newData =blog.findAll(

        {

            where:{
    
                [Op.or]:[

                    {id:isNaN(keyword) ? -1 :parseFloat(keyword)},
    
                    {biaoti:keyword },
    
                    {zaiyao:keyword},
    
                    {neirong:keyword},
    
                    {fenlei:keyword  },
    
                    {writer:keyword},
                    {datetime:isNaN(keyword)}
    
                ]
    
            }
    
        }

    )

    ctx.body = newData

}

let fn_blogDel =async (ctx, next) => {

    let id = ctx.request.body.id || ''

    await blog.destroy(

        {
            where:{

               id:id 
            
            }
            
            }

    )
    
    let delarr =await blog.findAll()


    ctx.body = delarr

}


let fn_blogAdd =async (ctx, next) => {

    let obj = ctx.request.body

    await blog.create(

        obj

    )

    let arr =await blog.findAll()

    ctx.body = arr

}


let fn_blogEdit =async (ctx, next) => {

    let obj = ctx.request.body


    let blog = {

        biaoti:obj.biaoti,

        zaiyao:obj.zaiyao,

        neirong:obj.neirong,

        fenlei:obj.fefnlei,

        writer:obj.writer,
        datatime:obj.datatime

      

    }

    await blog.update(blog,{

        where:{

            id:obj.id

        }

    })

   
    let arr = await blog.findAll()


    ctx.body = arr

}

module.exports = {


    'get /blog': fn_blog,
    'post /delete': fn_blogDel,
    'post /add': fn_blogAdd,
    'post /edit': fn_blogEdit,
    'post /find': fn_blogfind,


}
