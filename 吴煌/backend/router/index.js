const router=require('koa-router')()
const fs=require('fs');




function routerFile(path){
    let routerFile=fs.readdirSync(path);
    let routerFiles=routerFile.filter(item=>{
        return item.endsWith('.js') && item!=='index.js'
    })

    return routerFiles
}

function regrouter(routerFiles){
    routerFiles.forEach(item => {
            let obj=require(__dirname+'/'+item);

            for(let key in obj)
            {
                let temArr=key.split(' ');
                let rmethod=temArr[0];
                let rurl=temArr[1];
                let fnobj=obj[key]
                console.log(fnobj);

                if(rmethod=='get'){
                    router.get(rurl,fnobj)
                }else if(rmethod=='post'){
                    console.log('post进来了');
                    router.post(rurl,fnobj)
                }else if(rmethod=='put'){
                    router.put(rurl,fnobj)
                }else if(rmethod=='delete'){
                    router.delete(rurl,fnobj)
                }

            }
    });

    return router.routes();
}




module.exports=function(){
    let routerFiles=routerFile(__dirname);
    let fn=regrouter(routerFiles);
    return fn
}