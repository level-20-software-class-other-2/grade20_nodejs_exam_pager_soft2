var {DataTypes}=require('sequelize')


let model={
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    digest:{
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    class:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    createTime:{
        type:DataTypes.STRING,
        allowNull:true,
        default:'2020-04-03'
    }

}

module.exports=model