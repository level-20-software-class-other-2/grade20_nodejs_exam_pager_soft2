'use strict'
//封装
const { Sequelize, Op, DataTypes } = require('sequelize')
const fs = require('fs')
const { db_database, db_username, db_password, db_host, db_dialect } = require('../config/db')
//连接
let sequelize = new Sequelize(db_database, db_username, db_password, {
    host: db_host,
    dialect: db_dialect
})

let resPath = __dirname;
let Files = fs.readdirSync(resPath);

let resFiles = Files.filter(item => {
    return item.endsWith('.js') && item !== 'index.js';
})
//resobj的
let resobj = {};
resFiles.forEach(item => {
    let modelName = item.replace('.js','');
    let tmpObj = require(__dirname + '/' + modelName)
    let tableName = modelName.toLowerCase();
    resobj[modelName] = sequelize.define(tableName, tmpObj)
})

resobj.Op = Op;

resobj.sync = async () => {
    sequelize.sync({ force: true }).then(() => {
        resobj.Products.bulkCreate([
            {
                biaoti: '论EF Core的自我修养',
                zhaiyao: '论EF Core的自我修养',
                neirong: '论EF Core的自我修养',
                fenlei: '.Net',
                zuozhe: 'InCerry',
            },
            {
                biaoti: 'DDD之我见',
                zhaiyao: 'DDD之我见',
                neirong: 'DDD之我见',
                fenlei: '编程技术',
                zuozhe: '某大神',
            },
            {
                biaoti: 'ngibx负载平衡的几种策略',
                zhaiyao: 'ngibx负载平衡的几种策略',
                neirong: 'ngibx负载平衡的几种策略',
                fenlei: '服务器',
                zuozhe: '老胡来也',
            },
            {
                biaoti: 'Linux用户创建的学习研究',
                zhaiyao: 'Linux用户创建的学习研究',
                neirong: 'Linux用户创建的学习研究',
                fenlei: 'Linux',
                zuozhe: '某大神',
            },
            {
                biaoti: '大数据仪表盘探讨',
                zhaiyao: '大数据仪表盘探讨',
                neirong: '大数据仪表盘探讨',
                fenlei: '大数据',
                zuozhe: '居家博士',
            }
        ])
    })
}


module.exports = resobj;