'use strict'

const { Blogs, Op } = require('../model');

let fn_product = async (ctx, next) => {
    let keyword = ctx.request.query.keyword || ''
    if (keyword) {
        let list = await Blogs.findAll({
            where: {
                [Op.or]: []
            },
            order: ['id']
        })
        ctx.body = list
    } else {
        let list = await Blogs.findAll({
            order: ['id']
        })
        console.log(list);
        ctx.body = list
    }
}
let fn_product_id = async (ctx, next) => {
    let id = ctx.request.params.id;
    let arr = await Blogs.findByPk(id);
    if (arr) {
        ctx.body = {
            code: 1000,
            data: arr,
            msg: '获取成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '没有找到'
        }
    }
}
let fn_post = (ctx, next) => {
    let obj = ctx.request.body;
    if (obj) {
        let tmpObj = {
            
        };
        Blogs.create(tmpObj)
        ctx.body = {
            code: 1000,
            data: tmpObj,
            msg: '新增成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '数据验证不成功'
        }
    }
}
let fn_put = async (ctx, next) => {
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    let Blogs = await Blogs.findByPk(id);
    if (Blogs) {
        await Blogs.update(obj, {
            where: { id: id }
        });
        ctx.body = {
            code: 1000,
            data: Blogs,
            msg: '修改成功'
        }
    }
}
let fn_delete = async (ctx, next) => {
    let id = ctx.request.params.id;
    let Blogs = await Blogs.findByPk(id)
    if (Blogs) {
        await Blogs.destroy({
            where: { id: id }
        })
        ctx.body = {
            code: 1000,
            data: { id: id },
            msg: '删除成功'
        }
    }
}

module.exports = {
    'get /product': fn_product,
    'get /product_id': fn_product_id,
    'post /product': fn_post,
    'put /product/:id': fn_put,
    'delete /product/:id': fn_delete,
}