'use strict'

const koa = require('koa')
const app = new koa()
const bodyparser = require('koa-bodyparser')
const cors = require('koa-cors')
const router = require('./router/router')
const {sync} = require('./model/index')
sync()

app.use(cors())
app.use(bodyparser())
app.use(router.routes())

app.listen(8000)