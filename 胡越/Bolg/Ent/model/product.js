const { DataTypes } = require('sequelize')


const Product = {
    title: {
        type: DataTypes.STRING,
        allowNull:false
    },
    abstract: {
        type: DataTypes.STRING,
        allowNull:false
    },
    content: {
        type: DataTypes.STRING,
        allowNull:false
    },
    sort: {
        type: DataTypes.STRING,
        allowNull:false
    },
    author: {
        type: DataTypes.STRING,
        allowNull:false
    },
    dateline: {
        type: DataTypes.STRING,
        allowNull:false
    }
}


module.exports=Product;