function getBlogById(id) {
    return new Promise(function (resolve,reject) {
        $.get(`http://localhost:8000/blog/${id}`,data=>{
            
        })
    })
}

function postBlog(obj) {
    console.log(obj);
    return new Promise(function (resolve,reject) {
        $.post(`http://localhost:8000/blog`,obj,data=>{
            resolve(data)
        })
    })
}
function putBlog(id,obj) {
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`http://localhost:8000/blog/${id}`,
            type:'put',
            data:obj,
            success:data=>{
                resolve(data)
            }
        })
    })
}

function delBlogById(id) {
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`http://localhost:8000/blog/${id}`,
            type:'delete',
            success:data=>{
                resolve(data)
            }
        })
    })
}