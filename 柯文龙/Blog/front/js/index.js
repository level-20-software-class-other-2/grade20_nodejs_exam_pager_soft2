$(function () {

    $.get('http://127.0.0.1:8000/head', (data) => {
        $('#top').append(data)

        $.get('http://127.0.0.1:8000/index', (data) => {
            log(data)
        })
    })
})
function log(data) {
    data.forEach(item => {
        let html = `
    <tr id ="html">
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${item.zy}</td>
            <td>${item.nr}</td>
            <td>${item.Category}</td>
            <td>${item.Author}</td>
            <td>
            <input type="button" value="编辑" onclick="Edit(${item.id})">
            <input type="button" value="删除" onclick="Delete(${item.id})">
            </td>
        </tr>
    `
        $('#content').append(html)
    });
}

function query() {
    let keyword = $('[name=keyword]').val()
    $.get('http://127.0.0.1:8000/data?keyword=' + keyword, (data) => {
        let tr = $('tr').length
        for (let i = 0; i < tr; i++) {
            $('#html').remove()
        }
        console.log(tr);
        log(data)
    })
}
function add() {
    $.get('http://127.0.0.1:8000/addhtml', (data) => {

        $('#all').remove()

        $('#html').remove()

        $('#top').append(data)
    })
}

function save() {
    let name = $('[name=name]').val()
    let zy = $('[name=zy]').val()
    let nr = $('[name=nr]').val()
    let Category = $('[name=Category]').val()
    let Author = $('[name=Author]').val()

    let obj = {
        name,
        zy,
        nr,
        Category,
        Author
    }
    if (name == '') {
        alert('无法添加空值')
    } else {
        $.post('http://127.0.0.1:8000/indexadd', obj, (Data) => {

            $('#add').remove()

            $.get('http://127.0.0.1:8000/head', (data) => {

                $('#top').append(data)

                log(Data)
            })
        })
    }
}
function Delete(id) {
    if (confirm("确认删除数据吗？")) {

        $.get('http://127.0.0.1:8000/indexDeleted?id=' + id, (data) => {

            let tr = $('tr').length
            for (let i = 0; i < tr; i++) {
                $('#html').remove()

            }
            log(data)
        })
    }
}


function cancel() {
    $('#add').remove

    $.get('http://127.0.0.1:8000/head', (data) => {

        $('#top').append(data)

        $.get('http://127.0.0.1:8000/index', (data) => {

            log(data)
        })
    })
}