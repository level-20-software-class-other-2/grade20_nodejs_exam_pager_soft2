'use strict'

const { Sequelize, Op } = require('sequelize');
const fs = require('fs');


let sequelize = new Sequelize('stock', 'postgres', 'clp5201314.', {
    host: 'mhltk.top',
    dialect: 'postgres'
})

let files = fs.readdirSync(__dirname)

let resFiles = files.filter(item => {
    return item.endsWith('.js') && item != 'index.js'
})

let resObj = {}

resFiles.forEach(item => {
    let moduleName = item.replace('.js', '')

    let tmObj = require(__dirname + '/' + item);

    let tableName = moduleName.toLowerCase();

    resObj[moduleName] = sequelize.define(tableName, tmObj)
});

resObj.Op = Op

resObj.sync = async() => {
    sequelize.sync({ force: true }).then(() => {
        resObj.Product.bulkCreate([
            
            {
                head:'论EF Core的自我修养',
                digest:'论EF Core的自我修养',
                details:'论EF Core的自我修养',
                classify:'.Net',
                anthour:'InCerry',
                
            },
            {
                head:'DDD之我见',
                digest:'DDD之我见',
                details:'DDD之我见',
                classify:'编程技术',
                anthour:'某大神',
                
            },
            {
                head:'nginx负载平衡的几种策略',
                digest:'nginx负载平衡的几种策略',
                details:'nginx负载平衡的几种策略',
                classify:'服务器',
                anthour:'老虎来也',
                
            },
            {
                head:'linux用户创建的学习研究',
                digest:'linux用户创建的学习研究',
                details:'linux用户创建的学习研究',
                classify:'linux',
                anthour:'某大神',
                
            },
            {
                head:'大数据仪表盘探讨',
                digest:'大数据仪表盘探讨',
                details:'大数据仪表盘探讨',
                classify:'大数据',
                anthour:'居家博士',
                
            },
        ])
    })
}

module.exports = resObj;