

const {DataTypes}=require('sequelize');

let Product={
    headline:{
        type:DataTypes.STRING,
        allowNull:false
    },
    abstract:{
        type:DataTypes.STRING,
        allowNull:false
    },
    details:{
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },

}

module.exports=Product