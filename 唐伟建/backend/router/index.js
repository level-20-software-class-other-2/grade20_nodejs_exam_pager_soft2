const {Blog,Op} = require('../sequelize/sequelize')



fn_data=async(ctx)=>{
   
    await Blog.findAll().then((res)=>{
        ctx.body=res
    })
}
fn_del=async(ctx)=>{
    let id = ctx.request.query.id

    await Blog.findByPk(id).then( async(res)=>{
        await res.destroy()
    })

    await Blog.findAll().then((res)=>{
        ctx.body=res
    })
}

fn_query=async(ctx)=>{
    let keyword = ctx.request.query.keyword
    console.log(keyword);
    if (keyword!=='') {
        
        if(isNaN(keyword)){
            await Blog.findAll({
                where:{
                    [Op.or]:[
                        {title:keyword},
                        {digest:keyword},
                        {content:keyword},
                        {classify:keyword},
                        {author:keyword},
                        {time:keyword}
                    ]
                }
            }).then((res)=>{
                ctx.body=res
            })
        }else{
            await Blog.findAll({
                where:{
                    [Op.or]:[
                        {id:keyword},
                        {title:keyword},
                        {digest:keyword},
                        {content:keyword},
                        {classify:keyword},
                        {author:keyword},
                        {time:keyword}
                    ]
                }
            }).then((res)=>{
                ctx.body=res
            })
        }

    }else{
        await Blog.findAll().then((res)=>{
            ctx.body=res
        })
    }

}

fn_add =async(ctx)=>{

        let data = ctx.request.body

        await Blog.create(data)

        await Blog.findAll().then((res)=>{
            ctx.body=res
        })

}


fn_edit=async(ctx)=>{
    let data = ctx.request.body


    await Blog.findByPk(data.id).then( async(res)=>{
            await res.update({
                title:data.title,
                digest:data.digest,
                content:data.content,
                classify:data.classify,
                author:data.author
            })
    })
    await Blog.findAll().then((res)=>{
        ctx.body=res
    })
}

let get = [['/data',fn_data],['/query',fn_query],['/del',fn_del]]
let post=[['/add',fn_add],['/edit',fn_edit]]
module.exports={
    'GET':get,
    'POST':post
}