const fs=require('fs')
const {Sequelize,Op}=require('sequelize')

const sequelize=new Sequelize('sker1','postgres','123',{
    host:'www.sker.cool',
    dialect:'postgres'
})

let files=fs.readdirSync(__dirname)

let resFiles=files.filter(item=>{
    return item.endsWith('.js') && item !== 'index.js'
})

let resObj={}
resFiles.forEach(item => {
    let modelName=item.replace('.js','')
    let tmpObj=require(__dirname+'/'+item)
    let tableName=modelName.toLowerCase()
    resObj[modelName]=sequelize.define(tableName,tmpObj)
});
resObj.Op=Op

resObj.sync=async()=>{
    sequelize.sync({force:true}).then(()=>{
        resObj.Boke.bulkCreate([
            {
                tit: '论Ef Core的自我修养',
                abs:'论Ef Core的自我修养',
                cont: '论Ef Core的自我修养',
                clas:'.Net',
                writer:'InCerry',
                time:'2022-04-06 08:47',
            },
            {
                tit: 'DDD之我见',
                abs:'DDD之我见',
                cont: 'DDD之我见',
                clas:'编程技术',
                writer:'某大神',
                time:'2022-04-03 23:47',
            },
            {
                tit: 'nginx负载平衡的几种策略',
                abs:'nginx负载平衡的几种策略',
                cont: 'nginx负载平衡的几种策略',
                clas:'服务器',
                writer:'老胡来也',
                time:'2022-04-06 08:47',
            },
            {
                tit: 'Linux用户创建的学习研究',
                abs:'Linux用户创建的学习研究',
                cont: 'Linux用户创建的学习研究',
                clas:'Linux',
                writer:'某大神',
                time:'2022-04-06 08:47',
            },
            {
                tit: '大数据仪表盘探讨',
                abs:'大数据仪表盘探讨',
                cont: '大数据仪表盘探讨',
                clas:'大数据',
                writer:'居家博士',
                time:'2022-04-18 16:18',
            },
            {
                tit: '他没id啊',
                abs:'我这能力有限这在我知识盲区了啊',
                cont: '段段菜菜',
                clas:'老胡捞捞',
                writer:'QAQ',
                time:'2022-04-08 19:42',
            },
        ])
    })
}

module.exports=resObj