$(function () {
    $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)

                $.get('http://127.0.0.1:8000/index', (data) => {
                    console.log(data);
                    log(data)
                })
            })
})

function log(data) {
    let Data = data.filter((item) => {
        return item.name !== undefined
    })

    Data.forEach(item => {
        let html = `
        <tr id ="html">
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.abstract}</td>
            <td>${item.content}</td>
            <td>${item.type}</td>
            <td>${item.author}</td>
            <td>${item.postedTime}</td>

            <td>
            <input type="button" value="编辑" onclick="Edit(${item.id})">
            <input type="button" value="删除" onclick="Delete(${item.id})">
            </td>
        </tr>
        `
        $('#content').append(html)
    });
}

function query() {

    let keyword = $('[name=keyword]').val()

    $.get('http://127.0.0.1:8000/data?keyword=' + keyword, (data) => {

        let tr = $('tr').length
        for (let i = 0; i < tr; i++) {
            $('#html').remove()
        }
        log(data)
    })
}

function add() {
    $.get('http://127.0.0.1:8000/addhtml', (data) => {

        $('#all').remove()

        $('#html').remove()

        $('#top').append(data)
    })
}

function save(Id) {

    let id = $('[name=id]').val()
    let title = $('[name=title]').val()
    let abstract = $('[name=abstract]').val()
    let content = $('[name=content]').val()
    let type = $('[name=type]').val()
    let author = $('[name=author]').val()
    let postedTime = $('[name=postedTime]').val()

    let obj = {
        id,
        title,
        abstract,
        content,
        type,
        author,
        postedTime
    }

    if (Id == undefined) {
        if (name=='') {
            alert('无法添加空值')
        }else{
            $.post('http://127.0.0.1:8000/indexadd', obj, (Data) => {

                    $('#add').remove()
                    $.get('http://127.0.0.1:8000/head', (data) => {
                    $('#top').append(data)

                    log(Data)

                })


            })
        }

    } else {

        $.post('http://127.0.0.1:8000/indexedit', obj, (Data) => {

            $('#add').remove()
            $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)
                log(Data)
            })
        })
    }
}

function Edit(id) {

    $.get('http://127.0.0.1:8000/index', (data) => {

        let Data = data.filter((item) => {
            return item.id == id
        })
// id-id name-title age-abstract(int-string)摘要 gander-content内容 type分类 author作者 postedTime发表时间
        Data.forEach((item) => {
            let newId = item['id']
            let newTitle = item['title']
            let newAbstract = item['abstract']
            let newContent = item['content']
            let newType = item['type']
            let newAuthor = item['author']
            let newPostedTime = item['postedTime']

            let obj = {
                newId,
                newTitle,
                newAbstract,
                newContent,
                newType,
                newAuthor,
                newPostedTime
            }

            $.post('http://127.0.0.1:8000/addhtml', obj, (data) => {
                $('#all').remove()

                let tr = $('tr').length
                for (let i = 0; i < tr; i++) {
                    $('#html').remove()
                }
                $('#top').append(data)
            })
        })
    })
}

function Delete(id) {

    $.get('http://127.0.0.1:8000/indexDeleted?id=' + id, (data) => {
        console.log(data);
        let tr = $('tr').length
        for (let i = 0; i < tr; i++) {
            $('#html').remove()
        }
        log(data)
    })

}

function cancel(){
    $('#add').remove()

    $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)

                $.get('http://127.0.0.1:8000/index', (data) => {
                    log(data)
                })
            })
}