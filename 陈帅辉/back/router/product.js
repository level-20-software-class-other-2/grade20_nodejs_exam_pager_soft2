

const{Products,Op}=require('../model')


let fn_lsit=async(ctx,next)=>{
    let keyword=ctx.request.query.keyword
    if(keyword){
        let list=await Products.findAll({
            where:{
                [Op.or]:[
                    {id:isNaN(keyword)?0:parseInt(keyword)},
                    {biaoti:(keyword)},
                    {zhaiyao:(keyword)},
                    {neirong:(keyword)},
                    {fenlei:(keyword)},
                    {zuozhe:(keyword)},
                    {time:(keyword)}
                ]
            }
        },{
            order:['id']
        })
        ctx.body=list
    }else{
        let list=await Products.findAll({
            order:['id']
        })
        ctx.body=list
    }
}

let fn_lsit_id=async(ctx,next)=>{
    let id =ctx.request.params.id
    if(id){
        await Products.findByPk(id)
        ctx.body=id
    }
}
let fn_lsit_post=async(ctx,next)=>{
    let obj=ctx.request.body;
    await Products.create(obj)
    ctx.body=''
}
let fn_lsit_put=async(ctx,next)=>{
    let id =ctx.request.body.id
    let obj=ctx.request.body;
    if(id){
        await Products.update(obj,{
            where:{
                id:id
            }
        })
        ctx.body=id
    }

}
let fn_lsit_delete=async(ctx,next)=>{
    let id =ctx.request.params.id
    if(id){
        await Products.destroy({
            where:{
                id:id
            }

        })
        ctx.body=id

    }
}

module.exports={
    'get /product':fn_lsit,
    'get /product/:id':fn_lsit_id,
    'post /product':fn_lsit_post,
    'put /product':fn_lsit_put,
    'delete /product/:id':fn_lsit_delete
}