// id-id name-title age-abstract(int-string)摘要 gander-content内容 type分类 author作者 postedTime发表时间
fn_addOrEdit=(ctx)=>{

    let id = ctx.request.body.newId
    let title = ctx.request.body.newTitle
    let abstract = ctx.request.body.newAbstract
    let content = ctx.request.body.newContent
    let type = ctx.request.body.newType
    let author = ctx.request.body.newAuthor
    let postedTime = ctx.request.body.newPostedTime



    if (id!==undefined) {
        let html =
        `
        <div id="add">
            <table>
                <tr hidden>
                    <td>id：</td>
                    <td>
                        <input type="text" name="id" value="${id}">
                    </td>
                </tr>
                <tr>
                    <td>标题：</td>
                    <td>
                        <input type="text" name="title" value="${title}">
                    </td>
                </tr>
                <tr>
                    <td>摘要：</td>
                    <td>
                        <input type="text" name="abstract" value="${abstract}">
                    </td>
                </tr>
                <tr>
                    <td>内容：</td>
                    <td>
                        <input type="text" name="content" value="${content}">
                    </td>
                </tr>
                <tr>
                    <td>分类：</td>
                    <td>
                        <input type="text" name="type" value="${type}">
                    </td>
                </tr>
                <tr>
                    <td>作者：</td>
                    <td>
                        <input type="text" name="author" value="${author}">
                    </td>
                </tr>
                <tr>
                    <td>发表时间：</td>
                    <td>
                        <input type="text" name="postedTime" value="${postedTime}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="保存" onclick="save(${id})">
                    </td>
                    <td>
                        <input type="button" value="取消" onclick="cancel()">
                    </td>
                </tr>
            </table>
        </div>
        `
        ctx.body=html
    }else{
        let html =
        `
        <div id="add">
            <table>
                <tr hidden>
                    <td>id：</td>
                    <td>
                        <input type="text" name="id">
                    </td>
                </tr>
                <tr>
                    <td>题目：</td>
                    <td>
                        <input type="text" name="title">
                    </td>
                </tr>
                <tr>
                    <td>摘要：</td>
                    <td>
                        <input type="text" name="abstract">
                    </td>
                </tr>
                <tr>
                    <td>内容：</td>
                    <td>
                        <input type="text" name="content">
                    </td>
                </tr>
                <tr>
                    <td>分类：</td>
                    <td>
                        <input type="text" name="type">
                    </td>
                </tr>
                <tr>
                    <td>作者：</td>
                    <td>
                        <input type="text" name="author">
                    </td>
                </tr>
                <tr>
                    <td>发表时间：</td>
                    <td>
                        <input type="text" name="postedTime">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="保存" onclick="save(${''})">
                    </td>
                    <td>
                        <input type="button" value="取消" onclick="cancel()">
                    </td>
                </tr>
            </table>
        </div>
        `
        ctx.body=html
    }

}
let get = [['/addhtml',fn_addOrEdit]]
let post = [['/addhtml',fn_addOrEdit]]
module.exports={
    "GET":get,
    "POST":post
}