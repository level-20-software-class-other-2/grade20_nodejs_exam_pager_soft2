'use strict'

const { Blog, Op } = require('../model')


//展示列表
let fn_blog_List = async (ctx, next) => {
    let keyword = ctx.request.query.keyword;
    if (keyword) {
        let list = await Blog.findAll({
            where: {
                [Op.or]: [
                    { id: isNaN(keyword) ? 0 : parseInt(keyword) },
                    { title: keyword },
                    { get: keyword },
                    { text: keyword },
                    { type: keyword },
                    { author: keyword }
                ]
            },
            order: ['id']
        })
        ctx.body = list
    } else {
        let list = await Blog.findAll({
            order: ['id']
        })
        ctx.body = list
    }
}
//根据id

let fn_blog_Id = async (ctx, next) => {
    let id = ctx.request.params.id;
    let arr = await Blog.findByPk(id)
    if (arr) {
        ctx.body = {
            code: 1000,
            data: arr,
            msg: '请求成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '请求失败'
        }
    }
}

//增加
let fn_postBlog = async (ctx, next) => {
    let obj = ctx.request.body;
    if (obj.hasOwnProperty('title') && obj.hasOwnProperty('get') && obj.hasOwnProperty('text') &&
        obj.hasOwnProperty('type') && obj.hasOwnProperty('author') &&
        obj.title && obj.get && obj.text && obj.type && obj.author) {
        let resobj = {
            title: obj.title,
            get: obj.get,
            text: obj.text,
            type: obj.type,
            author: obj.author
        }
        await Blog.create(resobj)
        ctx.body = {
            code: 1000,
            data: resobj,
            msg: '请求成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '请求失败'
        }
    }
}

//编辑

let fn_putBlog = async (ctx, next) => {
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    let arr = await Blog.findByPk(id)
    if (arr) {
        await Blog.update(obj, {
            where: {
                id: id
            }
        })
        ctx.body = {
            code: 1000,
            data: arr,
            msg: '成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '失败'
        }
    }
}

//删除
let fn_deleteBlog = async (ctx, next) => {
    let id = ctx.request.params.id;
    let arr = await Blog.findByPk(id)
    if (arr) {
        await Blog.destroy({
            where: {
                id: id
            }
        })
        ctx.body = {
            code: 1000,
            data: { id: id },
            msg: '删除成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '删除失败'
        }
    }
}

module.exports = {
    'get /blog': fn_blog_List,
    'get /blog/:id': fn_blog_Id,
    'post /blog': fn_postBlog,
    'put /blog/:id': fn_putBlog,
    'delete /blog/:id': fn_deleteBlog
}
