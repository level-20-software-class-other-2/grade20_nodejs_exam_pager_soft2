'use strict'
let baseUrl='http://localhost:8000';
//封装
function getProductList(keyword){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/product?keyword=`+keyword,data=>{
            resolve(data);
        })
    })
}
function getProductById(id){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/product/${id}`,data=>{
            resolve(data);
        })
    })
}
function postProduct(obj){
    return new Promise(function(resolve,reject){
        $.post(`${baseUrl}/product`,obj,data=>{
            console.log(data);
            resolve(data);
        })
    })
}
function putProduct(id,obj){
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`${baseUrl}/product/${id}`,
            type:'put',
            data:obj,
            success:data=>{
                resolve(data);
            }
        })
    })
}
function delProductById(id){
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`${baseUrl}/product/${id}`,
            type:'delete',
            success:data=>{
                resolve(data);
            }
        })
    })
}
$(function () {
    query();
    let id = getUrlParam('id');
    if (id) {
        getProductById(id).then(res => {
            let data = res.data;
            if (data) {
                $('[name=id]').val(data.id)
                //标题
                $('[name=headline]').val(data.headline)
                //摘要
                $('[name=abstract]').val(data.abstract)
                //内容
                $('[name=details]').val(data.details)
                //分类
                $('[name=classify]').val(data.classify)
                //作者
                $('[name=author]').val(data.author)
            }
        })
    }
})
//查找
function query(){
    let keyword=$('#keyword').val();
    getProductList(keyword).then(data=>{
        renderFn(data);
    })
}
function renderFn(arr){
    let row=$('.rowData');
    let tb=$('#tbData');
    row.remove();
    arr.forEach(item=>{
        let html=`
                <tr class="rowData" key="${item.id}">
                    <td>${item.id}</td>
                    <td>${item.headline}</td>
                    <td>${item.abstract}</td>
                    <td>${item.details}</td>
                    <td>${item.classify}</td>
                    <td>${item.author}</td>
                    <td>
                        <input type="button" value="编辑" onclick="update(${item.id})">
                        <input type="button" value="删除" onclick="del(${item.id})">
                    </td>
                </tr>
        `
        tb.append(html);
    })
}
//修改
function update(id){
    location.href=`./addOrEdit.html?id=${id}`;
}
//删除
function del(id){
    let confirmDel=confirm(`你确定要删除id为${id}的项目吗？`);
    if(confirmDel){
        delProductById(id).then(data=>{
            let id=data.data.id;
            $(`[key=${id}]`).remove();
        })
    }
}
//添加
function add(){
    location.href='./addOrEdit.html'
}
//保存
function save(){
    let id=getUrlParam('id');
    let obj={
        headline:$('[name=headline]').val(),
        abstract:$('[name=abstract]').val(),
        details:$('[name=details]').val(),
        classify:$('[name=classify]').val(),
        author:$('[name=author]').val()
    }
    if(id){
        putProduct(id,obj).then(data=>{
            if(data.code===1000){
                location.href='./index.html'
            }else{
                alert(data.msg)
            }
        })
    }else{
        postProduct(obj).then(data=>{
            if(data.code===1000){
                location.href='./index.html'
            }else{
                alert(data.msg)
            }
        })
    }
}
//取消
function cancel(){
    location.href='./index.html'
}
 //获取url中的参数 其中name指的是url中的键的名称
 function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg); //匹配目标参数
    if (r != null) {
        return unescape(r[2]);
    } else {
        return null; //返回参数值
    }
}