const koa = require("koa");

const app = new koa()

const bodyparse = require("koa-bodyparser")

const cors = require("koa-cors")

const router = require("./router/router.js")

const {sequelizeSync} = require("../back/sequelize/sequelize")

sequelizeSync();

app.use(cors())

app.use(bodyparse())

app.use(router.routes())

app.listen(3000)