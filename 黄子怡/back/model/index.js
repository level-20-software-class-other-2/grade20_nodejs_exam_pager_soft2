"use strict";

const fs=require('fs');
const { Sequelize,DataTypes,Op }=require('sequelize');
const { db_dialect,db_host,db_database,db_username,db_password }=require('../config/db');

const sequelize=new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
});


let files=fs.readdirSync(__dirname);

let resFiles=files.filter(item =>{
    return item.endsWith('.js') && item !== 'index.js';
})

let resObj={};



resFiles.forEach(item =>{
    let modelName=item.replace('.js','');
    let tmpObj=require(__dirname+'/'+item);
    let tableName=modelName.toLowerCase();
    resObj[modelName]=sequelize.define(tableName,tmpObj);
})


resObj.Op=Op;

resObj.sync=async() =>{
        sequelize.sync({force:true}).then(()=>{
            resObj.Blogs.bulkCreate([
                {
                    title:'论EF的自我修养',
                    tips:'论EF的自我修养',
                    content:'论EF的自我修养',
                    type:'.NET',
                    author:'InCerry',
                    time:'2022-04-06 08：47'
                },                {
                    title:'DDD之我见',
                    tips:'DDD之我见',
                    content:'DDD之我见',
                    type:'编程技术',
                    author:'某大神',
                    time:'2022-04-03 23：47'
                },

            ])



        })
    }


module.exports=resObj;