


function getList(keyword){
    return new Promise(resolve=>{
        $.get(`${baseurl}/blog?keyword=`+keyword,data=>{
        resolve(data)
        })

    })
}

function getList_id(id){
    return new Promise(resolve=>{
        $.get(`${baseurl}/blog/${id}`,data=>{
            resolve(data)
        })
    })
}

function postList(obj){
    return new Promise(resolve=>{
        $.post(`${baseurl}/blog`,obj,data=>{
            resolve(data)
        })
    })
}
function putList(id,obj){
    return new Promise(resolve=>{
        $.ajax({
            type:'put',
            url:`${baseurl}/blog/${id}`,
            data:obj,
            success:data=>{
                resolve(data)
            }
        })
    })
}
function del(id){
    return new Promise(resolve=>{
        $.ajax({
            type:'delete',
            url:`${baseurl}/blog/${id}`,
            success:data=>{
                resolve(data)
            }
        })
    })
}