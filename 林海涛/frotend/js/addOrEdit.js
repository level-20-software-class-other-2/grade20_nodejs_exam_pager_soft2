
$(function(){
    let id = location.search.split('?')[1]
    let newData = new Promise (data=>{
        return data.id == id
    })
    if(id){
        $('[name=id]').val(newData[0].id),
        $('[name=headline]').val(newData[0].headline),
        $('[name=abstract]').val(newData[0].abstract),
        $('[name=content]').val(newData[0].content),
        $('[name=classify]').val(newData[0].classify),
        $('[name=autor]').val(newData[0].autor)
    }
})

function cancel(){
    location.href='./index.html'
}

function save(obj,id){
    let obj = {
        id:$('[name=id]').val(),
        headline:$('[name=headline]').val(),
        abstract:$('[name=abstract]').val(),
        content:$('[name=content]').val(),
        classify:$('[name=classify]').val(),
        autor:$('[name=autor]').val()
    }
    if(obj.id){
        putProduct(obj.id,obj).then(data=>{
            if(data.code===1000){
                location.href='./index.html'
            }else{
                alert(code.msg)
            }
        })
    }else{
        postProduct(obj).then(data=>{
            if(data.code===1000){
                location.href='./index.html'
            }else{
                alert(code.msg)
            }
        })
    }
}