'use strict'

const fs = require('fs')
const router = require('koa-router')()

function getFilesPath(filePath) {
    let files = fs.readdirSync(filePath)
    let routerFile = files.filter(item => {
        return item.endsWith('.js') && item != 'index.js'
    })
    return routerFile
}

function createRouter(routerFile) {
    routerFile.forEach(item => {
        let tmpPath = __dirname + '/' + item;
        let obj = require(tmpPath)
        for (let i in obj) {
            let tmpArr = i.split(' ')
            let rMethod = tmpArr[0]
            let rPath = tmpArr[1]
            let rFunction = obj[i]
            router[rMethod](rPath,rFunction)
        }
    })
}

let routers = getFilesPath(__dirname)
createRouter(routers)

module.exports=router