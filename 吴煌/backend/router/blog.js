
const {Blogs,Op}=require('../model')


let fn_index=async(ctx,next)=>{
    let keyword=ctx.request.query.keyword
    console.log(keyword);
    if(keyword){
        let list=await Blogs.findAll({
            where:{
                [Op.or]:{
                    id:isNaN(keyword)? 0 : parseInt(keyword),
                    biaoti:keyword,
                    zhaiyao:keyword,
                    neirong:keyword,
                    fenlei:keyword,
                    zuozhe:keyword
                }
            }
        })
        ctx.body=list
    }else{
        let list=await Blogs.findAll({
            order:[['id','asc']]
        })
        ctx.body=list
    }

  

}



let fn_delete=async(ctx,next)=>{
    let id=ctx.request.params.id;
     console.log(id);
     await Blogs.destroy({
         where:{id:id}
     })

     let list =await Blogs.findAll({});

     ctx.body=list

}


let fn_post=async(ctx,next)=>{
    let obj=ctx.request.body

    await Blogs.bulkCreate([obj])
    let list = await Blogs.findAll({});
    ctx.body=list
}


let fn_put_id=async(ctx,next)=>{
    let id=ctx.request.params.id
    console.log(id);
    let list =await Blogs.findAll({
        where:{id:id}
    });
    ctx.body=list

}


let fn_put=async(ctx,next)=>{
    let id=ctx.request.params.id
    let obj=ctx.request.body
    await Blogs.update({biaoti:obj.biaoti,zhaiyao:obj.zhaiyao,neirong:obj.neirong,fenlei:obj.fenlei,zuozhe:obj.zuozhe,createdAt:obj.time},{
        where:{id:id}
    })

    let list =await Blogs.findAll({});

    ctx.body=list
}


module.exports={
    'get /blog':fn_index,
    'delete /blog/:id':fn_delete,
    'post /blog':fn_post,
    'get /blog/:id':fn_put_id,
    'put /blog/:id':fn_put
}