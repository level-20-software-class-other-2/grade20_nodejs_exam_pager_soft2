$(()=>{
    find()

})

//渲染函数
function renderer(dataArr) {
    let table=$("#table")
    console.log(table);
    
    let dataArea=$(".dataArea").remove()


    dataArr.data.forEach(item => {
        let html=`
            <tr class="dataArea" key="${item.id}" >
            <td>${item.id}</td>
            <td>${item.headline}</td>
            <td>${item.digest}</td>
            <td>${item.content}</td>
            <td>${item.classify}</td>
            <td>${item.author}</td>
            <td>${item.postedTime}</td>
        

        <td>
            <input type="button" value="编辑" onclick="Edit(${item.id})">
            <input type="button" value="删除" onclick="del(${item.id})">
        </td>

    </tr>
    
        `
        table.append(html)
        
    });

}

function find() {
    let keyword=$("#keyword").val()
    console.log(keyword);
    getList(keyword).then(res=>{
        console.log(res);

        renderer(res)
    })
    
}


function del(id) {
    deleteId(id).then(res=>{
        console.log(res);
        renderer(res)

    })
    
}

function Edit(id) {
    window.location.href=`./addOrEdit.html?${id}`
    
}

function add() {
    window.location.href=`./addOrEdit.html`
    
}