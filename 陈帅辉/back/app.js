'use strict'

const koa=require('koa')
const router=require('./router')
const bodyparser=require('koa-bodyparser')
const cors=require('koa-cors')
const{sync}=require('./model')
let app=new koa();
sync()
app.use(cors())
app.use(bodyparser())
app.use(router())


let prot=8000;
app.listen(prot)
console.log(`http://localhost:${prot}`);