const {Sequelize,DataTypes,Op}=require('sequelize')
const fs=require('fs')
const sequelize=new Sequelize('soft2db','postgres','wh201314',{
    host:'long-live.top',
    dialect:'postgres'
})



let blogFile=fs.readdirSync(__dirname);
let blogFiles=blogFile.filter(item=>{
    return item.endsWith('.js') && item!=='index.js'
})


let resobj={}
resobj.Op=Op;

blogFiles.forEach(item => {
     let modelName=item.replace('.js','');
     let modelobj=require(__dirname+'/'+item);
     let tablName=modelName.toLowerCase();
     resobj[modelName]=sequelize.define(tablName,modelobj)
});

resobj.sync=async()=>{
    await sequelize.sync({force:true}).then(()=>{
        resobj.Blogs.bulkCreate([
            {
                biaoti:'论ef core',
                zhaiyao:'摘要1',
                neirong:'我是内容',
                fenlei:'分类1',
                zuozhe:'作者1',
            },
            {
                biaoti:'标题2',
                zhaiyao:'摘要2',
                neirong:'我是内容',
                fenlei:'分类2',
                zuozhe:'作者2',
            },
            {
                biaoti:'标题3',
                zhaiyao:'摘要3',
                neirong:'我是内容',
                fenlei:'分类3',
                zuozhe:'作者3'
            },
        ])
    })
}



module.exports=resobj

