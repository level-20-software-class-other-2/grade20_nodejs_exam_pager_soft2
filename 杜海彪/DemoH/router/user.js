//前端用户页显示数据
const {Users}=require('../model/index');

let fn_user=async(ctx,next)=>{
    list_user=await Users.add();
    ctx.body=list_user
}

//删除
let fn_delete=async (ctx,next)=>{
    let id=ctx.request.params.id
    console.log(id);
    await Users.destroy({
        where:{
            id:id
        }
    })
    ctx.body=1
}
module.exports={
    'get /user':fn_user,
    'delete /user/:id':fn_delete
}
