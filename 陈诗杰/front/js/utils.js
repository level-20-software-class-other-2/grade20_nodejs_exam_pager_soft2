'use strict';

let get = async url => $.get(url);

let post = async (url, data) => $.post(url, data)

let put = async (url, data) => {
    return $.ajax({
        url,
        data,
        type: 'PUT'
    })
}

let del = async url => {
    return $.ajax({
        url,
        type: 'DELETE'
    })
}

function render(table, dataArr) {
    for (const dataRow of dataArr) {
        table.append(`<tr class="dataRow" key="${dataRow.id}"></tr>`);
        let curRow = table.find('tr:last');

        for (const key in dataRow) {
            if (dataRow.hasOwnProperty(key)) {
                const data = dataRow[key];
                curRow.append(`<td class="${key}">${data}</td>`);
            }
        }

        curRow.append(`
            <td>
                <input type="button" value="编辑" onclick="edit(${dataRow.id})">
                <input type="button" value="删除" onclick="remove(${dataRow.id})">
            </td>
        `);
    }
}