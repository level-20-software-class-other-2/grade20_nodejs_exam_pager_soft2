'use strict'

const koa = require('koa');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors');
const router = require('./router/router.js');
const app = new koa();
app.use(bodyparser());
app.use(cors());
app.use(router.routes());
const { sync } = require('./model')
sync()
app.listen(8000)