"use strict"
let fn_data;
$(function () {
    console.log('我来了，我又走了');
    query();
})

function query() {
    let keyword = $('#keyword').val();
    if (keyword) {
        console.log(keyword);
        // 2请求获得商品列表数据
        $.get('http://localhost:8000/product', data =>{
            console.log(data);
            // 3、
            let result = data.filter(item => {
                return item.id == keyword || item.headline== keyword || item.abstract == keyword || item.details == keyword ||
                item.classify ==keyword ||item.author ==keyword;
            })

            console.log(result);
            let row = $('.rowData');
            row.remove();
            result.forEach(item => {
                let html = `
                <tr class="rowData">
                    <td>${item.id}</td>
                    <td>${item.headline}</td>
                    <td>${item.abstract}</td>
                    <td>${item.details}</td>
                    <td>${item.classify}</td>
                    <td>${item.author}</td>
                    // <td>${item.update}</td>
                    <td>
                        <input type="button" value="编辑" onclick="update(${item.id})" >
                        <input type="button" value="删除" onclick="del(${item.id})" >
                    </td>
                </tr>
                `
                $('#tbData').append(html);
            })


        })
    } else {
        $.get('http://localhost:8000/product', data =>{
            // 3、
            let result = data.filter(item => {
                return item.id == keyword || item.headline== keyword || item.abstract == keyword || item.details == keyword ||
                item.classify ==keyword ||item.author ==keyword;
            })
            let row = $('.rowData');
            row.remove();
            data.forEach(item => {
                let html = `
                <tr class="rowData">
                <td>${item.id}</td>
                <td>${item.headline}</td>
                <td>${item.abstract}</td>
                <td>${item.details}</td>
                <td>${item.classify}</td>
                <td>${item.author}</td>
                // <td>${item.update}</td>
                <td>
                    <input type="button" value="编辑" onclick="update(${item.id})" >
                    <input type="button" value="删除" onclick="del(${item.id})" >
                </td>

                </tr>
                `
                $('#tbData').append(html);
            })
        })
    }

}


function add(){
    window.location.href='./editOrAdd.html'
}

function cancel(){
    window.location.href='./index.html'
}

function update(id){
    window.location.href=`./editOrAdd.html?${id}`
}

function del(id){
    if (confirm("确认删除该数据吗？")) {
        $.ajax({
            url:`http://localhost:8000/product/${id}`,
            type:'delete',
            success:()=>{
                window.location.href='./index.html'
            }
        })
    }

}