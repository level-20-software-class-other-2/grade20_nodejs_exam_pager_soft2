'use strict'

$(()=>{
    $.get('http://localhost:8080/blogs',(data)=>{
        list(data)
    })
})

function btnFind(){
    let keyword=$('#keyword').val()
    $.get(`http://localhost:8080/blogs?keyword=${keyword}`,(data)=>{
        $('.blogs').remove();
        list(data)
    })
}
function btnAdd(){
    window.location.href='./addOrEidt.html'
}
function btnEidt(id){
    window.location.href=`./addOrEidt.html?id=${id}`
}
function btnDle(id){
    if ( confirm('确认删除？')) {
        $.ajax({
            type:"delete",
            url:`http://localhost:8080/blogs/${id}`,
            success:(res)=>{
                if (res==='删除成功') {
                    $(`[key=${id}]`).remove();
                }else{
                    alert('删除失败')
                }
            }
        })
    }
}


function list(data){
    data.forEach(item => {
        let html=`
        <tr class="blogs" key="${item.id}">
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.abstract}</td>
            <td>${item.body}</td>
            <td>${item.classify}</td>
            <td>${item.auter}</td>
            <td>${item.createdAt}</td>
            <td>
                <input type="button" value="修改" onclick="btnEidt(${item.id})">
                <input type="button" value="删除" onclick="btnDle(${item.id})">
            </td>
        </tr>
        `
        $('#tb').append(html);
    });
}