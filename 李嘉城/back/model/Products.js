'use strict';

const { DataTypes } = require('sequelize');
const model = {
    name: {//标题
        type: DataTypes.STRING,
        allowNull: false
    },
    abstract: {//摘要
        type: DataTypes.STRING,
        allowNull: false
    },
    content: {//内容
        type: DataTypes.STRING,
        allowNull: false
    },
    classify: {//分类
        type: DataTypes.STRING,
        allowNull: false
    },
    author: {//作者
        type: DataTypes.STRING,
        allowNull: false
    }
}
module.exports = model;