'use strict'

$(function(){
    $.get('http://localhost:8000/blog',(data)=>{
        search(data);
    })
})


function getJsonResult(){
    return new Promise(function(resolve,reject){
        $.get('http://localhost:8000/blog',(data)=>{
            resolve(data);
        })
    })
}


function query(){
    let keyword = $('[name=keyword]').val();
    getJsonResult().then((data)=>{
        if (keyword) {
            let res = data.filter(item=>{
                return item.id==keyword || item.title==keyword || item.digest==keyword || item.content==keyword 
                        || item.classify == keyword || item.autor==keyword || item.time==keyword
            });
            search(res)
        }else{
            search(data);
        }
    })
}

function search(data){
    $('.tb').remove();
    data.forEach(item => {
        let html =`
            <tr class="tb">
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td>${item.digest}</td>
                <td>${item.content}</td>
                <td>${item.classify}</td>
                <td>${item.autor}</td>
                <td>${item.time}</td>
                <td>
                    <input type="button" value="编辑" onclick="edit(${item.id})">
                    <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
        `
        $('#td').append(html);
    });
}


function add(){
    window.location.href='./editOrAdd.html'
}

function save(){
    let id = $('[name=id]').val();
    let title = $('[name=title]').val();
    let digest = $('[name=digest]').val();
    let content = $('[name=content]').val();
    let classify = $('[name=classify]').val();
    let autor = $('[name=autor]').val();


    let obj = {
        id,
        title,
        digest,
        content,
        classify,
        autor
    }

    let ob = {
        title,
        digest,
        content,
        classify,
        autor
    }

    if (obj.id) {
        $.ajax({
            url:'http://localhost:8000/blog',
            type:'put',
            data:obj,
            success:()=>{
                window.location.href='./index.html'
            }
        })
    }else{
        $.post('http://localhost:8000/blog',ob,(data)=>{
            window.location.href='./index.html'
        })
    }
}

function cancel(){
    window.location.href='./index.html'
}

function edit(id){
    window.location.href=`./editOrAdd.html?${id}`
}

function del(id){
    let msg = confirm('确认删除吗？')
    if(msg){
        $.ajax({
            url:`http://localhost:8000/blog/${id}`,
            type:'delete',
            success:()=>{
                window.location.href='./index.html'
            }
        })
    }
}


if (location.search.split('?')[1]) {
    let id = location.search.split('?')[1];
    if (id) {
        $.get(`http://localhost:8000/blog/${id}`,(data)=>{
                $('[name=id]').val(data.id);
                $('[name=title]').val(data.title);
                $('[name=digest]').val(data.digest);
                $('[name=content]').val(data.content);
                $('[name=classify]').val(data.classify);
                $('[name=autor]').val(data.autor);
        })
    }
}