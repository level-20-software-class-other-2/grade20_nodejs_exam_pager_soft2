$(function () {
    $.get('http://localhost:2000/blog', (data) => {
        fn_data(data)
    })
})

function fn_data(data) {
    data.forEach(item => {
        let html = `
        <tr class="rowdata">
            <td>${item.id}</td>
            <td>${item.headline}</td>
            <td>${item.abstract}</td>
            <td>${item.content}</td>
            <td>${item.classify}</td>
            <td>${item.author}</td>
            <td>${item.time}</td>
            <td><input type="button" value="编辑" onclick="bianji(${item.id})"></td>
            <td><input type="button" value="删除" onclick="shanchu(${item.id})"></td>
        </tr>
        `
        $('#tb').append(html)
    });
}

function chaxun() {
    let a = $('#text').val();
    chaxun1().then(data => {
        if (a) {
            let arr = data.filter(item => {
                return item.id == a || item.headline == a || item.abstract == a || item.content == a || item.classify == a || item.author == a || item.time == a
            })
            chaxun2(arr);
        } else {
            chaxun2(data);
        }
    })
}

function chaxun1() {
    return new Promise(function (res, rej) {
        $.get('http://localhost:2000/blog', data => {
            res(data)
        })
    })
}

function chaxun2(arr) {
    let row = $('.rowdata')
    let tb = $('#tb')
    row.remove()
    arr.forEach(item => {
        let html = `
        <tr class="rowdata">
            <td>${item.id}</td>
            <td>${item.headline}</td>
            <td>${item.abstract}</td>
            <td>${item.content}</td>
            <td>${item.classify}</td>
            <td>${item.author}</td>
            <td>${item.time}</td>
            <td><input type="button" value="编辑" onclick="bianji(${item.id})"></td>
            <td><input type="button" value="删除" onclick="shanchu(${item.id})"></td>
        </tr>
        `
        tb.append(html)
    });
}

function xinzen() {
    window.location.href = './xinzenbianji.html'
}

function bianji(id) {
    window.location.href = `./xinzenbianji.html?id=${id}`
}

let url = location.search;
let id = (url.split('='))[1]
if (id) {
    $.get('http://localhost:2000/blog', data => {
        data.forEach(item => {
            if (id==item.id) {
            $('#headline').val(item.headline)
            $('#abstract').val(item.abstract)
            $('#content').val(item.content)
            $('#classify').val(item.classify)
            $('#author').val(item.author)
            $('#time').val(item.time)
            }
        });
    })
}

function baocun() {
    let url = location.search;
    let id = (url.split('='))[1]
    if (id) {
        let headline = $('#headline').val()
        let abstract = $('#abstract').val()
        let content = $('#content').val()
        let classify = $('#classify').val()
        let author = $('#author').val()
        let time = $('#time').val()
        let obj = {
            id,
            headline,
            abstract,
            content,
            classify,
            author,
            time
        }
        $.ajax({
            type: "put",
            url: 'http://localhost:2000/blog',
            data: obj,
            dataType: "text",
            success: function (data) {
                window.location.href = './index.html'
            }
        })
    } else {
        let headline = $('#headline').val()
        let abstract = $('#abstract').val()
        let content = $('#content').val()
        let classify = $('#classify').val()
        let author = $('#author').val()
        let time = $('#time').val()
        let obj = {
            headline,
            abstract,
            content,
            classify,
            author,
            time
        }
        $.post('http://localhost:2000/blog', obj, (res) => {
            location.href = './index.html'
        })
    }
}

function quxiao() {
    window.location.href = './index.html'
}

function shanchu(id) {
    let a = confirm('确认删除')
    if (a) {
        $.ajax({
            type: "delete",
            url: `http://localhost:2000/blog/${id}`,
            success: function (data) {
                $('.rowdata').remove();
                fn_data(data);
            }
        })
    }
}