'use strict';

$(
    async () => {
        search()
    }
);  

async function search() {
    let keyword = $('#keyword').val() || '';
    let data = await get(`${blogUrl}?keyword=${keyword}`);
    render($('#tbData'), data);
}

async function create() {
    location.href = './createOrEdit.html'
}

async function edit(id) {
    location.href = `./createOrEdit.html?id=${id}`
}

async function remove(id) {
    let msg = `你确定要删除id为 ${id} 的项目吗？`;
    if (confirm(msg)) {
        id = await del(`${blogUrl}/${id}`);
        $(`[key="${id}"]`).remove();
    }
}