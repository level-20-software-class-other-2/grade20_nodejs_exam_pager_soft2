'use strict'
const{DataTypes}=require('sequelize');
const model={
    //标题
    headline:{
        //类型
        type:DataTypes.STRING,
        //是否允许为空
        allowNull:false
    },
    //摘要
    abstract:{
        type:DataTypes.STRING,
        allowNull:false
    },
    //内容
    details:{
        type:DataTypes.STRING,
        allowNull:false
    },
    //分类
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    //作者
    author:{
        type:DataTypes.STRING,
        allowNull:false
    }
}
module.exports=model;