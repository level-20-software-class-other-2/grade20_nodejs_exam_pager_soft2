'use strict'

function getProductList(keyword) {
    return new Promise(function (resolve, reject) {
        $.get(`http://localhost:8000/product?keyword=` + keyword, data => {
            resolve(data);
        })
    })
}

function getProductById(id) {
    return new Promise(function (resolve, reject) {
        $.get(`http://localhost:8000/product/${id}`, data => {
            resolve(data)
        })
    })
}

function putProduct(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `http://localhost:8000/product/${id}`,
            type: 'put',
            data: obj,
            success: data => {
                resolve(data);
            }
        })
    })
}

// 新增内容
function postProduct(obj) {
    return new Promise(function (resolve, reject) {
        $.post(`http://localhost:8000/product`, obj, data => {
            resolve(data);
        })
    })
}


//删除内容
function delProductById(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `http://localhost:8000/product/${id}`,
            type: 'delete',
            success: data => {
                resolve(data);
            }
        })
    })
}




