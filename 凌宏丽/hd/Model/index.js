const fs=require('fs')
const{Sequelize,DataTypes,Op}=require('sequelize');

const{db_dialect,db_host,db_username,db_password,db_database}=require('../config/db')

const sequelize=new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
})

let files=fs.readdirSync(__dirname);

let resFiles=files.filter(item=>{
    return item.endsWith('.js') && item !== 'index.js'
})

let resObj={};
resFiles.forEach(item => {
    let modeName=item.replace('.js','')
    let tmpObj=require(__dirname+'/'+item)

    let tableName=modeName.toLowerCase();
    resObj[modeName]=sequelize.define(tableName,tmpObj)
});

resObj.Op=Op;

resObj.sync=async()=>{
    sequelize.sync({force:true}).then(()=>{
        resObj.Products.bulkCreate([
            {
                title:'论EF Core的自我修养',
                abstract:'论EF Core的自我修养',
                content:'论EF Core的自我修养',
                classify:'论EF Core的自我修养',
                author:'InCerry',
                publish:2022-04-06
            },
            {
                title:'论EF Core的自我修养',
                abstract:'论EF Core的自我修养',
                content:'论EF Core的自我修养',
                classify:'论EF Core的自我修养',
                author:'InCerry',
                publish:2022-04-06
            },
            {
                title:'论EF Core的自我修养',
                abstract:'论EF Core的自我修养',
                content:'论EF Core的自我修养',
                classify:'论EF Core的自我修养',
                author:'InCerry',
                publish:2022-04-06
            },
            {
                title:'论EF Core的自我修养',
                abstract:'论EF Core的自我修养',
                abstract:'论EF Core的自我修养',
                classify:'论EF Core的自我修养',
                author:'InCerry',
                publish:2022-04-06
            }
        ])
    })
}

module.exports=resObj;