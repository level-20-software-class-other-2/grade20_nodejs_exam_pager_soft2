'use strict'
$(function() {
    query()
})

function query() {
    let keyword = $('#keyword').val()
    getProduct(keyword).then(data => {
        readFn(data)
    })
    console.log(keyword);
}

function readFn(arr) {
    let row = $(".rowData")

    let tb = $("#tbData")

    row.remove()

    arr.forEach(item => {
        let html = ` 
         <tr class="rowData" key = "${item.id}">
        <td>${item.id}</td>
        <td>${item.head}</td>
        <td>${item.digest}</td>
        <td>${item.details}</td>
        <td>${item.classify}</td>
        <td>${item.anthour}</td>
        
        <td>
        <input type="button" value="编辑" onclick="update(${item.id})">
        <input type="button" value="删除" onclick="del(${item.id})">
        </td>
    </tr>
    `
        tb.append(html)
    });


}

function Add() {
    window.location.href = './addEdit.html'
}

function update(id) {
    window.location.href = `./addEdit.html?id=${id}`
}

function del(id) {
    let a = confirm(`确认删除id为${id}的数据`)

    if (a) {
        delProduct(id).then(data => {
            $(`[key = ${data}]`).remove()
        })
    }
}