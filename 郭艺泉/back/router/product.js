'use strict';

const { Products, Op } = require('../model');
//判断类型
let fn_product_list = async (ctx, next) => {
    let keyword = ctx.request.query.keyword || '';
    if (keyword) {
        if(isNaN(keyword)){
            let list = await Products.findAll({
                where:{
                    [Op.or]:[
                        {title: keyword},
                        {abstract: keyword},
                        {content: keyword},
                        {sort: keyword},
                        {author: keyword},
                    ]
                },
                order:['id']
            })
            ctx.body = list;
        }else{
            let list = await Products.findAll({
                where:{
                    [Op.or]:[
                        {title: keyword},
                        {abstract: keyword},
                        {content: keyword},
                        {sort: keyword},
                        {author: keyword},
                        {time: keyword},
                    ]
                },
                order:['id']
            })
            ctx.body = list;
        }
  
        
    } else {
        let list = await Products.findAll({
            order:['id']
        });
        console.log(list);
        ctx.body = list;
    }

}

let fn_product_id = async (ctx, next) => {
    let id = ctx.request.params.id;
    let arr = await Products.findByPk(id);
    if (arr) {
        ctx.body = {
            code: 1000,
            data: arr,
            msg: '获取成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '获取失败'
        }
    }
}

let fn_post = (ctx, next) => {

    let obj = ctx.request.body;
    console.log(obj);
    if (obj.hasOwnProperty('title') && obj.hasOwnProperty('abstract') && obj.hasOwnProperty('content') && obj.hasOwnProperty('sort')&& obj.hasOwnProperty('author') && obj.hasOwnProperty('time') &&
    obj.title && obj.abstract && obj.content && obj.sort && obj.author && obj.time ) {
        let tmpObj = {
            title: obj.title,
            abstract: obj.abstract,
            content: obj.content,
            sort: obj.sort,
            author: obj.author,
            time: obj.time,
        };
        Products.create(tmpObj)

        ctx.body = {
            code: 1000,
            data: tmpObj,
            msg: '新增成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '新增失败'
        }
    }
}
let fn_put = async (ctx, next) => {

    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    console.log(obj);
    let product = await Products.findByPk(id);
//根据id寻找
    console.log(product);
    if (product) {

        await Products.update(obj, {
            where: {
                id: id
            }
        });
        ctx.body = {
            code: 1000,
            data: product,
            msg: '修改成功！'
        }

    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '修改失败'
        }

    }
}

let fn_delete =async (ctx, next) => {
    let id = ctx.request.params.id;
    // 删除
    let product = await Products.findByPk(id);

    if (product) {
       await Products.destroy({
            where:{
                id:id
            }
        })
        ctx.body = {
            code: 1000,
            data: { id: id },
            msg: '删除成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: '',
            msg: '删除失败'
        }
    }
}

module.exports = {
    'get /product': fn_product_list,
    'get /product/:id': fn_product_id, 
    'post /product': fn_post, 
    'put /product/:id': fn_put, 
    'delete /product/:id': fn_delete 
}