'use strict'
const koa=require('koa')
const bodyparser=require('koa-bodyparser')
const router=require('./router');
const cors=require('koa-cors')
const {sync}=require('./model')
const port=2000
const app=new koa();
app.use(cors())
app.use(bodyparser())
app.use(router())
sync();
app.listen(port,()=>{
    console.log(`服务器运行于：http://localhost:${port}`);
})