$(function () {
    //获取数据表头
    $.get('http://127.0.0.1:8000/head', (data) => {
        $('#top').append(data)
        //获取数据
        $.get('http://127.0.0.1:8000/data', (data) => {
            log(data)
        })
    })
})

//循环遍历数据
function log(data) {
    data.forEach(item => {
        let html = `
            <tr id="data">
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td>${item.digest}</td>
                <td>${item.content}</td>
                <td>${item.classify}</td>
                <td>${item.author}</td>
                <td>${item.time}</td>
                <td>
                    <input type="button" value="编辑" onclick="edit(${item.id})">
                    <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
        `
        $('#content').append(html)
    });
}
//查询
function query() {
    //获取关键字
    let keyword = $('[name=keyword]').val()
    //发生get请求
    $.get('http://127.0.0.1:8000/query?keyword=' + keyword, (data) => {
        //获取tr节点长度
        let tr = $('tr').length
        //循环删除
        for (let i = 0; i < tr; i++) {
            $('#data').remove()
        }
        //打印数据
        log(data)
    })


}
//删除
function del(id) {
    //删除判断
    if (confirm('确认删除该数据吗？')) {
        //发送get请求给后端
        $.get('http://127.0.0.1:8000/del?id=' + id, (data) => {
            //获取tr节点长度
            let tr = $('tr').length
            //循环删除
            for (let i = 0; i < tr; i++) {
                $('#data').remove()
            }
            //打印数据
            log(data)
        })  
    }

    
}
//新增
function add() {
    //移除表头
    $('#head').remove()
    //请求新增页面
    $.get('http://127.0.0.1:8000/addhtml', (data) => {
        $('#top').append(data)
    })
}
//保存
function save(Id) {
    //获取节点数据
    let id = $('[name=id]').val()
    let title = $('[name=title]').val()
    let digest = $('[name=digest]').val()
    let content = $('[name=content]').val()
    let classify = $('[name=classify]').val()
    let author = $('[name=author]').val()

    //为新增封装对象
    let obj = {
        title,
        digest,
        content,
        classify,
        author,
        time:'2022-04-10 08:47'
    }
    //为编辑封装对象
    let ob = {
        id,
        title,
        digest,
        content,
        classify,
        author,
        time:'2022-04-10 08:47'
    }
    //判断Id是否存在，存在为编辑，不存在为新增
    if (Id) {
        //将数据发送到编辑后端
        $.post('http://127.0.0.1:8000/edit',ob,(Data)=>{
            $('#addhtml').remove()
            $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)

                    log(Data)
            })
        })

    }else{
        if (title=='' ||digest==''||content=='' ||classify=='' ||author=='' ) {
            alert('no no no 不可以插入空数据!')
        }else{
            $.post('http://127.0.0.1:8000/add', obj, (Data) => {
                $('#addhtml').remove()
                $.get('http://127.0.0.1:8000/head', (data) => {
                    $('#top').append(data)
    
                        log(Data)
                })
            })
        }
        //将数据发送到新增后端
        
    }
}
//编辑
function edit(id){
    let keyword = id
    //利用查询找到数据
    $.get('http://127.0.0.1:8000/query?keyword=' + keyword, (data) => {
           let Data= data.filter((item)=>{
                return item.id ==id
            })

            
            Data.forEach((item)=>{
                //找到的数据封装对象
                let obj = {
                    id:item['id'],
                    title:item['title'],
                    digest:item['digest'],
                    content:item['content'],
                    classify:item['classify'],
                    author:item['author'],

                }

                //数据发送到新增页面
                $.post('http://127.0.0.1:8000/addhtml',obj,(data)=>{

                    $('#head').remove()

                    $('#top').append(data)
                })  
            })

    })
}
//取消
function cancel(){
    //移除编辑页面
    $('#addhtml').remove()
    
    $.get('http://127.0.0.1:8000/head', (data) => {
        $('#top').append(data)

        $.get('http://127.0.0.1:8000/data', (data) => {
            console.log(data);
            log(data)
        })
    })
}