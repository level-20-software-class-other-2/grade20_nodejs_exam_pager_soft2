'use strict';

$(function(){
    query();
})

$(function(){
    let [reqUrl,params]=window.location.href.split('?');
    let searchParams = new URLSearchParams(params);
    let id = searchParams.get('id');
    if(id){
        productById(id).then(data=>{
            $('#id').val(data[0].id);
            $('#title').val(data[0].title);
            $('#digest').val(data[0].digest);
            $('#content').val(data[0].content);
            $('#type').val(data[0].type);
            $('#author').val(data[0].author);
            $('#time').val(data[0].time);
        })
    }
})

function renderFn(arr){
    let row = $('.rowData');
    let tb = $('#tbData');
    row.remove();
    arr.forEach(item=>{
        let html = `
        <tr class="rowData" key="${item.id}">
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.digest}</td>
            <td>${item.content}</td>
            <td>${item.type}</td>
            <td>${item.author}</td>
            <td>${item.time}</td>
            <td>
                <input type="button" value="修改" onclick="edit(${item.id})" class="edit">
                <input type="button" value="删除" onclick="del(${item.id})" class="del">
            </td>
        </tr>
        `
        tb.append(html);
    })
}