'use strict';

//请求
$(function () {
    $.get('http://localhost:8000/product', data => {
        data.forEach(item => {
            let html = `
            <tr  class="rowData" key="${item.id}">
            <td>${item.id}</td>
            <td>${item.biaoti}</td>
            <td>${item.zhaiyao}</td>
            <td>${item.neirong}</td>
            <td>${item.fenlei}</td>
            <td>${item.zuozhe}</td>
            <td><input type="button" value="编辑" onclick="update(${item.id})">
            <input type="button" value="删除" onclick="del(${item.id})"></td>
            </tr>
            `
            $('#tbData').append(html);
        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          )
    })
})
//查询
function query() {
    let key = $('#key').val();
    getProductList(key).then(data =>{
        let row = $('.rowData')
        row.remove();
        data.forEach(item => {
            let html = `
            <tr class="rowData">
            <td>${item.id}</td>
            <td>${item.biaoti}</td>
            <td>${item.zhaiyao}</td>
            <td>${item.neirong}</td>
            <td>${item.fenlei}</td>
            <td>${item.zuozhe}</td>
            <td><input type="button" value="编辑" onclick="update(${item.id})">
            <input type="button" value="删除" onclick="del(${item.id})"></td>
            </tr>
            `
            $('#tbData').append(html);
        });
    })
}
//新增
function add() {
    location.href = './addOrEdit.html'
}
//编辑
function update(id) {
    location.href = `./addOrEdit.html?${id}`
}
//删除
function del(id) {
    delProductById(id).then(data => {
        let id = data.data.id;
        $(`[key=${id}]`).remove();
    });
}
//保存
function save() {
    let id = $('[name=id').val();
    let biaoti = $('[name=biaoti').val();
    let zhaiyao = $('[name=zhaiyao').val();
    let neirong = $('[name=neirong').val();
    let fenlei = $('[name=fenlei').val();
    let zuozhe = $('[name=zuozhe').val();
    let obj = {
        id,
        biaoti,
        zhaiyao,
        neirong,
        fenlei,
        zuozhe
    }
    if (obj.id) {
        putProduct(obj.id, id).then(data => {
            if (data.code === 1000) {
                location.href = './index.html'
            } else {
                alert(code.msg);
            }
        })
    } else {
        postProduct(obj).then(data => {
            if (data.code === 1000) {
                location.href = './index.html'
            } else {
                alert(code.msg);
            }
        })
    }
}
//取消
function cancel() {
    location.href = './index.html'
}

