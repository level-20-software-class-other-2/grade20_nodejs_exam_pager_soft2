'use strict'

const koa = require('koa');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors');
const router = require('./router/router')
const app = new koa();

const {sync} = require('./model')
sync();

app.use(bodyparser());
app.use(cors());
app.use(router.routes())
let port = 8000;

app.listen(port);