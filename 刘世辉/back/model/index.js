'use strict'

const fs = require('fs');

const { Sequelize, Datatypes, Op } = require('sequelize');

const sequelize = new Sequelize('pdx', 'postgres', 'xklxs666', {
    host: 'sdas.ltd',
    dialect: 'postgres'
})

let files = fs.readdirSync(__dirname);

let resFiles = files.filter(item=>{
    return item.endsWith('.js')&&item!=='index.js'
})

let resObj = {};
resFiles.forEach(item => {
    let ModelName = item.replace('.js', '');
    let tmpObj = require(__dirname + '/' + item)
    let tabaleName = ModelName.toLowerCase();
    resObj[ModelName] = sequelize.define(tabaleName, tmpObj);
})
//逻辑包装
resObj.Op = Op;

resObj.sync = async () => {
    sequelize.sync({ force: true }).then(() => {
        resObj.Product.bulkCreate([
            { title: '标题', digest: '摘要', content: '内容', classify: '分类',author:'作者',time:'时间' },
            { title: '论ef core的自我修养', digest: '论ef core的自我修养', content: '论ef core的自我修养', classify: '.net',author:'InCerry',time:'2022-04' },
            { title: 'DDD之我见', digest: 'DDD之我见', content: 'DDD之我见', classify: '编程技术',author:'某大神',time:'时间' },
            { title: 'nginx负载平衡的几种策略', digest: 'nginx负载平衡的几种策略', content: 'nginx负载平衡的几种策略', classify: '服务器',author:'老胡来也',time:'时间' },
            { title: 'Linux用户创建的学习研究', digest: 'Linux用户创建的学习研究', content: 'Linux用户创建的学习研究', classify: 'Linux',author:'某大神',time:'时间' },
            { title: '大数据仪表盘探讨', digest: '大数据仪表盘探讨', content: '大数据仪表盘探讨', classify: '大数据',author:'居家博士',time:'时间' },

        ])
    })
}

module.exports = resObj;
