
function getList(keyword) {
    return new Promise((res,rej)=>{
        $.get(`${baseUrl}/blog?keyword=${keyword}`,data=>{
            res(data)
        })

    })
    
}



function getId(id) {
    return new Promise((res,rej)=>{
        $.get(`${baseUrl}/${id}`,data=>{
            res(data)
        })

    })
    
}




function postObj(obj) {
    return new Promise((res,rej)=>{
        $.post(`${baseUrl}/blog`,obj,data=>{
            res(data)
        })

    })
    
}




function putIdObj(id,obj) {
    return new Promise((res,rej)=>{
        $.ajax({
            url:`${baseUrl}/blog/${id}`,
            type:"put",
            data:obj,
            success:data=>{
                res(data)
            }
        })

    }) 
}

function deleteId(id,obj) {
    return new Promise((res,rej)=>{
        $.ajax({
            url:`${baseUrl}/blog/${id}`,
            type:"delete",
            success:data=>{
                res(data)
            }
        })

    }) 
}



