var fs=require('fs')
var {Sequelize,DataTypes,Op}=require('sequelize')
var {db_database,db_dialect,db_host,db_password,db_username}=require('../config/db')
var sequelize=new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
})

let resObj={}

let files=fs.readdirSync(__dirname).filter(item=>{
    return item.endsWith('.js')&&item!='index.js'
})

files.forEach(item=>{
    let modelName=item.replace('.js','')
    let tableName=modelName.toLowerCase()
    let obj=require(__dirname+'/'+item)
    resObj[modelName]=sequelize.define(tableName,obj)
})

resObj.Op=Op

resObj.sync=async()=>{
    sequelize.sync({force:true}).then(()=>{
        resObj.Blogs.bulkCreate([{title:'asdas',digest:'sasad',content:'sadas',class:'asda',author:'nimd'},{title:'asdas',digest:'sasad',content:'sadas',class:'asda',author:'nimd'},])
    })
}
module.exports=resObj