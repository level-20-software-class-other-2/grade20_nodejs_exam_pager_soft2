const {Sequelize,Op,DataTypes}=require('sequelize')
const sequelize=new Sequelize('soft1','postgres','yml123456',{
    hooks:'ymlnb.top',
    dialect:'postgres'
})
let menglin={}
menglin['Table']=sequelize.define('blog',{
    biaoti:{
        type:DataTypes.STRING,
        allowNull:false
    },
    zhaiyao:{
        type:DataTypes.STRING,
        allowNull:false
    },
    neirong:{
        type:DataTypes.STRING,
        allowNull:false
    },
    fenlel:{
        type:DataTypes.STRING,
        allowNull:false
    },
    zuozhe:{
        type:DataTypes.STRING,
        allowNull:false
    },
    time:{
        type:DataTypes.STRING,
        allowNull:false
    }
})
menglin.sync=async()=>{
    await sequelize.sync({force:true})

    menglin.Table.bulkCreate([
        {
            biaoti:"论EF Core的自我修养",
            zhaiyao:"论EF Core的自我修养",
            neirong:"论EF Core的自我修养",
            fenlel:"Net",
            zuozhe:"InCerry",
            time:"2022-04-06 08:47"
        },
        {
            biaoti:"DD之我见",
            zhaiyao:"DD之我见",
            neirong:"DD之我见",
            fenlel:"编辑技术",
            zuozhe:"某大神",
            time:"2022-04-03 08:47"
        },
        {
            biaoti:"nginx负载平衡的几种策略",
            zhaiyao:"nginx负载平衡的几种策略",
            neirong:"nginx负载平衡的几种策略",
            fenlel:"服务器",
            zuozhe:"老胡来也",
            time:"2022-04-03 08:47"
        },
        {
            biaoti:"Linux用户创建的学习研究",
            zhaiyao:"Linux用户创建的学习研究",
            neirong:"Linux用户创建的学习研究",
            fenlel:"Linux",
            zuozhe:"某大神",
            time:"2022-04-03 08:47"
        },
        {
            biaoti:"大数据仪表盘讨论",
            zhaiyao:"大数据仪表盘讨论",
            neirong:"大数据仪表盘讨论",
            fenlel:"大数据",
            zuozhe:"居家博士",
            time:"2022-04-03 08:47"
        },
        
        
    ])
}
module.exports=menglin;