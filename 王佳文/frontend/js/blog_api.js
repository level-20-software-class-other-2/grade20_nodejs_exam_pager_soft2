'use strict'


//展示列表
function getBlogList(keyword){
    return new Promise(function (res,rej){
        $.get('http://localhost:8000/blog?keyword='+keyword,data=>{
            res(data)
        })
    })
}


//根据id
function getBlogId(id){
    return new Promise(function (res,rej){
        $.get(`http://localhost:8000/blog/${id}`,data=>{
            res(data)
        })
    })
}

//增加
function postBlog(obj){
    return new Promise(function (res,rej){
        $.post('http://localhost:8000/blog',obj,data=>{
            res(data)
        })
    })
}

//编辑
function putBlog(id,obj){
    return new Promise(function (res,rej){
        $.ajax({
            url:`http://localhost:8000/blog/${id}`,
            type:'put',
            data:obj,
            success:data=>{
                res(data)
            }
        })
    })
}

//删除
function deleteBlog(id){
    return new Promise(function (res,rej){
        $.ajax({
            url:`http://localhost:8000/blog/${id}`,
            type:'delete',
            success:data=>{
                res(data)
            }
        })
    })
}