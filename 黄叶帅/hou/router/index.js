const router = require('koa-router')();
const fs=require('fs')




function getrouter(filespath) {
    let path = filespath || __dirname;
    let files = fs.readdirSync(path);
    let routerfiles = files.filter(item => {
        return item.endsWith('.js') && item !== 'index.js'


    })
    return routerfiles;

}


function regrouter(router, routerfiles) {
    routerfiles.forEach(item => {
        let path = __dirname + '/' + item
        let obj = require(path);
        for (let key in obj) {
            let arr = key.split(' ');
            let rmethod = arr[0];
            let rpath = arr[1];
            let rfunction = obj[key];
            if (rmethod === 'get') {
                router.get(rpath, rfunction)
            } else if (rmethod === 'post') {
                router.post(rpath, rfunction)
            } else {

                console.log("路径不正确");
            }


        }
    });

    return router.routes();
}


module.exports=function(){

let routerfiles=getrouter();
let  fn= regrouter(router,routerfiles)
return fn
}