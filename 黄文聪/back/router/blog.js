let blogs=[
    {
        id:1,
        headline:"大数据仪表盘探讨",
        digest:'大数据仪表盘探讨',
        content:'大数据仪表盘探',
        classify:'大数据仪表盘探讨',
        author:"哈利波特",
        postedTime:"2022-02-01"

    },
    {
        id:2,
        headline:"大数据仪表盘探讨",
        digest:'大数据仪表盘探讨',
        content:'大数据仪表盘探',
        classify:'大数据仪表盘探讨',
        author:"哈利波特",
        postedTime:"2022-02-01"

    }
]

let getList=(ctx,next)=>{
    let keyword=ctx.request.query.keyword
    console.log(keyword);

   let list= blogs.filter(item=>{
        return item.id==keyword ||
        item.author==keyword ||
        item.classify==keyword ||
        item.content==keyword ||
        item.digest==keyword ||
        item.headline==keyword ||
        item.postedTime==keyword 
    })

    if (keyword) {
        if (list.length>0) {
            ctx.body={
                code:1000,
                data:list,
                mgs:"关键字返回数据成功"
            }
            
        } else {
            ctx.body={
                code:400,
                data:null,
                mgs:"根据关键字没有找到数据"
            }
            
        }
        
    } else {
        if (blogs.length>0) {
            ctx.body={
                code:1000,
                data:blogs,
                mgs:"全部数据返回成功"
            }
            
        } else {
            ctx.body={
                code:400,
                data:null,
                mgs:"没有一丁点数据"
            }
            
        }
        
    }

}



let getId=(ctx,next)=>{
    
}



let postObj=(ctx,next)=>{
    
}


let putIdObj=(ctx,next)=>{
    
}


let deleteId=(ctx,next)=>{
    let id=ctx.request.params.id
    console.log(id);

    let startLength=blogs.length;

    blogs=blogs.filter(item=>{
        return item.id !=id
    })

    let endLength =blogs.length

    if (endLength<startLength) {
        ctx.body={
            code:1000,
            data:blogs,
            mgs:"数据删除成功"
        }
        
    } else {
        ctx.body={
            code:400,
            data:blogs,
            mgs:"数据删除失败"
        }
        
    }
    
}


module.exports={
    "get /blog":getList,
    "get /blog/:id":getId,
    "post /blog":postObj,
    "put /blog/:id":putIdObj,
    "delete /blog/:id":deleteId,
}