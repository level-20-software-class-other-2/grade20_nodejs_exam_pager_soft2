'use strict'

const fs = require('fs')
const path = require('path')
const {Sequelize,Op} = require('sequelize')

const sequelize = new Sequelize('soft2db','postgres','zgsgbtb0120',{
    host:'hardwork.top',
    dialect:'postgres'
})

let file =fs.readdirSync(path.join(__dirname))

let tableFile = file.filter(item=>{
    return item.endsWith('.js') && item!== 'index.js'
})

let obj = {}
obj.Op = Op
tableFile.forEach(item => {
    let fileName = item.replace('.js','')
    let modelName = fileName.toLowerCase()
    let tableName = require(path.join(__dirname,item))
    obj[fileName] = sequelize.define(modelName,tableName)
});

obj.sync=()=>{
    sequelize.sync({force:true}).then(res=>{
        obj.Blog.bulkCreate([
            {
                title:'qq',
                digest:'qq1',
                content:'qq2',
                type:'qq3',
                author:'qq4'
            },
            {
                title:'ww',
                digest:'ww1',
                content:'ww3',
                type:'ww4',
                author:'ww5qq4'
            },
            {
                title:'ee1',
                digest:'ee2',
                content:'ee3',
                type:'ee4',
                author:'rr5'
            },
        ])
    })
}

module.exports = obj