'use strict'

const router = require('koa-router')();
const fs = require('fs');

function getRoute(filePath) {
    let tmpPath = filePath || __dirname;
    let file = fs.readdirSync(tmpPath);
    let routeFile = file.filter(item => {
        return item.endsWith('.js') && item !== 'index.js';
    })
    return routeFile;
}
function registryRoute(router, routeFile) {
    routeFile.forEach(item => {
        let tmpPath = __dirname + '/' + item;
        console.log(tmpPath);
        let obj = require(tmpPath);
        for (let key in obj) {
            let tmpArr=key.split(' ');
            let rMethod=tmpArr[0];
            let rPath = tmpArr[1];
            let rFunction = obj[key];
            if(rMethod==='get'){
                router.get(rPath,rFunction);
            }else if(rMethod==='post'){
                router.post(rPath,rFunction);
            }else if(rMethod==='put'){
                router.put(rPath,rFunction);
            }else if(rMethod==='delete'){
                router.delete(rPath,rFunction);
            }else{
                console.log('不正确的请求方法或路径');
            }
        }
    });
    return router.routes();
}
module.exports = function () {
    let routeFile = getRoute();
    let a = registryRoute(router, routeFile);
    return a;
};