const koa =require('koa');
const cors =require('koa-cors');
const bodyparser =require('koa-bodyparser');
const router =require('koa-router')();


const {DataTypes,Op,Sequelize, where, NOW} = require('sequelize');
const { time, timeLog } = require('console');

const sql = new Sequelize('nodejsdemo','postgres','1589953126_j',{
    
host:"shixinya.top",

dialect:'postgres'

})


let Product =sql.define('product',{

    Title:{

        type:DataTypes.STRING,
    
        allowNull:false
    
      },
      Digest:{
    
        type:DataTypes.STRING,
    
        allowNull:false
    
      },
      Content:{
    
        type:DataTypes.STRING,
    
        allowNull:false
    
      },
      Classify:{
    
        type:DataTypes.STRING,
    
        allowNull:false
    
      },
      Author:{
    
        type:DataTypes.STRING,
    
        allowNull:false
    
      },
      Time:{
    
        type:DataTypes.STRING,
    
        allowNul:true
    
      }

})


sql.sync({force:true}).then(()=>{

    Product.bulkCreate([

        {
            Title :'论EF Core的自我修养',
            Digest   :'论EF Core的自我修养',
            Content  :'论EF Core的自我修养',
            Classify   :'.Net',
            Author  :'InCerry',
            Time :'2022-04-06 08:47',

        },
        {
            Title :'DDD至我见',
            Digest   :'DDD至我见',
            Content  :'DDD至我见',
            Classify   :'编程技术',
            Author  :'某大神',
            Time :'2022-04-03 23:47',

        },
        {
            Title :'nginx负平衡的几种策略',
            Digest   :'nginx负平衡的几种策略',
            Content  :'nginx负平衡的几种策略',
            Classify   :'服务器',
            Author  :'老胡来也',
            Time :'2022-04-06 08:47',

        },
        {
            Title :'Linux用户创建的学习研究',
            Digest   :'Linux用户创建的学习研究',
            Content  :'Linux用户创建的学习研究',
            Classify   :'Linux',
            Author  :'某大神',
            Time :'2022-04-06 08:47',

        },
        {
            Title :'大数据仪表探讨',
            Digest   :'大数据仪表探讨',
            Content  :'大数据仪表探讨',
            Classify   :'大数据',
            Author  :'居家博士',
            Time :'2022-04-18 16:18',

        }


    ])


})


router.get('/product',async(ctx,next)=>{

    let arr =await Product.findAll();

    ctx.body=arr
})


router.get('/product/:id',async(ctx,next)=>{

    let keyword = ctx.request.params.id

    console.log(keyword);

    let arr =await Product.findAll({

        where:{

            [Op.or]:[

                {id:isNaN(keyword) ? -1 : parseFloat(keyword)},
                {Title :keyword},
                {Digest  :keyword},
                {Content :keyword},
                {Classify  :keyword},
                {Author :keyword},
                {Time:keyword}
            ]

        }

    });

    ctx.body=arr
})



router.delete('/product',async(ctx,next)=>{

    let id  =ctx.request.body.id

    let arr =await Product.destroy({

        where:{

            id

        }

    });

    ctx.body=arr
})



router.put('/product',async(ctx,next)=>{

    let obj  =ctx.request.body

    let newObj={

        Title :obj.Title ,
        Digest   :obj.Digest   ,
        Content  :obj.Content  ,
        Classify   :obj.Classify   ,
        Author  :obj.Author  ,

    }

    await Product.update(newObj,{

        where:{

            id:obj.id

        }

    });

    let arr =await Product.findAll();


    ctx.body=arr
})



router.post('/product',async(ctx,next)=>{

    let obj  =ctx.request.body
    await Product.create(obj)


    let arr =await Product.findAll();

    ctx.body=arr
})



const app = new koa();



app.use(cors())
app.use(bodyparser())
app.use(router.routes())

app.listen(3000);

console.log(`http://localhost:3000`);



