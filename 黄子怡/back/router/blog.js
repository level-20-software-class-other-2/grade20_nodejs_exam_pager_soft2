'use strict';

const {Blogs,Op} =require('../model')

let fn_blog_list=async(ctx,next) =>{

    let keyword=ctx.request.query.keyword || '';

    if (keyword) {
        let list=await Blogs.findAll({
            where:{
                [Op.or]:[
                    {tiele:isNaN(keyword) ?0:parseInt(keyword)},
                    {tips:isNaN(keyword) ?0:parseInt(keyword)},
                    {content:isNaN(keyword) ?0:parseInt(keyword)},
                    {type:isNaN(keyword) ?0:parseInt(keyword)},
                    {author:isNaN(keyword) ?0:parseInt(keyword)},
                    {time:keyword}
                ]
            },
            order:['id']
        })
        ctx.body=list;

    }else{
        let list=await Blogs.findAll({
            order:['id']
        });
        ctx.body=list;
    }
}

let fn_blog_id=async(ctx,next)=>{
    let id =ctx.request.params.id;
    let arr=await Blogs.findByPk(id)
    if(arr){
        ctx.body={
            code:1000,
            data:arr,
            msg:'获取成功'
        }
    }else{
        ctx.body={
            code:400,
            data:null,
            msg:'获取失败'
        }
    }
}

let fn_post=(ctx,next)=>{

    let obj=ctx.request.body;

    if (obj.hasOwnProperty('id') &&obj.hasOwnProperty('title') && obj.hasOwnProperty('tips') &&obj.hasOwnProperty('content')&& obj.hasOwnProperty('type') &&obj.hasOwnProperty('author')&&obj.hasOwnProperty('time')&&obj.hasOwnProperty('supplier')
    && obj.id && obj.title && obj.tips && obj.content && obj.type &&obj.author &&obj.time&&obj.supplier) {
      
        let tmpObj={
            id:obj.id,
            title:obj.title,
            tips:obj.tips,
            content:obj.content,
            type:obj.type,
            author:obj.author,
            time:obj.time,
            supplier:obj.supplier,
        };

       Products.create(tmpObj)

        ctx.body={
            code:1000,
            data:tmpObj,
            msg:'新增成功'
        }
    }else{
        ctx.body={
            code:400,
            data:null,
            msg:'新增失败'
        }
    }
}

let fn_put=async(ctx,next)=>{

    let id = ctx.request.params.id;

    let obj = ctx.request.body;

    let blog =await Blogs.findByPk(id);

    if (blog) {
        await Blogs.update(obj,{
            where:{
                id:id
            }
        });
        ctx.body = {
            code: 1000,
            data: blog,
            msg: '更新成功'
        }

    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '更新失败'
        }

    }

}

let fn_delete=async(ctx,next)=>{
    
    let id=ctx.request.params.id;


    let blog=await Blogs.findByPk(id);

    if (blog) {
       await Blogs.destroy({
           where:{
               id:id
           }
       })
        ctx.body={
            code:1000,
            data:{id:id},
            msg:'删除成功'
        }

        }else{
            ctx.body={
                code:400,
                data:'',
                msg:'删除失败'
            }
    }
}


module.exports={
    'get /blog':fn_blog_list,
    'get /blog/:id':fn_blog_id,
    'post /blog':fn_post,
    'put /blog/:id':fn_put,
    'delete /blog/:id':fn_delete
}