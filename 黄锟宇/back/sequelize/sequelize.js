const { Sequelize, Op } = require('sequelize')

const fs = require('fs')
const path = require('path')



const sequelize = new Sequelize('soft2', 'postgres', '113', {
  host: 'www.unname.top',
  dialect: 'postgres'
})


let obj = {}

obj.Op = Op

let files = fs.readdirSync(path.join(__dirname))

tableFile = files.filter((item) => {

  return item !== 'sequelize.js' && item.endsWith('.js')

})

tableFile.forEach(item => {
  let fileName = item.replace('.js', '')

  let name = fileName.toLowerCase()

  let table = require(path.join(__dirname, item))


  obj[fileName] = sequelize.define(name, table)

});


//sequelize.sync({force:true})



obj.sequelizeSync = () => {

  sequelize.sync({ force: true }).then(res => {

    obj.Blog.bulkCreate(
      [
        {
          title: '论EF Core的自我修养',
          abstract: '论EF Core的自我修养',
          content: '论EF Core的自我修养',
          type:'.Net',
          author:'InCerry',
          postedTime:'2022-04-06 08:47'
        },
        {
            title: 'DDD之我见',
            abstract: 'DDD之我见',
            content: 'DDD之我见',
            type:'编程技术',
            author:'某大神',
            postedTime:'2022-04-03 23：47'
          },
          {
            title: 'nginx负载平衡的几种策略',
            abstract: 'nginx负载平衡的几种策略',
            content: 'nginx负载平衡的几种策略',
            type:'服务器',
            author:'老胡来也',
            postedTime:'2022-04-06 08：47'
          },
          {
            title: 'Linux用户创建的学习研究',
            abstract: 'Linux用户创建的学习研究',
            content: 'Linux用户创建的学习研究',
            type:'Linux',
            author:'某大神',
            postedTime:'2022-04-06 08:47'
          },
          {
            title: '大数据仪表盘探讨',
            abstract: '大数据仪表盘探讨',
            content: '大数据仪表盘探讨',
            type:'大数据',
            author:'居家博士',
            postedTime:'2022-04-18 16:18'
          }
      ]
    )

  })
}


module.exports = obj