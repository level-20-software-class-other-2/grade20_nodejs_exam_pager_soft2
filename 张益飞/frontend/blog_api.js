let baseUrl = "http://localhost:8000/blog"


//查找或显示所有数据
function getbloglist(keyword){
    return new Promise((resolve,reject)=>{
        $.get(`${baseUrl}?keyword=${keyword}`,data=>{
            resolve(data)
        })
     }
    )}

//删除数据
 function delblog(id){
     return new Promise((resolve,reject)=>{
         $.ajax({
             url:`${baseUrl}/${id}`,
             type:"delete",
             success:data=>{
                 resolve(data);
             }
         })
     })
 }   