const koa = require('koa')
const bodyparser = require('koa-bodyparser')
const cors = require('koa-cors')

const router = require('./router/router')
const {sync} = require('./sequelize/sequelize')
sync()

const app = new koa()
app.use(cors())
app.use(bodyparser())
app.use(router.routes())


app.listen(8000)