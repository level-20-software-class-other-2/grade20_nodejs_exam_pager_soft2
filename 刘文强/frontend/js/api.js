'use strict'
function getblogs(keyword) {
    return new Promise(function (reslove, reject) {
        $.get('http://localhost:8000/blogs?keyword=' + keyword, data => {
            reslove(data)
        })
    })
}
function getblogss(id){
    return new Promise(function(reslove,reject){
        $.get('http://localhost:8000/blogs?id='+id , data=>{
            reslove(data)
        })
    })
}
function postblogs(obj) {
    return new Promise(function (reslove, reject) {
        $.post('http://localhost:8000/blogs', obj, data => {
            reslove(data)
        })
    })
}
function putblogs(obj, id) {
    return new Promise(function (reslove, reject) {
        $.ajax({
            url: `http://localhost:8000/blogs/${id}`,
            type: 'put',
            data: obj,
            success: data => {
                reslove(data)
            }
        })
    })
}
function delblogs(id) {
    return new Promise(function (reslove, reject) {
        $.ajax({
            url: `http://localhost:8000/blogs/${id}`,
            type: 'delete',

            success: data => {
                reslove(data)
            }
        })
    })
}