'use strict'

let fn_head = (ctxt)=>{
    let html =`
    <div id="all">
        <tr>
            <td>
                <input type="text" name="keyword">
                <input type="button" value="查找" onclick="query()">
                <input type="button" value="添加" onclick="add()">
            </td>
        </tr>
        <table id="content">
            <th>Id</th>
            <th>标题</th>
            <th>摘要</th>
            <th>内容</th>
            <th>分类</th>
            <th>作者</th>
            <th>发表时间</th>
            <th>操作</th>
        </table>
    </div>
    `
    ctxt.body = html
}
let get = [['/head',fn_head]]

module.exports = {
    "get":get
}