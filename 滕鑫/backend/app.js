'use strict'
const koa=require('koa');
const router=require('./router');
const cors=require('koa-cors');
const bodyparser=require('koa-bodyparser');
const{sync}=require('./model');
let app=new koa();
sync();
app.use(cors());
app.use(bodyparser());
app.use(router());
let a=8000;
app.listen(a);