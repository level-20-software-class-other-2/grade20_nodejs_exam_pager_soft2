'use strict';
const Koa=require('koa');
const router=require('koa-router');
const cors=require('koa-cors')
const bodyparser=require('koa-bodyparser');
const{Sync}=require('./model/index')

let app=new Koa();
// Sync()
app.use=(router());
app.use=(cors());
app.use=(bodyparser());

let post=8000;
app.listen(post);
console.log(`http://localhost:${post}`);