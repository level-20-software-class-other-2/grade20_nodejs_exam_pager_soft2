'use strict';

//查找
function query(){
    let keyword = $('#keyword').val();
    productList(keyword).then(data=>{
        renderFn(data);
    })
}

//添加
function add(){
    window.location.href='./addOrEdit.html'
}

//保存
function save(){
    let obj = {
        id:$('#id').val(),
        title:$('#title').val(),
        digest:$('#digest').val(),
        content:$('#content').val(),
        type:$('#type').val(),
        author:$('#author').val(),
        time:$('#time').val(),
    }
    if(obj.id){
        editProduct(obj.id,obj).then(data=>{
            window.location.href='./index.html'
        })
    }else{
        addProduct(obj).then(data=>{
            if(data.code===1000){
                window.location.href='./index.html'
            }else{
                alert(data.msg)
            }
        })
    }
}

//取消
function cancel(){
    window.location.href='./index.html'
}

//编辑
function edit(id){
    window.location.href=`addOrEdit.html?id=${id}`
}

//删除
function del(id){
    if(confirm('确认删除？')){
        delProduct(id).then(data=>{
            let id = data.data.id;
            $(`[key=${id}]`).remove();
        }).then(data=>{
            window.location.href='./index.html'
        })
    }
}