'use strict'
const {Products,Op}=require('../model');
let fn_product_list=async(ctx,next)=>{
    let keyword=ctx.request.query.keyword||'';
    if(keyword){
        let list=await Products.findAll({
            where:{
                [Op.or]:[
                        {id:isNaN(keyword)?0:parseInt(keyword)},
                        //标题
                        {headline:keyword},
                        //摘要
                        {abstract:keyword},
                        //内容
                        {details:keyword},
                        //分类
                        {classify:keyword},
                        //作者
                        {author:keyword}
                ]
            },
            order:['id']
        })
        ctx.body=list;
    }else{
        let list=await Products.findAll({
            order:['id']
        });
        ctx.body=list;
    }
}
let fn_product_id=async(ctx,next)=>{
    let id=ctx.request.params.id;
    let arr=await Products.findByPk(id);
    if(arr){
        ctx.body={
            code:1000,
            data:arr,
            msg:'获取成功'
        }
    }else{
        ctx.body={
            code:400,
            data:null,
            msg:'获取失败'
        }
    }
}
let fn_post=(ctx)=>{
    let obj=ctx.request.body;
    if(obj.hasOwnProperty('headline')&&obj.hasOwnProperty('abstract')&&obj.hasOwnProperty('details')&&obj.hasOwnProperty('classify')&&obj.hasOwnProperty('author')&&
    obj.headline&&obj.abstract&&obj.details&&obj.classify&&obj.author){
        let tmpobj={
            headline:obj.headline,
            abstract:obj.abstract,
            details:obj.details,
            classify:obj.classify,
            author:obj.author,
        };
        Products.create(tmpobj)
        ctx.body={
                code:1000,
                data:tmpobj,
                msg:'新增成功'
            }
        }else{
            ctx.body={
                code:400,
                data:null,
                msg:'数据验证失败'
            }
        }
    }
let fn_put=async(ctx,next)=>{
    let id=ctx.request.params.id;
    let obj=ctx.request.body;
    let product=await Products.findByPk(id);
    if(product){
        await Products.update(obj,{
            where:{
                id:id
            }
        });
        ctx.body={
            code:1000,
            data:product,
            msg:'修改成功'
        }
    }else{
        ctx.body={
            code:400,
            data:null,
            msg:'修改失败'
        }
    }
}
let fn_delete=async(ctx,next)=>{
    let id=ctx.request.params.id;
    let product=await Products.findByPk(id);
    if(product){
        await Products.destroy({
            where:{
                id:id
            }
        })
        ctx.body={
            code:1000,
            data:{id:id},
            msg:'删除成功'
        }
    }else{
        ctx.body={
            code:400,
            data:'',
            msg:'删除失败'
        }
    }
}
module.exports={
    'get /product':fn_product_list,//列表
    'get /product/:id':fn_product_id,//获取id
    'post /product':fn_post,//添加
    'put /product/:id':fn_put,//修改
    'delete /product/:id':fn_delete//删除
}