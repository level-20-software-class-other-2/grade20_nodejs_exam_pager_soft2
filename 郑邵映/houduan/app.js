const fs = require('fs')
// const router=require('koa-router')()
const router = require('./routers/router')
const bodyparser = require('koa-bodyparser')
const cors = require('koa-cors')
const koaCors = require('koa-cors')
const koa = require('koa')
const { sync } = require('./model/index')
let app = new koa()
app.use(cors())
app.use(bodyparser())
app.use(router.routes())
// sync()
let port = 3000;
app.listen(port);
console.log(`http://localhost:${port}`);