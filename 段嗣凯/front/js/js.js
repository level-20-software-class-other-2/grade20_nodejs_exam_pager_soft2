$(function () {
    $.get('http://localhost:2000/login',data=>{
        fn_data(data)
    })
})

function fn_data(data) {
    data.forEach(item => {
        let html=`
            <tr class="rowdata">
                <td>${item.tit}</td>
                <td>${item.abs}</td>
                <td>${item.cont}</td>
                <td>${item.clas}</td>
                <td>${item.writer}</td>
                <td>${item.time}</td>
                <td><input type="button" value="编辑" onclick="bianji(${item.id})"> </td>
                <td><input type="button" value="删除" onclick="shanchu(${item.id})"> </td>
            </tr>
        `
        $('#tb').append(html)
    });
}

function chaxun() {
    let a=$('#text').val()
    find1().then(data=>{
        if (a) {
            let arr=data.filter(item=>{
                return item.id==a || item.name==a
            })
            find2(arr)
        } else {
            find2(data)
        }
    })
}

function find1() {
    return new Promise(function (res,rej) {
        $.get('http://localhost:2000/login',data=>{
            res(data)
        })
    })
}

function find2(arr) {
    let row=$('.rowdata')
    row.remove()
    arr.forEach(item => {
        let html=`
            <tr class="rowdata">
            <td>${item.tit}</td>
            <td>${item.abs}</td>
            <td>${item.cont}</td>
            <td>${item.clas}</td>
            <td>${item.writer}</td>
            <td>${item.time}</td>
                <td><input type="button" value="编辑" onclick="bianji(${item.id})"> </td>
                <td><input type="button" value="删除" onclick="shanchu(${item.id})"> </td>
            </tr>
        `
        $('#tb').append(html)
    });
}

function xinzen() {
    window.location.href='./kongzhi.html'
}

function bianji(id) {
    window.location.href=`./kongzhi.html?id=${id}`
}

function baocun() {
    let url=location.search
    let id=(url.split('='))[1]
    if (id) {
        let tit=$('#tit').val()
        let abs=$('#abs').val()
        let cont=$('#cont').val()
        let clas=$('#clas').val()
        let writer=$('#writer').val()
        let time=$('#time').val()
        let obj={
            id,
            tit,
            abs,
            cont,
            clas,
            writer,
            time
        }
        $.ajax({
            type:"put",
            url:'http://localhost:2000/login',
            data:obj,
            dataType:"text",
            success:function (data) {
                window.location.href='./index.html'
            }
        })
    } else {
        let tit=$('#tit').val()
        let abs=$('#abs').val()
        let cont=$('#cont').val()
        let clas=$('#clas').val()
        let writer=$('#writer').val()
        let time=$('#time').val()
        let obj={
            tit,
            abs,
            cont,
            clas,
            writer,
            time
        }
        $.post('http://localhost:2000/login',obj,(res)=>{
            location.href='./index.html'
        })
    }
}

function quxiao() {
    window.location.href='./index.html'
}

function shanchu(id) {
    let a=confirm('确认删除')
    if (a) {
        $.ajax({
            type:"delete",
            url:`http://localhost:2000/login/${id}`,
            success:function (data) {
                $('.rowdata').remove()
                fn_data(data)
            }
        })
    }
}