'use strict';

$(
    async () => {
        var id = getUrlParam('id');
        if (id) {
            document.title = '修改';
            let data = await get(`${blogUrl}/${id}`);
            $('#title').val(data.title);
            $('#digest').val(data.digest);
            $('#content').val(data.content);
            $('#classification').val(data.classification);
            $('#author').val(data.author);

            $('#save').click(async () => {
                let data = {
                    title:$('#title').val(),
                    digest:$('#digest').val(),
                    content:$('#content').val(),
                    classification:$('#classification').val(),
                    author:$('#author').val()
                }
                
                await put(`${blogUrl}/${id}`, data);

                location.href = '.';
                }
            )
        } else {
            $('#save').click(async () => {
                let data = {
                    title:$('#title').val(),
                    digest:$('#digest').val(),
                    content:$('#content').val(),
                    classification:$('#classification').val(),
                    author:$('#author').val()
                }
                
                await post(blogUrl,data);

                location.href = '.';
                }
            )
        }
    },
    $('#cancel').click(() => {
        location.href = '.';
    })
)