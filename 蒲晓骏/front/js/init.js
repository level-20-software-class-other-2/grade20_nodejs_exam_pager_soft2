$(function () {
    $.get('http://localhost:8000/head', (data) => {
        $('#top').append(data)

        $.get('http://localhost:8000/show', (data) => {
            print(data)
        })
    })
})

function print(data) {
    data.forEach(item => {
        let html = `
        <tr id ="html">
        <td>${item.id}</td>
        <td>${item.title}</td>
        <td>${item.abstract}</td>
        <td>${item.content}</td>
        <td>${item.classify}</td>
        <td>${item.author}</td>
        <td>${item.postedTime}</td>
        <td>
        <input type="button" value="修改" onclick="Edit(${item.id})">
        <input type="button" value="删除" onclick="Delete(${item.id})">

        </td>
        </tr>
        `
        $('#content').append(html)
    });

}
//查
function query() {
    let keyword = $('[name=keyword]').val();

    $.get('http://localhost:8000/query?keyword=' + keyword, (data) => {

        let tr = $('tr');

        if (data.length < 1) {

            alert('表中无数据')
        } else {
            for (let i = 0; i < tr.length; i++) {
                $('#html').remove();
            }
            print(data)
        }

    })
}

//增
function add() {
    $.get('http://localhost:8000/addhtml', (data) => {
        $('#all').remove()
        $('#html').remove()
        $('#top').append(data)
    })
}
//删
function Delete(id) {
    if (confirm('是否删除')) {
     $.get('http://localhost:8000/indexDeleted?id=' + id, (data) => {
         let tr = $('tr')
         for (let i = 0; i < tr.length; i++) {
             $('#html').remove()
         }
         print(data)
     })
    }
 }
//改
function Edit(id) {
    $.get('http://localhost:8000/show', (data) => {
        let Data = data.filter((item) => {
            return item.id == id
        })

        Data.forEach((item) => {
            let newId = item['id']
            let newTitle = item['title']
            let newAbstract = item['abstract']
            let newContent = item['content']
            let newClassify = item['classify']
            let newAuthor = item['author']
            let newPostedTime = item['postedTime']

            let obj = {
                newId,
                newTitle,
                newAbstract,
                newContent,
                newClassify,
                newAuthor,
                newPostedTime
            }

            $.post('http://localhost:8000/addhtml', obj, (data) => {
                $('#all').remove()
                let tr = $('tr').length
                for (let i = 0; i < tr; i++) {
                    $('#html').remove()
                }
                $('#top').append(data)
            })
        })
    })
}

//保存
function save(Id) {
    let id = $('[name=id]').val()
    let title = $('[name=title]').val()
    let abstract = $('[name=abstract]').val()
    let content = $('[name=content]').val()
    let classify = $('[name=classify]').val()
    let author = $('[name=author]').val()
    let postedTime = $('[name=postedTime]').val()

    let obj = {
        id,
        title,
        abstract,
        content,
        classify,
        author,
        postedTime
    }
    console.log(obj);
    if (Id == undefined) {
        $.post('http://localhost:8000/indexadd', obj, (Data) => {
            $('#add').remove()
            $.get('http://localhost:8000/head', (data) => {
                $('#top').append(data)
                print(Data)
            })
        })

    } else {
        $.post('http://localhost:8000/indexedit', obj, (Data) => {
            $('#add').remove()
            $.get('http://localhost:8000/head', (data) => {
                $('#top').append(data)
                print(Data)

            })
        })
    }
}
//取消
function cel() {
    $('#add').remove()
    $.get('http://localhost:8000/head', (data) => {
        $('#top').append(data)
        $.get('http://localhost:8000/show', (data) => {
            console.log(data);
            print(data);
        })
    })
}