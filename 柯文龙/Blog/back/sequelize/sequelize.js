const { Sequelize, Op } = require('sequelize')

const fs = require('fs')
const path = require('path')

const sequelize = new Sequelize('text', 'postgres', '18279203063', {
    host: 'cowlong.top',
    dialect: 'postgres'
})
let obj = {}

obj.Op = Op

let files = fs.readdirSync(path.join(__dirname))
tableFile = files.filter((item) => {
    return item !== 'sequelize.js' && item.endsWith('.js')
})

tableFile.forEach(item => {
    let fileName = item.replace('.js', '')

    let name = fileName.toLowerCase()

    let table = require(path.join(__dirname, item))

    obj[fileName] = sequelize.define(name, table)
});
obj.sequelizeSync = () => {
    sequelize.sync({ force: true }).then(res => {
        obj.Book.bulkCreate(
            [
                {
                    name: '论EF Core的自我修养',
                    zy: '论EF Core的自我修养',
                    nr: '论EF Core的自我修养',
                    Category:'.Net',
                    author:'InCerry',
                },
                {
                    name: 'DDD 之我见',
                    zy: 'DDD 之我见',
                    nr: 'DDD 之我见',
                    Category:'编程技术',
                    author:'某大神',
                },
                {
                    name: 'nginx 负责平衡得的几种策略',
                    zy: 'nginx 负责平衡得的几种策略',
                    nr: 'nginx 负责平衡得的几种策略',
                    Category:'服务器',
                    author:'老胡来也',
                },
                {
                    name: 'Linux 用户创建的学习研究',
                    zy: 'Linux 用户创建的学习研究',
                    nr: 'Linux 用户创建的学习研究',
                    Category:'Linux',
                    author:'某大神',
                },
                {
                    name: '大数据仪表盘探讨',
                    zy: '大数据仪表盘探讨',
                    nr: '大数据仪表盘探讨',
                    Category:'大数据',
                    author:'居家博士',
                },
            ]
        )
    })
}

module.exports = obj