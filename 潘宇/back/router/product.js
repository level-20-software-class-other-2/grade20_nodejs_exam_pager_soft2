"use strict"
const {Product,Op} = require("../model");



// 商品的数据列表，可能存在关键字查找的情况
let fn_product_list = async (ctx, next) => {
    let keyword = ctx.request.query.keyword || '';
    if (keyword) {
        let list = await Product.findAll({
            where:{
                [Op.or]:[
                    {id:isNaN(keyword) ? 0 : parseInt(keyword)},
                    {headline:keyword},
                    {abstract:keyword},
                    {details:keyword},
                    {classify:keyword},
                    {author:keyword},
                    // {updata:keyword},
                ]
            }
        })
        ctx.body=list
    }else{
        let list = await Product.findAll({
            order:['id']
        })
        ctx.body=list;
    }
}
// 用于产品详情页面，返回的是某个指定id的产品的详细信息
let fn_product_id = async (ctx, next) => {
    let id = ctx.request.params.id;
    console.log(id);
    await Product.findByPk(id).then((res)=>{
        ctx.body=res
    })
}
//添加
let fn_post = async(ctx, next) => {
    let data = ctx.request.body
    console.log(data);
   let max = 0;
   await Product.findAll().then((res)=>{
       res.forEach((item)=>{
           if (max<item.id) {
               max=item.id
           }
       })
   })

   data.id=max+1;

   await Product.create(data)

    ctx.body='product';

}
//编辑
let fn_put = async (ctx, next) => {
    let obj = ctx.request.body;
    console.log(obj);
    await Product.findByPk(obj.id).then(async(res)=>{
        await res.update({
            id:obj.id,
            headline:obj.headline,
            abstract:obj.abstract,
            details:obj.details,
            classify:obj.classify,
            author:obj.author,
            // updata:obj.updata
        })
    })
    ctx.body='product';
}
//删除
let fn_delete =async (ctx, next) => {
    let id = ctx.request.params.id;

    Product.destroy({
        where:{
            id:id
        }
    })


    ctx.body='products';
}

module.exports = {
    'get /product': fn_product_list,// 用于产品列表页面，返回列表信息
    'get /product/:id': fn_product_id, // 用于产品详情页面，返回的是某个指定id的产品的详细信息
    'post /product': fn_post, // 用于产品新增，增加一条产品数据
    'put /product': fn_put, //用于产品修改，修改指定id的产品数据
    'delete /product/:id': fn_delete // 用于删除指定id的产品信息
}