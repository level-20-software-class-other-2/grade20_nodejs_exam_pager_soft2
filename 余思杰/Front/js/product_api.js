'use strict';

//商品列表
function productList(keyword){
    return new Promise(function(res,rej){
        $.get('http://localhost:8000/product?keyword='+keyword,data=>{
            res(data);
        })
    })
}

//id
function productById(id){
    return new Promise(function(res,rej){
        $.get(`http://localhost:8000/product/${id}`,data=>{
            res(data)
        })
    })
}

//添加
function addProduct(obj){
    return new Promise(function(res,rej){
        $.post('http://localhost:8000/product',obj,data=>{
            res(data)
        })
    })
}

//编辑
function editProduct(id,obj){
    return new Promise(function(res,rej){
        $.ajax({
            url:`http://localhost:8000/product/${id}`,
            type:'put',
            data:obj,
            success:(data)=>{
                res(data)
            }
        })
    })
}

//删除
function delProduct(id){
    return new Promise(function(res,rej){
        $.ajax({
            url:`http://localhost:8000/product/${id}`,
            type:'delete',
            success:data=>{
                res(data)
            }
        })
    })
}