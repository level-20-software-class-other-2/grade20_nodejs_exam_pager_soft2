'use strict'

const Koa = require('koa')
const bodyparser = require('koa-bodyparser')
const cors = require('koa-cors')
const router = require('./router')
const sequelize = require('sequelize')
const blogs = require('./model')

let sequelize = new Sequelize('soft2db','postgres','i02BIth18tH@lhT_ao',{
    dialect:'postgres',
    host:'godme.top'
})



let app = new Koa()
app.use(cors())
app.use(bodyparser())
app.use(router.routes())

let port = 8000
app.listen(port)
console.log(`server is running http://localhost:8000`);