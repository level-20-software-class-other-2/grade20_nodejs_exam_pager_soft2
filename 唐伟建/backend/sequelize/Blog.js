const {DataTypes} = require('sequelize')

let blog = {
    title:{
        type:DataTypes.STRING
    },
    digest:{
        type:DataTypes.STRING
    },
    content:{
        type:DataTypes.STRING
    },
    classify:{
        type:DataTypes.STRING
    },
    author:{
        type:DataTypes.STRING
    },
    time:{
        type:DataTypes.STRING
    },

}

module.exports=blog