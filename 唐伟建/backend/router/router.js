const router = require('koa-router')()
const fs = require('fs')
const path = require('path')

let files = fs.readdirSync(path.join(__dirname))


let jsFiles = files.filter((item)=>{
    return item.endsWith('.js') && item!=='router.js'
})


for(let name of jsFiles){

    let js = require(path.join(__dirname,name))

    for(let key in js ){
        if(key=='GET'){
            for (let i = 0; i < js[key].length; i++) {
                router.get(js[key][i][0],js[key][i][1])
                
            }
        }
        if(key=='POST'){
            for (let i = 0; i < js[key].length; i++) {
                router.post(js[key][i][0],js[key][i][1])
            }
        }
    }
}

module.exports=router