'use strict'

//封装路由
const router = require('koa-router')();
const fs = require('fs')

let filePath = __dirname;
let Files = fs.readdirSync(filePath)

let resFiles = Files.filter(item => {
    return item.endsWith('.js') && item !== 'idenx.js'
})


resFiles.forEach(item=>{
    resgiterRouter(item)
})

function resgiterRouter(resFiles){
    let tmp=__dirname+'/'+resFiles
    const a=require(tmp)
    for(let x in a){
        let tmpArr=x.split(' ')
        let rMethod=tmpArr[0]
        let rPath=tmpArr[1]
        let rFunction=a[x]
        if(rMethod==='get'){
            router.get(rPath,rFunction)
        }else if(rMethod==='post'){
            router.post(rPath,rFunction)
        }else if(rMethod==='put'){
            router.put(rPath,rFunction)
        }else if(rMethod==='delete'){
            router.delete(rPath,rFunction)
        }else{
            console.log('路径错误');
        }
    }
}

module.exports = router;