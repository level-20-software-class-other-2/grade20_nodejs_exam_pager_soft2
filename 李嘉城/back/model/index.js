'use strict'
const {Sequelize,Op}=require('sequelize');
const fs = require('fs');
const sequelize = new Sequelize('blog', 'postgres', '552552ljc', {
    host: 'jcjc520.top',
    dialect: 'postgres'
});
async function i() {
    try {
        sequelize.authenticate();
        console.log('数据库连接成功');
    } catch (error) {
        console.log('数据库连接失败',error);
    }
}
i();
let files = fs.readdirSync(__dirname);
let resfiles = files.filter(item => {
    return item.endsWith('.js') && item !== 'index.js';
})
let resObj = {};
resfiles.forEach(item => {
    let modelName = item.replace('.js', '');
    let tmpObj = require(__dirname + '/' + item);
    let tableName = modelName.toLowerCase();
    resObj[modelName] = sequelize.define(tableName, tmpObj);
})

resObj.Op = Op

resObj.sync = async () => {
    sequelize.sync({ force: true }).then(() => {
        resObj.Products.bulkCreate([
            {
                name: '论EF Core的自我修养',
                abstract: '论EF Core的自我修养',
                content: '论EF Core的自我修养',
                classify: '.Net',
                author:'InCerry'
            },
            {
                name: '论EF Core的自我修养',
                abstract: '论EF Core的自我修养',
                content: '论EF Core的自我修养',
                classify: '.Net',
                author:'InCerry'
            },
            {
                name: '论EF Core的自我修养',
                abstract: '论EF Core的自我修养',
                content: '论EF Core的自我修养',
                classify: '.Net',
                author:'InCerry'
            },
            {
                name: '论EF Core的自我修养',
                abstract: '论EF Core的自我修养',
                content: '论EF Core的自我修养',
                classify: '.Net',
                author:'InCerry'
            },
            {
                name: '论EF Core的自我修养',
                abstract: '论EF Core的自我修养',
                content: '论EF Core的自我修养',
                classify: '.Net',
                author:'InCerry'
            },
        ])
    });
}
module.exports = resObj;