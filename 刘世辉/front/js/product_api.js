'use strict'



function getProductList(keyword) {
    return new Promise(function (resolve,rejext) {
        $.get(`http://localhost:8000/product?keyword=${keyword}`,data=>{
            resolve(data);
        })
    })
}// 让后端请求博客信息列出数据，可能存在关键字查找

function getProductById(id) {
    return new Promise(function (resolve,reject) {
        $.get(`http://localhost:8000/product/${id}`,data=>{
            resolve(data);
        })
    })
}// 请求指定id的博客信息

function delProductById(id) {
    return new Promise(function (resolve,reject) {
        $.ajax({
            URL:`http://localhost:8000/product/${id}`,
            type:'dalete',
            success:data=>{
                resolve(data)
            }
        })
    })
}// 删除指定id的博客信息