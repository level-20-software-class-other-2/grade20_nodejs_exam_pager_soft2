'use strict';

const { DataTypes } = require('sequelize');

module.exports = {
    title:{
        type: DataTypes.STRING,
        allowNull: false
    },
    digest:{
        type: DataTypes.STRING,
        allowNull: false
    },
    content:{
        type: DataTypes.STRING,
        allowNull: false
    },
    classification:{
        type: DataTypes.STRING,
        allowNull: false
    },
    author:{
        type: DataTypes.STRING,
        allowNull: false
    },
    publish_time:{
        type: DataTypes.DATE,
        allowNull: false
    }
}