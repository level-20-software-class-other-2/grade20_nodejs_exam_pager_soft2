'use strict'
const{DataTypes}=require('sequelize')

//数据库的建表

let model={
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    get:{
        type:DataTypes.STRING,
        allowNull:false
    },
    text:{
        type:DataTypes.STRING,
        allowNull:false
    },
    type:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false,
    },
   time:{
       type:DataTypes.DATE,
       allowNull:true,
   }
}


module.exports=model