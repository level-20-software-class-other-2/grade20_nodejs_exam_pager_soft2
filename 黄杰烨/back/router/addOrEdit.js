fn_addOrEdit = (ctx)=> {
    let data = ctx.request.body

    if (data.id !== undefined) {
        let html =
            `
        <div id = "add">

        <table>
            <tr hidden>
                <td>id:</td>
                <td>
                    <input type="text" name="id" value="${data.id}">
                </td>
            </tr>
            <tr>
                <td>标题:</td>
                <td>
                <input type="text" name="headline" value="${data.headline}">
                </td>
            </tr>
            <tr>
                <td>摘要:</td>
                <td>
                <input type="text" name="abstract" value="${data.abstract}">
                </td>
            </tr>
            <tr>
                <td>内容:</td>
                <td>
                <input type="text" name="content" value="${data.content}">
                </td>
            </tr>
            <tr>
                <td>分类:</td>
                <td>
                <input type="text" name="classify" value="${data.classify}">
                </td>
            </tr>
            <tr>
                <td>作者:</td>
                <td>
                <input type="text" name="author" value="${data.author}">
                </td>
            </tr>
           
        </tr>
            <tr>
                <td>
                    <input type="button" value="保存" onclick="save(${data.id})">
                </td>
                <td>
                    <input type="button" value="取消" onclick="cancel()">
                </td>
            </tr>
        </table>
        </div>
        `
        ctx.body = html
    } else {
        let html =
        `
        <div id = "add">

        <table>
            <tr hidden>
                <td>id:</td>
                <td>
                    <input type="text" name="id" >
                </td>
            </tr>
            <tr>
            <td>标题:</td>
            <td>
            <input type="text" name="headline">
            </td>
        </tr>
            <tr>
                <td>摘要:</td>
                <td>
                <input type="text" name="abstract" >
                </td>
            </tr>
            <tr>
                <td>内容:</td>
                <td>
                <input type="text" name="content">
                </td>
            </tr>
            <tr>
                <td>分类:</td>
                <td>
                <input type="text" name="classify">
                </td>
            </tr>
            <tr>
                <td>作者:</td>
                <td>
                <input type="text" name="author">
                </td>
            </tr>
            <tr>
                     <td>
                    <input type="button" value="保存" onclick="save(${''})">
                     </td>
                     <td>
                    <input type="button" value="取消" onclick="cancel()">
                     </td>
                </tr>
             </table>
        </div>
        `
        ctx.body = html
    }
}

let get = [['/addhtml',fn_addOrEdit]]

let post = [['/addhtml',fn_addOrEdit]]

module.exports={
    'GET':get,
    'POST':post
}
