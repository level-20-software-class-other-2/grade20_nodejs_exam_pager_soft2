const Koa = require('koa')
const bodyparser = require('koa-bodyparser')
const router = require('koa-router')()
const cors = require('koa-cors')
const { Sequelize, Op, DataTypes } = require('sequelize')



let app = new Koa()

let sequelize = new Sequelize('text', 'postgres', 'qazxcv', {
    host: 'lqcwmn.top',
    dialect: "postgres"
})


let blogs = sequelize.define('blogs', {
    title: {
        type: DataTypes.STRING
    },
    abstract: {
        type: DataTypes.STRING
    },
    details: {
        type: DataTypes.STRING
    },
    classify: {
        type: DataTypes.STRING
    },
    author: {
        type: DataTypes.STRING
    }

})

sequelize.sync({ force: true }).then(() => {
    blogs.bulkCreate([
        {
            title: '论EfCore的自我修养',
            abstract: '论EfCore的自我修养',
            details: '论EfCore的自我修养',
            classify: '.Net',
            author: 'InCerry'

        },
        {
            title: 'DDD之我见',
            abstract: 'DDD之我见',
            details: 'DDD之我见',
            classify: '编程技术',
            author: '某大神'

        },
        {
            title: 'nginx负载平衡的几种策略',
            abstract: 'nginx负载平衡的几种策略',
            details: 'nginx负载平衡的几种策略',
            classify: '服务器',
            author: '老胡来也'

        },
        {
            title: 'Linux用户创建的学习研究',
            abstract: 'Linux用户创建的学习研究',
            details: 'Linux用户创建的学习研究',
            classify: 'Linux',
            author: '某大神'

        },
        {
            title: '大数据仪表盘探讨',
            abstract: '大数据仪表盘探讨',
            details: '大数据仪表盘探讨',
            classify: '大数据',
            author: '居家博士'

        }

    ])
})

router.get('/product', async (ctx, next) => {
    let keyword = ctx.request.query.keyword
    let list = await blogs.findAll({
        order: ['id']
    })
    if (keyword) {
        list = await blogs.findAll({
            where: {
                [Op.or]: [
                    { id: isNaN(keyword) ? -1 : parseFloat(keyword) },
                    {title: keyword},
                    {abstract: keyword},
                    {details: keyword},
                    {classify: keyword},
                    {author: keyword}
                ]
            }
        })
    }
    ctx.body=list
})
router.post('/find',async(ctx,next)=>{
    let id=ctx.request.body.id
    let list = await blogs.findAll({
        where:{
            id:id
        }
    })
    ctx.body=list
})
router.put('/save',async(ctx,next)=>{
    console.log(123456789);
    let obj=ctx.request.body
    console.log(obj);
    let id=ctx.request.body.id
    if (id) {
        await blogs.update({title:obj.title,abstract:obj.abstract,details:obj.details,classify:obj.classify,author:obj.author},{
            where:{
                id:obj.id
            }
        })
    } else {
        await blogs.create({title:obj.title,abstract:obj.abstract,details:obj.details,classify:obj.classify,author:obj.author})
    }
    ctx.body=''
})

router.delete('/del',async(ctx,next)=>{
    let id=ctx.request.body.id
    await blogs.destroy({
        where:{
            id:id
        }
    })
    ctx.body=''
})

app.use(cors())
app.use(bodyparser())
app.use(router.routes())

app.listen(8000)
console.log(`http://localhost:8000`);