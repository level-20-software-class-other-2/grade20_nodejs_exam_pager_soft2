'use strict';
const koa= require('koa');
const router =require('./router');
const cors=require('koa-cors');

const bodyParser = require('koa-bodyparser');
const { sync } = require('./model');

let app =new koa();
sync();



app.use(cors());
app.use(bodyParser());
app.use(router());

let port=8000;
app.listen(`${port}`);

console.log(`服务运行在一下地址：http://localhost:${port}`);

