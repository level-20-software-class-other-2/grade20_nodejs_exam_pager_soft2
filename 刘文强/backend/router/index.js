'use strict'

const router = require('koa-router')()
const fs = require('fs')

function getfile(url) {
    let tmp = url||__dirname
    let files = fs.readdirSync(tmp)
    let getfilepath = files.filter(item=>{
        return item.endsWith('.js')&&item!=='index.js'
    })
    return getfilepath
}
function getrouter(router,getfilepath) {
    getfilepath.forEach(item => {
        let tmp = __dirname+'/'+item
        let obj = require(tmp)
        for(let x in obj){
            let arr = x.split(' ')
            let a1 = arr[0]
            let a2 = arr[1]
            let objfunction = obj[x]
            if(a1==='get'){
                router.get(a2,objfunction)
            }else if(a1==='post'){
                router.post(a2,objfunction)

            }else if(a1==='put'){
                router.put(a2,objfunction)

            }else if(a1==='delete'){
                router.delete(a2,objfunction)
                
            }
        }
    });
    return router.routes()
 }
 module.exports=function(){
     let files = getfile()
     let fn = getrouter(router,files)
     return fn
 }