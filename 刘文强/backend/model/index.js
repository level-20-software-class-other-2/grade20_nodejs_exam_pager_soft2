'use strict'
const {Sequelize,Model,Op,DataTypes} = require('sequelize')
const {db_database,db_dialect,db_host,db_password,db_username}= require('../config/db')
const fs = require('fs')
let sequelize =new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
})
let files = fs.readdirSync(__dirname)
let readfile = files.filter(item=>{
    return item.endsWith('.js')&&item!=='index.js'
})
let res = {}
res.Op=Op
readfile.forEach(item=>{
    let modelname = item.replace('.js','')
    let tpmobj = require(__dirname+'/'+item)
    let tablename = modelname.toLowerCase()
    res[modelname] = sequelize.define(tablename,tpmobj)
})
res.sync=async()=>{
    sequelize.sync({force:true}).then(()=>{
        res.Blogs.bulkCreate([
            {
                title:'很难过',
                digest:'裂开',
                content:'什么玩意',
                class:'gg',
                author:'omg',
                time:null,
                

            },{
                title:'很难过1',
                digest:'裂开2',
                content:'什么玩意3',
                class:'gg3',
                author:'omg3',
                time:null,
                

            },
        ])
    })
}
module.exports=res