'use strict';

const fs = require('fs');
const path = require('path');

function getFiles(pathname) {
    return fs.readdirSync(pathname)
        .map(item => path.join(pathname, item));
}

function getJsFiles(pathname) {
    return getFiles(pathname)
        .filter(item => item.endsWith('.js'));
}

function getBasename(pathname) {
    if (!(pathname instanceof Array)) {
        pathname = [pathname];
    }
    return pathname.map(item => path.basename(item));
}

module.exports = {
    getJsFiles,
    getBasename
}