
fn_addhtml = (ctx) => {

    let data = ctx.request.body
    console.log(data.id);

    if (data.id) {
        let html = `
            <div id="addhtml">
            <table>
                <tr hidden>
                    <td>Id:</td>
                    <td><input type="text" name="id"  value="${data.id}"></td>
                </tr>
                <tr>
                    <td>标题:</td>
                    <td><input type="text" name="title" value="${data.title}"></td>
                </tr>
                <tr>
                    <td>摘要:</td>
                    <td><input type="text" name="digest" id="" value="${data.digest}"></td>
                </tr>
                <tr>
                    <td>内容:</td>
                    <td><input type="text" name="content" id="" value="${data.content}"></td>
                </tr>
                <tr>
                    <td>分类:</td>
                    <td><input type="text" name="classify" id="" value="${data.classify}"></td>
                </tr>
                <tr>
                    <td>作者:</td>
                    <td><input type="text" name="author" id="" value="${data.author}"></td>
                </tr>
                <tr>

                    <td><input type="button" value="保存" onclick="save(${data.id})"></td>
                    <td><input type="button" value="取消" onclick="cancel()"></td>
                </tr>
            </table>
        </div>
    `
        ctx.body = html
    } else {
        let html = `
    <div id="addhtml">
            <table>
                <tr hidden>
                    <td>Id:</td>
                    <td><input type="text" name="id" id=""></td>
                </tr>
                <tr>
                    <td>标题:</td>
                    <td><input type="text" name="title" id=""></td>
                </tr>
                <tr>
                    <td>摘要:</td>
                    <td><input type="text" name="digest" id=""></td>
                </tr>
                <tr>
                    <td>内容:</td>
                    <td><input type="text" name="content" id=""></td>
                </tr>
                <tr>
                    <td>分类:</td>
                    <td><input type="text" name="classify" id=""></td>
                </tr>
                <tr>
                    <td>作者:</td>
                    <td><input type="text" name="author" id=""></td>
                </tr>
                <tr>

                    <td><input type="button" value="保存" onclick="save(${''})"></td>
                    <td><input type="button" value="取消" onclick="cancel()"></td>
                </tr>
            </table>
        </div>
    `
        ctx.body = html
    }
}
let get = [['/addhtml', fn_addhtml]]
let post = [['/addhtml', fn_addhtml]]
module.exports = {
    'GET': get,
    'POST': post
}
