const koa = require('koa');

const app = new koa();

const bodyparser = require('koa-bodyparser');

const cors = require('koa-cors');

const router = require('./router/router.js');

const {sequelizeSync}= require('./sequelize/sequelize');

sequelizeSync();

app.use(cors());
app.use(bodyparser());
app.use(router.routes());

app.listen(8000);