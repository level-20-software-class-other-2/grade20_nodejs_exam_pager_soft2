'use strict';

let baseUrl = 'http://localhost:8000';

//查询
function getProductList(key) {
    return new Promise(function (resolve, reject) {
        $.get(`${baseUrl}/product?key=` + key, data => {
            resolve(data);
        })
    })
}
//请求
function getProductById(id) {
    return new Promise(function (resolve, reject) {
        $.get(`${baseUrl}/product/${id}`, data => {
            resolve(data);
        })
    })
}
//新增
function postProduct(obj) {
    return new Promise(function (resolve, reject) {
        $.get(`${baseUrl}/product`, obj, data => {
            resolve(data);
        })
    })
}
//编辑
function putProduct(obj, id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/product/${id}`,
            type: 'put',
            data: obj,
            success: data => {
                resolve(data);
            }
        })
    })
}
//删除
function delProductById(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/product/${id}`,
            type: 'delete',
            success: data => {
                resolve(data);
            }
        })
    })
}



