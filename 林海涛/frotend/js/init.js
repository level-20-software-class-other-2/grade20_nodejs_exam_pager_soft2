$(function(){
    query()
})

function render(arr){
    let rowData = $('.rowData').val()
    let tb = $('#tbData')
    rowData.remove()
    arr.forEach(item => {
        let html = `
        <tr class="rowData" key="${item.id}">
            <td>${item.id}</td>
            <td>${item.headline}</td>
            <td>${item.abstract}</td>
            <td>${item.content}</td>
            <td>${item.classify}</td>
            <td>${item.autor}</td>
            <td>${item.date}</td>
        </tr>
        <tr>
            <td><input type="button" value="编辑" onclick="Edit(${item.id})"></td>
            <td><input type="button" value="删除" onclick="Del(${item.id})"></td>
        </tr>
        `
        tb.append(html)
    });
}   

function query(){
    let keyword = $('#keyword').val()
    getProduct(keyword).then(data=>{
        render(data)
    })
}

function add(){
    location.href='./addOrEdit.html'
}

function Edit(){
    location.href='./addOrEdit.html'
}

function Del(id){
    let confirm = confirm(`确认删除id为${id}的数据吗？`)
    if(confirm){
        delProduct(id).then(data=>{
            let id = data.data.id
            $(`[key=${id}]`).remove()
        })
    }
}