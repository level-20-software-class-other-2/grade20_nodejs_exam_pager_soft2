const {Sequelize,Op } = require('sequelize')

const fs = require('fs')
const path = require('path')

const sequelize = new Sequelize('soft2','postgres','113',{
    host:'www.gomyheart.top',
    dialect:'postgres'
})


    let obj ={}

    obj.Op=Op

    let files = fs.readdirSync(path.join(__dirname))

    tableFile = files.filter((item)=>{
      
      return item!=='sequelize.js' && item.endsWith('.js')

    })

    tableFile.forEach(item => {
      let fileName = item.replace('.js','')

      let name = fileName.toLowerCase()

      let table = require(path.join(__dirname,item))

      obj[fileName] = sequelize.define(name,table)
      
    });
    
  obj.sequelizeSync =()=>{

    sequelize.sync({force:true}).then(res=>{

      obj.Student.bulkCreate(
        [
          {
            name:'论EF Core的自我修养',
            abstract:'论EF Core的自我修养',
            connder:'论EF Core的自我修养',
            classify:'.net',
            author:'InCerry',
            dataa:'2022-04-06 08:47'
          },
          {
            name:'DDD之我见',
            abstract:'DDD之我见',
            connder:'DDD之我见',
            classify:'编程技术',
            author:'某大神',
            dataa:'2022-04-03 23:47'
          },
          {
            name:'nginx负载平衡的集中策略',
            abstract:'nginx负载平衡的集中策略',
            connder:'nginx负载平衡的集中策略',
            classify:'服务器',
            author:'老胡来也',
            dataa:'2022-04-06 08:47'
          },
          {
            name:'Linux用户创建的学习研究',
            abstract:'Linux用户创建的学习研究',
            connder:'Linux用户创建的学习研究之我见',
            classify:'Linux',
            author:'某大神',
            dataa:'2022-04-06 08:47'
          },
          {
            name:'大数据仪表盘探讨',
            abstract:'大数据仪表盘探讨',
            connder:'DDD大数据仪表盘探讨之我见',
            classify:'大数据',
            author:'居家博士',
            dataa:'2022-04-18 16：18'
          }
        ]
      )

    })
  } 
module.exports=obj
