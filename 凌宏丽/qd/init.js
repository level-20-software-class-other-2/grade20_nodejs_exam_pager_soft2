let renderfn=function(data){
    $('.tr_row').remove();

    data.forEach(item => {
        let html=
        `
            <tr class="tr_row">
                <td>
                    <input type="text" name="id" value=${item.id}>
                </td>
                <td>
                    <input type="text" name="title" value=${item.title}>
                </td>
                <td>
                    <input type="text" name="abstract" value=${item.abstract}>
                </td>
                <td>
                    <input type="text" name="content" value=${item.content}>
                </td>
                <td>
                    <input type="text" name="classify" value=${item.classify}>
                </td>
                <td>
                    <input type="text" name="author" value=${item.author}>
                </td>
                <td>
                    <input type="button" value="删除" onclick="deleted(${item.id})">
                </td>
                <td>
                    <input type="button" value="修改" style="margin-left:-40px" onclick="endit(${item.id})">
                </td>
            </tr>
        `
        $('#data_table').append(html);
    });
}

//显示数据
$(function(){
    getproduceList().then(data=>{
        renderfn(data)
    })
})

//查找
function query(){
    let keyword=$('#keyword').val();
    queryproduct(keyword).then(data=>{
        renderfn(data)
    })
}

//删除
function deleted(id){
    delproduce(id).then(data=>{
        renderfn(data)
    })
}

//增加
function add(){
    window.location.href='addOrendit.html';
}

//修改
function endit(id){
    enditproduce(id).then(data=>{
        window.location.href=`addOrendit.html?${data[0]["id"]}`
    })
}

//保存
function save(){
    let id=$('[name=id]').val();
    let title=$('[name=title]').val();
    let abstract=$('[name=abstract]').val();
    let content=$('[name=content]').val();
    let classify=$('[name=classify]').val();
    let author=$('[name=author]').val();
    let obj={
        id,
        title,
        abstract,
        content,
        classify,
        author
    }
    if(!id){
        addproduce(obj).then(()=>{
            window.location.href='info.html'
        })
    }else{
        updateproduce(obj).then(()=>{
            window.location.href='info.html'
        })
    }
}

//编辑页的取消
function cancal(){
    window.location.href='info.html'
}

//用户页取消
function qx(){
    window.location.href='info.html'
}