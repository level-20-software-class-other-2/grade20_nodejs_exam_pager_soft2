//引入工具模板
const Koa = require('koa');
const app = new Koa();
const router = require('koa-router')();
const bodyparser = require("koa-bodyparser")
const cors = require('koa-cors')

const {Sequelize,Op, DataTypes} = require('sequelize'); 


//数据库
const sequelize = new Sequelize('text','postgres','zyfyyds',{
    host:'zyfyyds.top',
    dialect:'postgres'
})

let Blog = sequelize.define('blog',{
    tite:{
        type:DataTypes.STRING,
        allowNull:false
    },
    imports:{
        type:DataTypes.STRING,
        allowNull:false
    },
    text:{
        type:DataTypes.STRING,
        allowNull:false
    },
    cat:{
        type:DataTypes.STRING,
        allowNull:false
    },
    atou:{
        type:DataTypes.STRING,
        allowNull:false
    },
    time:{
        type:DataTypes.STRING,
        allowNull:false
    }
})

//初始化数据库表
// sequelize.sync({force:true}).then(()=>{
//     Blog.bulkCreate([
//         {
//             tite:"我是程序员",
//             imports:'天天敲代码',
//             text:'不开心就去soul',
//             cat:'哈哈哈哈',
//             atou:'我在哪',
//             time:'2002-20-02'
//         },
//         {
//             tite:"我是程序员",
//             imports:'天天敲代码',
//             text:'不开心就去soul',
//             cat:'哈哈哈哈',
//             atou:'我在哪',
//             time:'2002-20-02'
//         },
//         {
//             tite:"我是程序员",
//             imports:'天天敲代码',
//             text:'不开心就去soul',
//             cat:'哈哈哈哈',
//             atou:'我在哪',
//             time:'2002-20-02'
//         },

//     ])
// })

//查找和展示数据
router.get('/blog',async (ctx,next)=>{
    let keyword = ctx.request.query.keyword;
    if(keyword !='undefined'){
        ctx.body=await Blog.findAll({
            where:{
                [Op.or]:[
                    { id:isNaN(keyword)?-1:parseInt(keyword)},
                    {tite:keyword},
                    {imports:keyword},
                    {text:keyword},
                    {cat:keyword},
                    {atou:keyword},
                    {time:keyword},
                ]          
            },order:['id']
        }
        )
    }else{
        ctx.body = await Blog.findAll({order:['id']})
    }

})
//删除
router.delete('/blog/:id',async (ctx,next)=>{
    let id = ctx.request.params.id;
    await Blog.destroy({where:{id:id}})
    ctx.body = await Blog.findAll({order:['id']});

})
//编辑的查找
router.get('/blog/:id',async (ctx,next)=>{
    let id = ctx.request.params.id;
    ctx.body =  await Blog.findByPk(id)
    
})
//修改
router.put('/blog',async (ctx,next)=>{
    let obj = ctx.request.body;
    if(obj.id){
        await Blog.update({tite:obj.tite,imports:obj.imports,text:obj.text,cat:obj.cat,atou:obj.atou,time:obj.time},{where:{id:obj.id}})
    ctx.body = {
        code:1000,
        msg:'编辑成功'
     }
    }
})
//添加
router.post('/blog',async (ctx,next)=>{
    let obj = ctx.request.body;
    await Blog.create({tite:obj.tite,imports:obj.imports,text:obj.text,cat:obj.cat,atou:obj.atou,time:obj.time})
    ctx.body = {
        code:1000,
        msg:'添加成功'
    }
})

//挂载工具
app.use(cors());
app.use(bodyparser())
app.use(router.routes())

//监听
app.listen(8000,()=>{
    console.log(`http://localhost:8000`);
})