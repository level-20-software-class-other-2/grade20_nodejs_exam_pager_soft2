'use strict';

const request = require("koa/lib/request");

const { DataTypes } = require('sequelize');

let model = {
    title:{//标题
        type:DataTypes.STRING,
        allowNull:false
    },
    digest:{//摘要
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{//内容
        type:DataTypes.STRING,
        allowNull:false
    },
    type:{//分类
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{//作者
        type:DataTypes.STRING,
        allowNull:false
    },
    time:{//发布时间
        type:DataTypes.STRING,
        allowNull:false
    },
}

module.exports = model;