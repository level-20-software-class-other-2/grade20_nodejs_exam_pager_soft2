const { DataTypes } = require('sequelize')
// id-id name-title age-abstract(int-string)摘要 gander-content内容 type分类 author作者 postedTime发表时间

const blog = {
  title: {
    type: DataTypes.STRING,
  },
  abstract: {
    type: DataTypes.STRING
  },
  content: {
    type: DataTypes.STRING
  },
  type: {
    type: DataTypes.STRING
  },
  author: {
    type: DataTypes.STRING
  },
  postedTime: {
    type: DataTypes.STRING
  }
}

module.exports = blog