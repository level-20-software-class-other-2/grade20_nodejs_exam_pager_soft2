const {Boke,Op}=require('../model')

let get_fn=async(ctx,next)=>{
    let a=ctx.request.body.text || ''
    if (a) {
        let list=await Boke.findAll({
            where:{
                [Op.or]:[
                    {id:idNan(a)} ? 0 :parseInt(a),
                    {name:a}
                ]
            },
            order:['id']
        })
        ctx.body=list
    } else {
        let list=await Boke.findAll({
            order:['id']
        })
        ctx.body=list
    }
}

let post_fn=(ctx,next)=>{
    let a=ctx.request.body
    Boke.create(a)
    ctx.body='123'
}

let put_fn=async (ctx,next)=>{
    let obj=ctx.request.body
    await Boke.update(obj,{
        where:{
            id:obj.id
        }
    })
    ctx.body='123'
}

let delete_fn=async(ctx,next)=>{
    let id=ctx.request.params.id
    await Boke.destroy({
        where:{
            id:id
        }
    })
    let list=await Boke.findAll({
        order:['id']
    })
    ctx.body=list
}

module.exports={
    "get /login":get_fn,
    "post /login":post_fn,
    "put /login":put_fn,
    "delete /login/:id":delete_fn
}