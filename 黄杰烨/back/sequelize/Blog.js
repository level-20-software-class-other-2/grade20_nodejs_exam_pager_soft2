const {DataTypes} = require('sequelize')


const Blog ={
    headline: {
        type: DataTypes.STRING,
        
      },
      abstract: {
        type: DataTypes.STRING
       
      },
      content: {
        type: DataTypes.STRING
       
      },
      classify: {
        type: DataTypes.STRING
       
      },
      author: {
        type: DataTypes.STRING
       
      },
      issuedate: {
          type: DataTypes.STRING
          
    }
}

module.exports=Blog
