'use strict'

const koa=require('koa')
const router=require('koa-router')();
const cors=require('koa-cors');
const bodyparser=require('koa-bodyparser');
const {Sequelize ,Op,DataTypes}=require('sequelize');

const port=8080;

const sequelize=new Sequelize('blogs','postgres','whr0625.',{
    host:'wuhuqf.top',
    dialect:'postgres'
})

let Blog=sequelize.define('blog',{
    title:{
        type:DataTypes.STRING
    },
    abstract:{
        type:DataTypes.STRING
    },
    body:{
        type:DataTypes.STRING
    },
    classify:{
        type:DataTypes.STRING
    },
    auter:{
        type:DataTypes.STRING
    }
})

sequelize.sync({force:true}).then(()=>{
    Blog.bulkCreate([
        {
            title:'论EF Croe的自我修养',
            abstract:'论EF Croe的自我修养',
            body:'论EF Croe的自我修养',
            classify:'.Net',
            auter:'InCerry'
        },
        {
            title:'DDD之我见',
            abstract:'DDD之我见',
            body:'DDD之我见',
            classify:'编程技术',
            auter:'某大神'
        },
        {
            title:'nginx负载平衡的几种策略',
            abstract:'nginx负载平衡的几种策略',
            body:'nginx负载平衡的几种策略',
            classify:'服务器',
            auter:'老胡来也'
        },
        {
            title:'Liunx用户创建的学习研究',
            abstract:'Liunx用户创建的学习研究',
            body:'Liunx用户创建的学习研究',
            classify:'Liunx',
            auter:'某大神'
        },
        {
            title:'大数据仪表盘探讨',
            abstract:'大数据仪表盘探讨',
            body:'大数据仪表盘探讨',
            classify:'大数据',
            auter:'居家博士'
        }
    ])
})


router.get('/blogs',async (ctx,next)=>{
    let keyword=ctx.request.query.keyword || ''
    if (keyword) {
        let list =await Blog.findAll({
            where:{
                [Op.or]:[
                    {title:{[Op.like]:`%${keyword}%`}},
                    {abstract:{[Op.like]:`%${keyword}%`}},
                    {body:{[Op.like]:`%${keyword}%`}},
                    {classify:{[Op.like]:`%${keyword}%`}},
                    {auter:{[Op.like]:`%${keyword}%`}},
                ]
            },
            order:['id']
        })
        ctx.body=list;
    }else{
        let list =await Blog.findAll({
            order:['id']
        })
        ctx.body=list;
    }
})
router.get('/blogs/:id',async (ctx,next)=>{
    let id = ctx.request.params.id;
    let list =await Blog.findByPk(id);
    ctx.body=list;
})
router.post('/blogs',async (ctx,next)=>{
    let obj = ctx.request.body;
    await Blog.create(obj);
    ctx.body='添加成功';
})
router.put('/blogs/:id',async (ctx,next)=>{
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    await Blog.update(obj,{
        where:{
            id:id
        }
    })
    ctx.body='修改成功';
})
router.delete('/blogs/:id',async (ctx,next)=>{
    let id = ctx.request.params.id;
    let list =await Blog.findByPk(id);
    if (list) {
        await Blog.destroy({
            where:{
                id:id
            }
        })
        ctx.body='删除成功';
    }else{
        ctx.body='删除成功';
    }
})


let app=new koa();

app.use(cors());
app.use(bodyparser());
app.use(router.routes());


app.listen(port,()=>{
    console.log(`http://localhost:${port}`);
})