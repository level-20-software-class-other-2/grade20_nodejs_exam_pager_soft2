'use strict'

const {DataTypes} = require('sequelize')

let blog = {
    title:{
        type:DataTypes.STRING
    },
    digest:{
        type:DataTypes.STRING
    },
    content:{
        type:DataTypes.STRING
    },
    type:{
        type:DataTypes.STRING
    },
    author:{
        type:DataTypes.STRING
    },
    createdAt:{
        type:DataTypes.DATE
    }
}
module.exports = blog