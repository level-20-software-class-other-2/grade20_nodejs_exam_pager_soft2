'use strict'
const fs=require('fs');
const {Sequelize,Op}=require('sequelize');
const { date } = require('./Products');
//数据库
const sequelize=new Sequelize('blog','postgres','2002518',{
    host:'7yusen.top',
    dialect:'postgres'
})
let files=fs.readdirSync(__dirname);
let resfiles=files.filter(item=>{
    return item.endsWith('.js')&&item!=='index.js';
})
let resobj={};
resfiles.forEach(item=>{
    let modelname=item.replace('.js','');
    let tmpobj=require(__dirname+'/'+item);
    //变小写
    let tablename=modelname.toLocaleLowerCase();
    resobj[modelname]=sequelize.define(tablename,tmpobj);
})
resobj.Op=Op
resobj.sync=async()=>{
    sequelize.sync({force:true}).then(()=>{
        //批量插入
        resobj.Products.bulkCreate([
            {
                //标题
                headline:'论EFCore的自我修养',
                //摘要
                abstract:'论EFCore的自我修养',
                //内容
                details:'论EFCore的自我修养',
                //分类
                classify:'Net',
                //作者
                author:'Incerry',
            },
            {
                //标题
                headline:'牛',
                //摘要
                abstract:'马',
                //内容
                details:'羊',
                //分类
                classify:'龙',
                //作者
                author:'滕鑫'
            }
        ])
    })
}
module.exports=resobj;