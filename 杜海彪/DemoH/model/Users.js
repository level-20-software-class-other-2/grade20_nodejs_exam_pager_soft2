const {Sequelize,DataTypes}=require('sequelize');
let User={
    id={
        type:DataTypes.STRING
    },
    title={
        type:DataTypes.STRING
    },
    summary={
        type:DataTypes.STRING
    },
    content={
        type:DataTypes.STRING
    },
    classify={
        type:DataTypes.STRING
    },
    writer={
        type:DataTypes.STRING
    },
    time={
        type:DataTypes.STRING
    },
}
