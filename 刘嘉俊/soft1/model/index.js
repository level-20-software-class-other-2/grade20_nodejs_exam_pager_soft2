'use strict'

const fs = require('fs');
const {Sequelize,DataTypes,Op} = require('sequelize');
const {db_dialect,db_host,db_username,db_password,db_database} = require('../config/db');
const { time } = require('console');

const sequelize = new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
})

let files = fs.readdirSync(__dirname);

let resFiles = files.filter(item=>{
    return item.endsWith('.js') && item!=='index.js'
})

let resObj = {}

resFiles.forEach(item=>{
    let modelName = item.replace('.js','');
    let temObj = require(__dirname + '/' + item);
    let tableName = modelName.toLowerCase();
    resObj[modelName] = sequelize.define(tableName,temObj);
})

resObj.Op=Op;

resObj.sync =async ()=>{
    sequelize.sync({force:true}).then(()=>{
        resObj.Blogs.bulkCreate([
            {
                title:'论EF Core的自我修养',
                digest:'论EF Core的自我修养',
                content:'论EF Core的自我修养',
                classify:'.NET',
                autor:'Incerry',
                time:'2022-04-09'
            },
            {
                title:'DDD之我见',
                digest:'DDD之我见',
                content:'DDD之我见',
                classify:'编程技术',
                autor:'某大神',
                time:'2022-04-03'
            },
            {
                title:'nginx负载平衡的几种策略',
                digest:'nginx负载平衡的几种策略',
                content:'nginx负载平衡的几种策略',
                classify:'服务器',
                autor:'老胡来也',
                time:'2022-04-06'
            },
            {
                title:'Linux的用户创建的学习研究',
                digest:'Linux的用户创建的学习研究',
                content:'Linux的用户创建的学习研究',
                classify:'Linux',
                autor:'某大神',
                time:'2022-04-06'
            },
            {
                title:'大数据仪表盘探讨',
                digest:'大数据仪表盘探讨',
                content:'大数据仪表盘探讨',
                classify:'大数据',
                autor:'居家博士',
                time:'2022-04-18'
            }
        ])
    })
}

module.exports = resObj;