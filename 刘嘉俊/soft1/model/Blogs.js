'use strict'

const {DataTypes} = require('sequelize');

let blog = {
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    digest:{
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    autor:{
        type:DataTypes.STRING,
        allowNull:false
    },
    time:{
        type:DataTypes.STRING,
        allowNull:true,
    },
}

module.exports=blog;