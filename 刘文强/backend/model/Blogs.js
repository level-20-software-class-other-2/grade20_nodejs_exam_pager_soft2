'use strict'
const {DataTypes} = require('sequelize')
let Blogs = {
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    digest:{
        type:DataTypes.STRING,
        allowNull:false
    },
    content :{
        type:DataTypes.STRING,
        allowNull:false
    },
    class:{
        type:DataTypes.STRING,
        allowNull:false
    },author:{
        type:DataTypes.STRING,
        allowNull:false
    },time:{
        type:DataTypes.TIME,
        allowNull:true
    }
}
module.exports=Blogs