const koa=require('koa')
const bodyparser=require('koa-bodyparser')
const router=require('./router')
const cors=require('koa-cors')
const {sync}=require('./model')
const app=new koa()
app.use(cors())
app.use(bodyparser())
app.use(router())
sync()
const port=2000
app.listen(port,()=>{
    console.log(`http://localhost:${port}`);
})