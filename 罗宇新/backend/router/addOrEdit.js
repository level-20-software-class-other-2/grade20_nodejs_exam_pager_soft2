'use strict'

let fn_addOrEdit=(ctx,next)=>{
    let id = ctx.request.body.newId
    let digest = ctx.request.body.newDigest
    let title = ctx.request.body.newTitle
    let content = ctx.request.body.newContent
    let type = ctx.request.body.newType
    let author = ctx.request.body.newAuthor

    if (id !==  undefined) {
        let html =`
        <div id="add">
        <h1>添加博客或修改</h1>
        <table>
            <tr hidden>
                <td>id</td>
                <td><input type="text" name="id" value="${id}"></td>
            </tr>
            <tr>
                <td>标题</td>
                <td><input type="text" name="title" value="${title}"></td>
            </tr>
            <tr>
                <td>摘要</td>
                <td><input type="text" name="digest" value="${digest}"></td>
            </tr>
            <tr>
                <td>内容</td>
                <td><input type="text" name="content" value="${content}"></td>
            </tr>
            <tr>
                <td>分类</td>
                <td><input type="text" name="type" value="${type}"></td>
            </tr>
            <tr>
                <td>作者</td>
                <td><input type="text" name="author" value="${author}"></td>
            </tr>
            <tr>
                <td><input type="button" value="保存" onclick="save(${id})"></td>
                <td><input type="button" value="取消" onclick="cancel()"></td>
            </tr>
        </table>
    </div>
        `
        ctx.body = html
    } else {
        let html =`
        <div id="add">
        <h1>添加博客或修改</h1>
        <table>
            <tr hidden>
                <td>id</td>
                <td><input type="text" name="id"></td>
            </tr>
            <tr>
                <td>标题</td>
                <td><input type="text" name="title" ></td>
            </tr>
            <tr>
                <td>摘要</td>
                <td><input type="text" name="digest"></td>
            </tr>
            <tr>
                <td>内容</td>
                <td><input type="text" name="content" ></td>
            </tr>
            <tr>
                <td>分类</td>
                <td><input type="text" name="type" ></td>
            </tr>
            <tr>
                <td>作者</td>
                <td><input type="text" name="author" ></td>
            </tr>
            <tr>
                <td><input type="button" value="保存" onclick="save(${''})"></td>
                <td><input type="button" value="取消" onclick="cancel()"></td>
            </tr>
        </table>
    </div>
        `
        ctx.body = html
    }
}

let get = [['/addHtml',fn_addOrEdit]]
let post = [['/addHtml',fn_addOrEdit]]

module.exports={
    "get":get,
    "post":post
}