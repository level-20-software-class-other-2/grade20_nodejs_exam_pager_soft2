'use strict'

const { DataTypes } = require("sequelize")

const model={
    headline: {
        type: DataTypes.STRING,
    },
    abstract: {
        type: DataTypes.STRING,
    },
    content: {
        type: DataTypes.STRING,
    },
    classify: {
        type: DataTypes.STRING,
    },
    author: {
        type: DataTypes.STRING,
    },
    time: {
        type: DataTypes.STRING,
    },
}

module.exports=model