'use strict';
const fs = require('fs');
const { Sequelize, DataTypes,Op } = require('sequelize');
const sequelize = new Sequelize('soft4', 'postgres', '645093', {
    host: 'riserise.top',
    dialect: 'postgres'
});

let files = fs.readdirSync(__dirname);

let resFiles = files.filter(item => {
    return item.endsWith('.js') && item !== 'index.js';
})

let resObj = {};
resFiles.forEach(item => {
    let modelName = item.replace('.js', '');
    let tmpObj = require(__dirname + '/' + item);
    let tableName = modelName.toLowerCase();
    resObj[modelName] = sequelize.define(tableName, tmpObj);
})

resObj.Op=Op

// 封装为异步函数
resObj.sync = async () => {
    sequelize.sync({ force: true }).then(() => {

        resObj.Products.bulkCreate([
            {
                title:'论EF Core的自我修养',
                abstract:'论EF Core的自我修养',
                content:'论EF Core的自我修养',
                sort:'.Net',
                author:'InCerry',
                time:'2022-04-06 08:47',
            },
            {
                title:'DDD之我见',
                abstract:'DDD之我见',
                content:'DDD之我见',
                sort:'编程技术',
                author:'某大神',
                time:'2022-04-03 23:47',
            },
            {
                title:'nginx负载平衡的几种策略',
                abstract:'nginx负载平衡的几种策略',
                content:'nginx负载平衡的几种策略',
                sort:'服务器',
                author:'老胡来也',
                time:'2022-04-06 08:47',
            },
            {
                title:'Linux用户创建的学习研究',
                abstract:'Linux用户创建的学习研究',
                content:'Linux用户创建的学习研究',
                sort:'.Linux',
                author:'某大神',
                time:'2022-04-06 08:47',
            },
            {
                title:'大数据仪表盘探讨',
                abstract:'大数据仪表盘探讨',
                content:'大数据仪表盘探讨',
                sort:'大数据',
                author:'居家博士',
                time:'2022-04-18 16:18',
            },
          
            
        ])
    });
}

module.exports = resObj;