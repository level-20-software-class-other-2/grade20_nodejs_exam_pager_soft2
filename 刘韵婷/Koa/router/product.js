'use strict'

const { where } = require('underscore');
const { Products, Op } = require('../model');

//展示
let fn_Product_List = async (ctx, next) => {
    let key = ctx.request.query.key || '';
    if (key) {
        let list = await Products.findAll({
            where: {
                [Op.or]: [
                    { id: isNaN(key) ? 0 : parseInt(key) },
                    { biaoti: key },
                    { zhaiyao: key },
                    { neirong: key },
                    { fenlei: key },
                    { zuozhe: key }
                ]
            },
            order: ['id']
        })
        ctx.body = list
    } else {
        let list = await Products.findAll({
            order: ['id']
        })
        ctx.body = list;
    }
}

//查找
let fn_Product_List_Id = async (ctx, next) => {
    let id = ctx.request.params.id;
    let arr = await Products.findByPk(id);
    if (arr) {
        ctx.body = {
            code: 1000,
            data: arr,
            msg: '请求成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '请求失败'
        }
    }
}


//增加
let fn_Product_post = async (ctx, next) => {
    let obj = ctx.request.body;
    if (obj.hasOwnProperty('biaoti') && obj.hasOwnProperty('zhaiyao') && obj.hasOwnProperty('neirong') && obj.hasOwnProperty('fenlei') && obj.hasOwnProperty('zuozhe') 
    && obj.biaoti && obj.zhaiyao && obj.neirong && obj.fenlei && obj.zuozhe) {
        let resObj={
            biaoti: obj.biaoti,
            zhaiyao: obj.zhaiyao,
            neirong: obj.neirong,
            fenlei: obj.fenlei,
            zuozhe: obj.zuozhe
        }
        await Products.create(resObj);
        ctx.body = {
            code: 1000,
            data: resObj,
            msg: '添加成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '添加失败'
        }
    }
}


//编辑
let fn_Product_put = async (ctx, next) => {
    let id=ctx.request.params.id;
    let obj=ctx.request.body;
    let arr=await Products.findByPk(id)
    if(arr){
        await Products.update(obj,{
            where:{
                id:id
            }
        })
        ctx.body={
            code:1000,
            data:obj,
            msg:'编辑成功'
        }
    }else{
        ctx.body={
            code:400,
            data:'',
            msg:'编辑失败'
        }
    }
}


//删除
let fn_Product_delete = async (ctx, next) => {
    let id=ctx.request.params.id;
    let arr=await Products.findByPk(id)
    if(arr){
        await Products.destroy({
            where:{
                id:id
            }
        })
        ctx.body={
            code:1000,
            data:arr,
            msg:'删除成功'
        }
    }else{
        ctx.body={
            code:400,
            data:'',
            msg:'删除失败'
        }
    }
}

module.exports = {
    'get /product': fn_Product_List,
    'get /product/:id': fn_Product_List_Id,
    'post /product': fn_Product_post,
    'put /product/:id': fn_Product_put,
    'delete /product/:id': fn_Product_delete
}