'use strict'

const Koa = require('koa');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors');
const router = require('./router');
const { sync }=require('./model');
let app = new Koa();

sync();

app.use(cors());
app.use(bodyparser());
app.use(router());

let port = 8000;
app.listen(port);
console.log(`服务器运行在：http://localhost:${port}`);