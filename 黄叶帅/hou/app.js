const koa=require('koa');
const cors=require('koa-cors');
const bodyparser=require('koa-bodyparser');
const Router=require('./router');
const {sync}=require('./model/index')



// sync();

let app=new koa();

app.use(cors());
app.use(bodyparser());
app.use(Router());


let port=8000;
app.listen(port);
console.log(`http://localhost:8000${port}`);