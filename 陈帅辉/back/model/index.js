'use strict'


const{Sequelize,DataTypes,Op}=require('sequelize')
const fs=require('fs')



let sequelize=new Sequelize('soft2db','postgres','qaz123',{
    host:'shuaihui.top',
    dialect:'postgres'
})

    
    let objpath=fs.readdirSync(__dirname)
    let res=objpath.filter(item=>{
        return item.endsWith('.js')&& item!='index.js'
    })

    let obj={}
    obj.Op=Op
    res.forEach(item=>{
        let nodename=item.replace('.js','')
        let objnode=require(__dirname+'/'+item)
        let low=nodename.toLowerCase();
        obj[nodename]=sequelize.define(low,objnode)
    })

    obj.sync=async()=>{
        sequelize.sync({force:true}).then(()=>{
            obj.Products.bulkCreate([
                {
                    biaoti:'我的修养',
                    zhaiyao:'我的修养',
                    neirong:'我的修养',
                    fenlei:'.net',
                    zuozhe:'坤',
                    time:'2022-04-06 08:47'
                },
                {
                    biaoti:'我的修养1',
                    zhaiyao:'我的修养1',
                    neirong:'我的修养1',
                    fenlei:'.net',
                    zuozhe:'坤',
                    time:'2022-04-08 08:47'
                },
                {
                    biaoti:'我的修养2',
                    zhaiyao:'我的修养2',
                    neirong:'我的修养2',
                    fenlei:'.net',
                    zuozhe:'坤',
                    time:'2022-04-06 23:47'
                },
            ])
        })
    }

    module.exports=obj;