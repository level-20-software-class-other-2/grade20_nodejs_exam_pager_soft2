'use strict'
let fn_data;
$(function () {
    console.log('成功成功');
    query();
    let id = location.search.split('?')[1]
    console.log(id);

    if (id) {  // 判断ID存在与否
        getProductById(id).then(data => {
            console.log(data);

            if (data) {   //赋值
                $('[name=id]').val(data.data.id)
                $('[name=title]').val(data.data.title)
                $('[name=abstract]').val(data.data.abstract)
                $('[name=content]').val(data.data.content)
                $('[name=sort]').val(data.data.sort)
                $('[name=author]').val(data.data.author)
                $('[name=time]').val(data.data.time)
            }
        })
    }
})


function query() {
    let keyword = $('#keyword').val()
    getProductList(keyword).then(data => {
        console.log(data);

        renderFn(data);
    })
}


function renderFn(arr) {
    let row = $('.rowData');
    let tb = $('#tbData');
    row.remove();
    arr.forEach(item => {
        console.log(item);
        let html = `
                <tr class="rowData" key="${item.id}">
                    <td>${item.id}</td>
                    <td>${item.title}</td>
                    <td>${item.abstract}</td>
                    <td>${item.content}</td>
                    <td>${item.sort}</td>
                    <td>${item.author}</td>
                    <td>${item.time}</td>
                    <td>
                        <input type="button" value="编辑" onclick="update(${item.id})" >
                        <input type="button" value="删除" onclick="del(${item.id})" >
                    </td>
                </tr>
                `
        tb.append(html);
    })
}

function update(id) {
    location.href = `./addOrEdit.html?${id}`;
}

function del(id) {
    let confirmDel = confirm(`确定要删除id为${id}的项目吗？`);
    if (confirmDel) {
        delProductById(id).then(data => {
            let id = data.data.id;
            console.log(data);
            $(`[key=${id}]`).remove();
        })
    }

}

function add(obj) {
    // 跳转编辑页面
    location.href = './addOrEdit.html';
    postProduct(obj).then(data => {
        console.log(data);
    })
}




//script标签内容


function save() {
    let obj = {
        id: $('[name=id]').val(),
        title: $('[name=title]').val(),
        abstract: $('[name=abstract]').val(),
        content: $('[name=content]').val(),
        sort: $('[name=sort]').val(),
        author: $('[name=author]').val(),
        time: $('[name=time]').val(),
    }

    console.log(obj);

    //区分编辑和新增的保存
    if (obj.id) {
        putProduct(obj.id, obj).then(res => {
            console.log(res);
            // 编辑后保存，跳转
            if (res.code === 1000) {
                location.href = './index.html';
            } else {// 保存失败，弹出信息
                alert(code.msg);
            }
        })
    } else {
        postProduct(obj).then(data => {
            if (data.code === 1000) {
                console.log(data);
                location.href = './index.html';
            } else {
                alert(data.msg)
            }

        })
    }


}
function cancel() {
    location.href = './index.html';
}

