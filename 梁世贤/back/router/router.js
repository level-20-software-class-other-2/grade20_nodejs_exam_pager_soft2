const router = require('koa-router')()

const fs =require('fs')

const  path =require('path')

let files = fs.readdirSync(path.join(__dirname))

let allFiles =files.filter((item)=>{

            let ext = path.extname('xxx.js')

            return ext && item !=='router.js'
        })

        for(let file of allFiles){

            let js = require(path.join(__dirname,file))

            for(let key in js){
                if(key=='GET'){
                    
                    for (let i = 0; i < js[key].length; i++) {
                        router.get(js[key][i][0],js[key][i][1])
                    }
                }
                if(key=='POST'){
                    for (let i = 0; i < js[key].length; i++) {
                        router.post(js[key][i][0],js[key][i][1])
                    }
                }

            }
        }

module.exports=router