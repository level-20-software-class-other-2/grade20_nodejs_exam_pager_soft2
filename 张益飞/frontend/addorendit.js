//获取编辑的id
let id = location.search.split('?')[1];

//判断是不是编辑
if(id){
    $.get(`http://localhost:8000/blog/${id}`,data=>{
        console.log(data);
         $('[name=id]').val(data["id"]);
        $('[name=tite]').val(data["tite"]);
         $('[name=imports]').val(data["imports"]);
        $('[name=text]').val(data["text"]);
        $('[name=cat]').val(data["cat"]);
        $('[name=atou]').val(data["atou"]);
        $('[name=time]').val(data["time"]);
    })
}
//保存
function save(){
    let tite = $('[name=tite]').val();
    let imports = $('[name=imports]').val();
    let text = $('[name=text]').val();
    let cat = $('[name=cat]').val();
    let atou = $('[name=atou]').val();
    let time = $('[name=time]').val();
    let obj ={
        tite,
        imports,
        text,
        cat,
        atou,
        time,
    
    }
    if($('[name=id]').val()){
        obj.id = $('[name=id]').val();
        $.ajax({
            url:`http://localhost:8000/blog`,
            type:'put',
            data:obj,
            success:res=>{
                console.log(res);
                if(res.code=1000){
                    window.location.href='index.html'
                }
            }
        })
    }else{
        $.post('http://localhost:8000/blog',obj,res=>{
            console.log(res);
            if(res.code=1000){
                window.location.href='index.html'
            }
        })
    }
}
//取消
function cancel(){
    window.location.href='index.html'
}