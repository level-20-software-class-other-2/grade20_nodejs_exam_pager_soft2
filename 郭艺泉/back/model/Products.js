'use strict';

const { DataTypes } = require('sequelize');
const model = {
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    abstract: {
        type: DataTypes.STRING,
        allowNull: true
    },
    content: {
        type: DataTypes.STRING,
        allowNull: true
    },
    sort: {
        type: DataTypes.STRING,
        allowNull: true
    },
    author: {
        type: DataTypes.STRING,
        allowNull: true,
        default:0
    },
    time: {
        type: DataTypes.STRING,
        allowNull: true,
        default:0
    },
   
}

module.exports = model;