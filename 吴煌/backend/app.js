const koa=require('koa');
const routerM=require('./router');
const bodyparser=require('koa-bodyparser');
const cors=require('koa-cors');
const{sync}=require('./model')


let app=new koa();

sync()

app.use(cors());
app.use(bodyparser());
app.use(routerM());

let port=8000;
app.listen(port);


console.log(`运行在以下地址： http://localhost:${port}`);