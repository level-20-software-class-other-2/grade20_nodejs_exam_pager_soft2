const koa=require('koa');
const app=new koa();
//const router=require('./router/index')
const cors=require('koa-cors');
const bodyparser=require('koa-bodyparser');
const {sync}=require('./model');
sync();
app.use(cors());
app.use(bodyparser());
//app.use(router)
app.listen(4000,()=>{
    console.log('http://localhost:4000');
})