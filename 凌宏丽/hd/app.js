const koa=require('koa')
const router=require('./router')
const bodyparser=require('koa-bodyparser')
const cors=require('koa-cors')
let app=new koa();
const{sync}=require('./Model')

sync(); 

app.use(bodyparser());
app.use(cors())
app.use(router())

app.listen(8000,()=>{
    console.log('http://localhost:8000');
})