
//获取id进行编辑
$(function(){
    let id=location.search.split('?')[1]
    if(id){
        getBlogId(id).then(res=>{
            $('[name=id]').val(res['data'].id)
            $('[name=title]').val(res['data'].title)
            $('[name=get]').val(res['data'].get)
            $('[name=text]').val(res['data'].text)
            $('[name=type]').val(res['data'].type)
            $('[name=author]').val(res['data'].author)
        })
    }
})