'use strict';

const koa = require('koa');
const router = require('./router');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors');
const {sync} = require('./model');


let app = new koa();
sync();

app.use(cors());
app.use(bodyparser());
app.use(router());

let port = 8000;
app.listen(port);
console.log(`Server is running at : http://localhost:${port}`);