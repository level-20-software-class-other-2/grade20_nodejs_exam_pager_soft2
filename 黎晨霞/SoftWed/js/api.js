'use strict'
let bseUrl='http://localhost:3000'
//查找
function getList(keyword){
    return new Promise (function(ress,rejj){
        $.get(`${bseUrl}/login?keyword=`+keyword,data=>{
            ress(data);
        })
    })
}
//查找指定id
function getById(id){
    return new Promise (function(ress,rejj){
        $.get(`${bseUrl}/login/${id}`,data=>{
            ress(data);
        })
    })
}
//新增
function postP(obj){
    return new Promise (function(ress,rejj){
        $.post(`${bseUrl}/login`,obj,data=>{
            ress(data);
        })
    })
}
//编辑
function putP(id,obj){
    return new Promise (function(ress,rejj){
        $.ajax({
            url:`${bseUrl}/login/${id}`,
            type:'put',
            data:obj,
            success:data=>{
                ress(data)
            }
        })
    })
}
//删除
function deleteP(id){
    return new Promise (function(ress,rejj){
        $.ajax({
            url:`${bseUrl}/login/${id}`,
            type:'delete',
            success:data=>{
                ress(data.data)
            }
        })
    })
}