'use strict';

const koa=require('koa');
const app=new koa();

const router=require('./router/index');
const bodyParser = require('koa-bodyparser');
const cors=require('koa-cors');
const {Users}=require('./model');
//sync()

app.use(cors());
app.use(bodyParser());
app.use(router);

let port=3000;
app.listen(port);
console.log(`http://localhost:${port}`);
