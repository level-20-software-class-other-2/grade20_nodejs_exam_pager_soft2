const {Sequelize,Op} = require('sequelize')

const fs = require('fs')

const path = require('path')

const sequelize = new Sequelize('soft2','postgres','113',{
    host:'www.jieye.pro',
    dialect:'postgres'
})

let obj = {}
 obj.Op=Op

 let files = fs.readdirSync(path.join(__dirname))

 tableFile = files.filter((item)=>{
     return item !== 'sequelize.js' && item.endsWith('.js')
 })

 tableFile.forEach(item =>{
     let fileName= item.replace('.js','')

     let name = fileName.toLowerCase()

     let table = require(path.join(__dirname,item))

     obj[fileName] = sequelize.define(name,table)
 });

 obj.sequelizeSync = ()=>{
     sequelize.sync({force:true}).then(res=>{
         obj.Blog.bulkCreate(
             [
                 {
                    headline:'论EF Core的自我修养',
                    abstract:'论EF Core的自我修养',
                    content:'论EF Core的自我修养',
                    classify:'.Net',
                    author:'InCerry',
                    issuedate:'2022-04-06 08:47'
                 },
                 {
                    headline:'DDD之我见',
                    abstract:'DDD之我见',
                    content:'DDD之我见',
                    classify:'编程技术',
                    author:'某大神',
                    issuedate:'2022-04-03 23:47'
                 },
                 {
                    headline:'nginx负载平衡的几种策略',
                    abstract:'nginx负载平衡的几种策略',
                    content:'nginx负载平衡的几种策略',
                    classify:'服务器',
                    author:'老胡来也',
                    issuedate:'2022-04-06 08:47'
                 },
                 
                 

                 
             ]
         )
     })
 }
 module.exports = obj

