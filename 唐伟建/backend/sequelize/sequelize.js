const {Sequelize,Op} =require('sequelize')

const fs = require('fs')
const path = require('path')
const { log } = require('console')

let sequelize = new Sequelize('soft2','postgres','113',{
    host:'galaxyeye.top',
    dialect:'postgres'
})


let files = fs.readdirSync(path.join(__dirname))


let jsFiles = files.filter((item)=>{
    return item.endsWith('.js') && item!=='sequelize.js'
})
let obj = {}

obj.Op=Op
jsFiles.forEach((item)=>{
    let name = item.replace('.js','')
    let tableName = name.toLowerCase()

    let table= require(path.join(__dirname,item))
    obj[name]= sequelize.define(tableName,table)
})

obj.sync=()=>{
    sequelize.sync({force:true}).then(()=>{
        obj.Blog.bulkCreate([
            {
                title:'论EF Core的自我修养',
                digest:'论EF Core的自我修养',
                content:'论EF Core的自我修养',
                classify:'.Net',
                author:'InCerry',
                time:'2020-04-06 08:47'
            },
            {
                title:'DDD之我见',
                digest:'DDD之我见',
                content:'DDD之我见',
                classify:'编程技术',
                author:'某大神',
                time:'2020-04-06 08:47'
            },
            {
                title:'nginx负载平衡的几种策略',
                digest:'nginx负载平衡的几种策略',
                content:'nginx负载平衡的几种策略',
                classify:'服务器',
                author:'老胡去也',
                time:'2020-04-06 08:47'
            },
            {
                title:'Linux用户创建的学习研究',
                digest:'Linux用户创建的学习研究',
                content:'Linux用户创建的学习研究',
                classify:'Linux',
                author:'某大神',
                time:'2020-04-06 08:47'
            },
            {
                title:'大数据仪表探讨',
                digest:'大数据仪表探讨',
                content:'大数据仪表探讨',
                classify:'大数据',
                author:'居家博士',
                time:'2020-04-06 08:47'
            },
        ])
    })
}

module.exports=obj