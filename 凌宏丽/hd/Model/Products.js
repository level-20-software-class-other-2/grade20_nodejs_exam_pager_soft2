const {DataTypes}=require('sequelize');


const User={
    //标题
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    //摘要
    abstract:{
        type:DataTypes.STRING,
        allowNull:false
    },
    //内容
    content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    //分类
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    //作者
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    //发表时间
    publish:{
        type:DataTypes.INTEGER,
        allowNull:false
    }
}

module.exports=User;