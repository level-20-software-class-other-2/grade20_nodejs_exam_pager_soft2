$(function () {
    $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)

                $.get('http://127.0.0.1:8000/index', (data) => {
                    console.log(data);
                    log(data)
                })
            })
})

function log(data) {
    let Data = data.filter((item) => {
        return item.title !== undefined
    })

    Data.forEach(item => {
        let html = `
        <tr id ="html">
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.digest}</td>
            <td>${item.matter}</td>
            <td>${item.classify}</td>
            <td>${item.author}</td>
            <td>${item.posted}</td>
            <td>
            <input type="button" value="修改" onclick="Edit(${item.id})">
            <input type="button" value="删除" onclick="Delete(${item.id})">
            </td>
        </tr>
        `
        $('#content').append(html)
    });
}

function query() {

    let keyword = $('[name=keyword]').val()

    $.get('http://127.0.0.1:8000/data?keyword=' + keyword, (data) => {

        let tr = $('tr').length
        for (let i = 0; i < tr; i++) {
            $('#html').remove()
        }
        log(data)
    })
}

function add() {
    $.get('http://127.0.0.1:8000/addhtml', (data) => {

        $('#all').remove()

        $('#html').remove()

        $('#top').append(data)
    })
}

function save(Id) {

    let id = $('[name=id]').val()
    let title = $('[name=title]').val()
    let digest = $('[name=digest]').val()
    let matter = $('[name=matter]').val()
    let classify = $('[name=classify]').val()
    let author = $('[name=author]').val()


    let obj = {
        id,
        title,
        digest,
        matter,
        classify,
        author,
    }

    if (Id == undefined) {
        if (name=='') {
            alert('无法添加空值')
        }else{
            $.post('http://127.0.0.1:8000/indexadd', obj, (Data) => {

                    $('#add').remove()
                    $.get('http://127.0.0.1:8000/head', (data) => {
                    $('#top').append(data)

                    log(Data)

                })


            })
        }

    } else {

        $.post('http://127.0.0.1:8000/indexedit', obj, (Data) => {

            $('#add').remove()
            $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)
                log(Data)
            })
        })
    }
}

function Edit(id) {

    $.get('http://127.0.0.1:8000/index', (data) => {

        let Data = data.filter((item) => {
            return item.id == id
        })

        Data.forEach((item) => {
            let newId = item['id']
            let newTitle = item['title']
            let newDigest = item['digest']
            let newMatter = item['matter']
            let newClassify= item['classify']
            let newAuthor = item['author']
            let newPosted =item['posted']

            let obj = {
                newId,
                newTitle,
                newDigest,
                newMatter,
                newClassify,
                newAuthor,
                newPosted
            }

            $.post('http://127.0.0.1:8000/addhtml', obj, (data) => {
                $('#all').remove()

                let tr = $('tr').length
                for (let i = 0; i < tr; i++) {
                    $('#html').remove()
                }
                $('#top').append(data)
            })
        })
    })
}

function Delete(id) {
    if (confirm('是否要删除该条数据')) {
        $.get('http://127.0.0.1:8000/indexDeleted?id=' + id, (data) => {

        let tr = $('tr').length
        for (let i = 0; i < tr; i++) {
            $('#html').remove()
        }
        log(data)
    })
    }


}

function cancel(){
    $('#add').remove()

    $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)

                $.get('http://127.0.0.1:8000/index', (data) => {
                    log(data)
                })
            })
}
