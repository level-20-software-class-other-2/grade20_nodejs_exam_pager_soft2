const router=require('koa-router')();
const fs=require('fs')
const path=require('path')

function getfile(dirname){
    let file=fs.readdirSync(dirname);
    return file.filter(item=>{
        return item.endsWith('.js') && item !== 'index.js'
    })
}

function reqfile(filesjs){
    filesjs.map(item=>{
        let obj=require(path.join(__dirname,item))
        for(let i in obj){
            let reqmsg=i.split(' ');
            let method=reqmsg[0];
            let url=reqmsg[1];
            if(method==='get'){
                router.get(url,obj[i])
            }else if(method==='post'){
                router.post(url,obj[i])
            }else if(method==='delete'){
                router.delete(`${url}`,obj[i])
            }else if(method==='put'){
                router.put(`${url}`,obj[i])
            }
        }
    })
    return router.routes();
}

module.exports=()=>{
    return reqfile(getfile(__dirname))
}