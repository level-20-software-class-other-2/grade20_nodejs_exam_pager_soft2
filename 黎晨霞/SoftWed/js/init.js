'use strict'
//不知道为啥，突然给我报没有定义以下的方法。
$(function(){
    query()
})
//渲染页面
function resFn(arr){
    let row=$('.rowData');
    let tb=$('#tbData');
    row.rmove();
    arr.forEach(item => {
        let html=`
        <tr class='rowData' key='${item.id}'>
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td>${item.digest}</td>
                <td>${item.details}</td>
                <td>${item.classify}</td>
                <td>${item.author}</td>
                <td>${item.ptime}</td>
                <td>
                <input type="button" value="修改" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
        `
        tb.append(html)
    });
}
//查找
function query(keyword){
    let keyword=$('.keyword').val();
    getList(keyword).then(data=>{
        resFn(data)
    })
}
//新增
function add(){
    window.location.href='./add.html'
    postP(obj).then(data=>{
        console.log(data);
    })
}
//修改
function update(id,obj){
    location.href=`./add.html?${id}`
    putP(obj).then(data=>{
        console.log(data);
    })
}
//删除
function del(id){
    let conf=confirm(`确定要删除id为${id}的项目吗？`)
    if(conf){
        deleteP(id).then(data=>{
            $(`[key=${id}]`).rmove();
        })
    }
}
//保存
function save(){
    let id=$('[name=id]').val();
    let title=$('[name=title]').val();
    let digest=$('[name=digest]').val();
    let details=$('[name=details]').val();
    let classify=$('[name=classify]').val();
    let author=$('[name=author]').val();
    let ptime=$('[name=ptime]').val();
    let obj={
        id,title,digest,details,classify,author,ptime
    }
    if(obj.id){
        putP(obj.id,obj).then(data=>{
            if(data.code===1000){
                location.href='./index.html'
            }else{
                alert(data.msg)
            }
        })
    }else{
        postP(obj).then(data=>{
            if(data.code===1000){
                location.href='./index.html'
            }else{
                alert(data.msg)
            }
        })
    }
}
//取消
function cancel(){
    window.location.href='./add.html'
}