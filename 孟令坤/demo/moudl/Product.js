'use strict'
const { DataTypes } = require('sequelize')

const Product = {
    head: {
        type: DataTypes.STRING,
        allowNull: false
    },
    digest: {
        type: DataTypes.STRING,
        allowNull: false
    },
    details: {
        type: DataTypes.STRING,
        allowNull: false
    },
    classify: {
        type: DataTypes.STRING,
        allowNull: false
    },
    anthour: {
        type: DataTypes.STRING,
        allowNull: false
    },
  

}

module.exports = Product