'use strict'

const fs = require('fs');
const router = require('koa-router')();

function getFiles(filePath){
    let temFile = filePath || __dirname;
    let files = fs.readdirSync(temFile);
    let fFiles = files.filter(item=>{
        return item.endsWith('.js') && item!=='index.js'
    })
    return fFiles;
}

function rFiles(router,routerPath){
    routerPath.forEach(item => {
        let temPath = __dirname + '/' + item;
        let obj = require(temPath);
        for(let key in obj){
            let temArr = key.split(' ');
            let arr1 = temArr[0];
            let arr2 = temArr[1];
            let oFunction = obj[key];
            if (arr1 == 'get') {
                router.get(arr2,oFunction);
            }else if (arr1 == 'post') {
                router.post(arr2,oFunction);
            }else if (arr1 == 'put') {
                router.put(arr2,oFunction);
            }else if (arr1 == 'delete') {
                router.delete(arr2,oFunction);
            }else{
                console.log('网页出错');
            }
        }
    });
    return router.routes();
}

module.exports=function(){
    let a = getFiles();
    let b = rFiles(router,a)
    return b
}