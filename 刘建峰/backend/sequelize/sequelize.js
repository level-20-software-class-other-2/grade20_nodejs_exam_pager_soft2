const {Sequelize,Op } = require('sequelize')

const fs = require('fs')
const path = require('path')

const sequelize = new Sequelize('soft3','postgres','123',{
    host:'www.ljf0923.top',
    dialect:'postgres'
})

    let obj ={}

    obj.Op=Op

    let files = fs.readdirSync(path.join(__dirname))

    tableFile = files.filter((item)=>{

      return item!=='sequelize.js' && item.endsWith('.js')

    })

    tableFile.forEach(item => {
      let fileName = item.replace('.js','')

      let name = fileName.toLowerCase()

      let table = require(path.join(__dirname,item))


      obj[fileName] = sequelize.define(name,table)

    });

  obj.sequelizeSync =()=>{

    sequelize.sync({force:true}).then(res=>{

      obj.Student.bulkCreate(
        [
          {
             title:'论EF Core的自我修养',
             digest:'论EF Core的自我修养',
             matter:'论EF Core的自我修养',
             classify:'.Net',
             author:'incerry',
             posted:'2022-04-06 8:47'
          },
          {
            title:'DDD之我见',
             digest:'DDD之我见',
             matter:'DDD之我见',
             classify:'编辑技术',
             author:'某大神',
             posted:'2022-04-03 23：47'
          },
          {
            title:'nginx负载平衡的几种策略',
             digest:'nginx负载平衡的几种策略',
             matter:'nginx负载平衡的几种策略',
             classify:'服务器',
             author:'老胡来也',
             posted:'2022-04-06 8:47'
          },
          {
            title:'Linux用户创建的学习研究',
            digest:'Linux用户创建的学习研究',
            matter:'Linux用户创建的学习研究',
            classify:'Linux',
            author:'某大神',
            posted:'2022-04-06 8:47'
          },
          {
            title:'大数据仪表盘探讨',
            digest:'大数据仪表盘探讨',
            matter:'大数据仪表盘探讨',
            classify:'大数据',
            author:'居家博士',
            posted:'2022-04-18 16：18'
          },
        ]
      )

    })
  }
module.exports=obj
