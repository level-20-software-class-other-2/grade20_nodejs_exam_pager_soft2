const {DataTypes} = require('sequelize')


const student ={
      title: {
        type: DataTypes.STRING,
      },
      digest: {
        type: DataTypes.STRING
      },
      matter: {
          type: DataTypes.STRING
      },
      classify:{
          type:DataTypes.STRING
      },
      author:{
        type:DataTypes.STRING
      },
      posted:{
        type:DataTypes.STRING
      }
}
module.exports=student