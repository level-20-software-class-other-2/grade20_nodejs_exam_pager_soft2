const{DataTypes}=require('sequelize');

let model={
    
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    tips:{
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    type:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    time:{
        type:DataTypes.STRING,
        allowNull:false
    }
}

module.exports=model;