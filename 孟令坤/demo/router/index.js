"use strict"

const router = require('koa-router')();
const fs = require('fs');

function getRouter(filePath) {
    let tmpPath = __dirname || filePath;
    let files = fs.readdirSync(tmpPath);

    let routerFiles = files.filter(item => {
        return item.endsWith('.js') && item != 'index.js'
    })
    return routerFiles;

}

function regRouter(router, routerFiles) {
    routerFiles.forEach(item => {
        let tmpPath = __dirname + '/' + item;

        let obj = require(tmpPath)

        for (const key in obj) {
            let [rMethod, rPath] = key.split(' ')
            
            router[rMethod](rPath, obj[key])

          
        }
    });
    return router.routes()
}



module.exports = function() {
    let routerFiles = getRouter();
    let fn = regRouter(router, routerFiles);
    return fn;
}