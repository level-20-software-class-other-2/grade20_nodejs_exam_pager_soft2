'use strict'
const koa = require('koa')
const routers = require('./router')
const bodyparser = require('koa-bodyparser')
const cors = require('koa-cors')
const {sync}=require('./model')
let app = new koa()
sync()
app.use(cors())
app.use(bodyparser())
app.use(routers())


let prot = 8000;
app.listen(prot)
console.log(`已启动请访问:http://localhost:${prot}`);