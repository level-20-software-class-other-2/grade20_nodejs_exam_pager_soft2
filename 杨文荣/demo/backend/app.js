var Koa=require('koa')
var cors=require('koa-cors')
var bodyparser=require('koa-bodyparser')
var router=require('./router')
var {sync}=require('./model')
var app=new Koa()
app.use(cors())
app.use(bodyparser())
app.use(router())


sync();
app.listen(8000)