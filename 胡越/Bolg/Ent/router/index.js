const { Product, Op } = require('../model/index')

let fn_index = async (ctx) => {

    await Product.findAll().then(res => {
        ctx.body = res
    })

}


let fn_add = async (ctx) => {

    let data = await Product.findAll().then(res => {
        let max = 0;

        for (let i in res) {
            if (max < res[i].id) {
                max = res[i].id
            }
        }
        let id = max + 1;
        let data = ctx.request.body;

        data.id = id
        return data
    })

    await Product.create(data)

    await Product.findAll(
        {
            order: [
                ['id', 'ASC']
            ]
        }
    ).then(res => {
        ctx.body = res
    })
}
let fn_Deleted = async (ctx) => {

    let id = ctx.request.query.id;
    
    await Product.destroy({
        where: { id: id }
    })

    await Product.findAll(
        {
            order: [
                ['id', 'ASC']
            ]
        }
    ).then(res => {
        ctx.body = res
    })

}

let fn_data = async (ctx) => {
    let keyword = ctx.request.query.keyword;
    if (keyword !== '') {
        if (isNaN(keyword) == false) {
            let data = await Product.findAll({
                where: {
                    [Op.or]: [
                        { id: keyword },
                        { title: keyword },
                        { abstract: keyword },
                        { content: keyword },
                        { sort:keyword },
                        { auhor:keyword },
                        { dateline:keyword }
                    ]
                }
            })
            ctx.body = data

        } else {
            let data = await Product.findAll({
                where: {
                    [Op.or]: [

                    ]
                }
            })
            ctx.body = data
        }

    } else {
        await Product.findAll(
            {
                order: [
                    ['id', 'ASC']
                ]
            }
        ).then(res => {
            ctx.body = res
        })
    }

}

let fn_edit = async (ctx) => {

    let data = ctx.request.body

    await Product.findByPk(data.id).then(async (post) => {
        await post.update(
            {
                id: data.id,
                title: data.title,
                abstract:data.abstract,
                content: data.content,
                sort: data.sort,
                author:data.author,
                dateline:data.dateline
            }
        )
    })


    await Product.findAll(
        {
            order: [
                ['id', 'ASC']
            ]
        }
    ).then(res => {
        ctx.body = res
    })

}




let get = [['/index', fn_index], ['/data', fn_data], ['/indexDeleted', fn_Deleted]]
let post = [['/indexadd', fn_add], ['/indexedit', fn_edit]]

module.exports = {
    'GET': get,
    'POST': post
}