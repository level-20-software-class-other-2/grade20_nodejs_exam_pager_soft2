
var {Blogs,Op}=require('../model')

let fn_list=async(ctx,next)=>{
    let keyword=ctx.request.query.keyword
    if(keyword){
        let list=await Blogs.findAll({
            order:['id'],
            where:{
                id:keyword
            }
        })
        ctx.body=list
    }else{
        let list =await Blogs.findAll({
            order:['id']
        })
        ctx.body=list
    }
}

let fn_del=async(ctx,next)=>{
    let id=ctx.request.params.id
    let list=await Blogs.findByPk(id)
    if(list){
        await Blogs.destroy({
            where:{
                id:id
            }
        })
        ctx.body={
            code:1000,
            data:{id:id},
            msg:'请重试'
        }
    }else{
        ctx.body={
            code:400,
            msg:'重试'
        }
    }
}

let fn_list_id=async(ctx,next)=>{
    let id=ctx.request.params.id
    if(id){
        let list =await Blogs.findByPk(id)
        ctx.body=list
    }else{
        ctx.body={
            code:400,
            msg:'重试'
        }
    }
}


let fn_post=async(ctx,next)=>{
    let obj=ctx.request.body
    if(obj){
        let tmpObj={
            title:obj.titile,
            content:obj.content,
            class:obj.class,
            author:obj.author
        }
        await Blogs.create(tmpObj)
        ctx.body={
            code:1000,
            data:obj,
            msg:'cg'
        }
    }else{
        ctx.body={
            code:400,
            msg:'sb'
        }
    }
}

let fn_put=async(ctx,next)=>{
    let id=ctx.request.params.id
    let obj=ctx.request.body
    let list=await Blogs.findByPk(id)
    if(list){
        await Blogs.update(obj,{
            where:{
                id:id
            }
        })
        ctx.body={
            code:1000,
            data:obj,
            msg:'cg'
        }
    }else{
        ctx.body={
            code:400,
            msg:'sb'
        }
    }
}
module.exports={
    'get /blog':fn_list,
    'get /blog/:id':fn_list_id,
    'post /blog':fn_post,
    'put /blog/:id':fn_put,
    'delete /blog/:id':fn_del
}