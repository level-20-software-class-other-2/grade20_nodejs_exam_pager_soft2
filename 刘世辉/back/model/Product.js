'use strict'

const {DataTypes}=require('sequelize');

const Model={
    title:{
        type:DataTypes.STRING,
        allowNoll:false
    },
    digest:{
        type:DataTypes.STRING,
        allowNoll:false
    },
    content:{
        type:DataTypes.STRING,
        allowNoll:true,
        default:0
    },
    classify:{
        type:DataTypes.STRING,
        allowNoll:true
    },
    author:{
        type:DataTypes.STRING,
        allowNoll:true
    }
}

module.exports=Model;
