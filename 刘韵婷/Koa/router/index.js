'use strict'

const router = require('koa-router')();
const fs = require('fs');

let FilesPath = __dirname;
let Files = fs.readdirSync(FilesPath);

let resFiles = Files.filter(item => {
    return item.endsWith('.js') && item !== 'index.js'
})

resFiles.forEach(item => {
    console.log(item);
    resgiterRouter(item);
})

function resgiterRouter(resFiles) {
    let tmp = __dirname + '/' + resFiles;
    const a = require(tmp);
    for (let x in a) {
        let tmpArr = x.split(' ');
        let rMethod = tmpArr[0]
        let rPath = tmpArr[1]
        let rfunction = a[x]
        if (rMethod === 'get') {
            router.get(rPath, rfunction)
        } else if (rMethod === 'post') {
            router.post(rPath, rfunction)
        } else if (rMethod === 'put') {
            router.put(rPath, rfunction)
        } else if (rMethod === 'delete') {
            router.delete(rPath, rfunction)
        } else {
            console.log('路径请求错误');
        }
    }
}

module.exports = router;