fn_addOrEdit=(ctx)=>{

    let id = ctx.request.body.newId
    let title = ctx.request.body.newTitle
    let digest = ctx.request.body.newDigest
    let matter = ctx.request.body.newMatter
    let classify = ctx.request.body.newClassify
    let author = ctx.request.body.newAuthor
    let posted = ctx.request.body.newPosted



    if (id!==undefined) {
        let html =
        `
        <div id="add">
            <table>
                <tr hidden>
                    <td>id：</td>
                    <td>
                        <input type="text" name="id" value="${id}">
                    </td>
                </tr>
                <tr>
                    <td>标题：</td>
                    <td>
                        <input type="text" name="title" value="${title}">
                    </td>
                </tr>
                <tr>
                    <td>摘要：</td>
                    <td>
                        <input type="text" name="digest" value="${digest}">
                    </td>
                </tr>
                <tr>
                    <td>内容：</td>
                    <td>
                        <input type="text" name="matter" value="${matter}">
                    </td>
                </tr>
                <tr>
                    <td>分类：</td>
                    <td>
                        <input type="text" name="classify" value="${classify}">
                    </td>
                </tr>
                <tr>
                    <td>作者：</td>
                    <td>
                        <input type="text" name="author" value="${author}">
                    </td>
                </tr>
                <tr>
                    <td>发表时间：</td>
                    <td>
                        <input type="text" name="posted" value="${posted}">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="保存" onclick="save(${id})">
                    </td>
                    <td>
                        <input type="button" value="取消" onclick="cancel()">
                    </td>
                </tr>
            </table>
        </div>
        `
        ctx.body=html
    }else{
        let html =
        `
        <div id="add">
            <table>
                <tr hidden>
                    <td>id：</td>
                    <td>
                        <input type="text" name="id">
                    </td>
                </tr>
                <tr>
                    <td>标题：</td>
                    <td>
                        <input type="text" name="title">
                    </td>
                </tr>
                <tr>
                    <td>摘要：</td>
                    <td>
                        <input type="text" name="digest">
                    </td>
                </tr>
                <tr>
                    <td>内容：</td>
                    <td>
                        <input type="text" name="matter">
                    </td>
                </tr>
                <tr>
                    <td>分类：</td>
                    <td>
                        <input type="text" name="classify">
                    </td>
                </tr>
                <tr>
                    <td>作者：</td>
                    <td>
                        <input type="text" name="author">
                    </td>
                </tr>
                <tr>
                    <td>发表时间：</td>
                    <td>
                        <input type="text" name="posted">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="保存" onclick="save(${''})">
                    </td>
                    <td>
                        <input type="button" value="取消" onclick="cancel()">
                    </td>
                </tr>
            </table>
        </div>
        `
        ctx.body=html
    }

}
let get = [['/addhtml',fn_addOrEdit]]
let post = [['/addhtml',fn_addOrEdit]]
module.exports={
    "GET":get,
    "POST":post
}
