

$(
    function(){
        query()
    }
)

function query(){
    let keyword=$('#keyword').val()
    getList(keyword).then(data=>{
        tableLoad(data)
    })
}
function tableLoad(arr){
    let row=$('.row')
    row.remove()
    let tb=$('#tbData')
    
    arr.forEach(item=>{
        let html=`
        <tr class="row">
        <td>${item.id}</td>
        <td>${item.title}</td>
        <td>${item.digest}</td>
        <td>${item.content}</td>
        <td>${item.class}</td>
        <td>${item.author}</td>
        <td>${item.createTime}</td>
        <td><input type="button" value="编辑" onclick="edit(${item.id})"><input type="button" value="删除" onclick="remove(${item.id})"></td>
        </tr>
        `
        tb.append(html)
    })
}

function add(){
    location.href='addOrEdit.html'
}

function edit(id){
    location.href=`addOrEdit.html?id=${id}`
}

function remove(id){
    let confirmdel=confirm(`确认要删除id为${id}的内容吗`)
    if(confirmdel){
        del(id).then(()=>{
            location.href='index.html'
        })
    }
}