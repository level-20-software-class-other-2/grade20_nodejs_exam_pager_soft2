//连接数据库
const {Sequelize,DataTypes}=require('sequelize');
const fs=require('fs');

let sequelize=new Sequelize('soft','postgres','dhb123...',{
    honst:'www.lifesunny.top',
    dialect:'postgres'
})
let file=fs.readdirSync(__dirname);
let files=file.filter(item=>{
    return item.endsWith('.js') && item!='index.js'
})
let resObj={};
files.filter(item=>{
    let modelName=item.replace('.js','');
    let temobj=require(__dirname+'/'+item);
    let tabName=modelName.toLowerCase();
    resObj[modelName]=sequelize.defin(temobj,tabName)
})
//信息数据
resObj.sync=async()=>{
    sequelize.sync({fore:true}).then(data=>{
        resObj.Users.bulkCreate([
            {
            //id //标题 //摘要 //内容 //分类 //作者 //发布时间
                id:1,
                title:'论EF Core的自我修养',
                summary:'论EF Core的自我修养',
                content:'论EF Core的自我修养',
                classify:'.NET',
                writer:'Incerry',
                time:2022-04-06
            },
            {
                id:2,
                title:'DDD之我之间',
                summary:'DDD之我之间',
                content:'DDD之我之间',
                classify:'编程技术',
                writer:'某大神',
                time:2022-04-03
            },
            {
                id:3,
                title:'nginx负载平衡的办法',
                summary:'nginx负载平衡的办法',
                content:'nginx负载平衡的办法',
                classify:'服务器',
                writer:'老胡来也',
                time:2022-04-06
            },
            {
                id:4,
                title:'Linux用户创建的学习研究',
                summary:'Linux用户创建的学习研究',
                content:'Linux用户创建的学习研究',
                classify:'Linux',
                writer:'某大神',
                time:2022-04-06
            },
            {
                id:5,
                title:'大数据仪表盘盘',
                summary:'大数据仪表盘盘',
                content:'大数据仪表盘盘',
                classify:'大数据',
                writer:'居家博士',
                time:2022-04-18
            }
        ])
    })
}
module.exports=resObj;