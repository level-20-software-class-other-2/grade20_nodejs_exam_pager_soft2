function getProduct(keyword){
    return new Promise(function(res,rej){
        $.get('http://localhost:8000/product?keyword='+keyword,data=>{
            res(data)
        })
    })
}
function getProductId(id){
    return new Promise(function(res,rej){
        $.get(`http://localhost:8000/product/:${id}`,data=>{
            res(data)
        })
    })
}
function postProduct(obj){
    return new Promise(function(res,rej){
        $.post(`http://localhost:8000/product`,data=>{
            res(data)
        })
    })
}

function putProduct(obj,id){
    return new Promise(function(res,rej){
        $.ajax({
            url:`http://localhost:8000/product/:${id}`,
            type:'put',
            data:obj,
            success:data=>{
                res(data)
            }
        })
    })
}
function delProduct(id){
    return new Promise(function(res,rej){
        $.ajax({
            url:`http://localhost:8000/product/:${id}`,
            type:'delete',
            success:data=>{
                res(data)
            }
        })
    })
}