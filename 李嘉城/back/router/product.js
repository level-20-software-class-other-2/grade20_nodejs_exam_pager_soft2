'use strict'

const { Products, Op } = require('../model');

let fnlist = async (ctx, next) => {
    let keyword = ctx.request.query.keyword || '';
    console.log(keyword);
    if (keyword) {
        let list = await Products.findAll({
            where: {
                [Op.or]: [
                    { id: isNaN(keyword) ? 0 : parseInt(keyword) },
                    { name: keyword },
                    { abstract: keyword },
                    { content: keyword },
                    { classify: keyword },
                    { author: keyword }
                ]
            },
            order:['id']
        })
        ctx.body = list;
    } else {
        let list = await Products.findAll({
            order:['id']
        });
        ctx.body = list;
    }

}

let fnid = async (ctx, next) => {
    let id = ctx.request.params.id;
    let arr = await Products.findByPk(id);
    if (arr) {
        ctx.body = {
            code: 1000,
            data: arr,
            msg: '获取成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '获取失败'
        }
    }
}

let fnpost = (ctx, next) => {
    let obj = ctx.request.body;
    console.log(obj);
    if (obj.hasOwnProperty('name') && obj.hasOwnProperty('abstract') && obj.hasOwnProperty('content') 
    && obj.hasOwnProperty('classify') && obj.hasOwnProperty('author') &&
        obj.name && obj.abstract && obj.content && obj.classify && obj.author ) {
        let AObj = {
            name: obj.name,
            abstract: obj.abstract,
            content: obj.content,
            classify: obj.classify,
            author:obj.author
        };
        Products.create(AObj)

        ctx.body = {
            code: 1000,
            data: AObj,
            msg: '新增成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '新增失败，请确认后重试'
        }
    }
}
let fnput = async (ctx, next) => {
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    console.log(obj);
    let res = await Products.findByPk(id);

    console.log(res);

    if (res) {

        await Products.update(obj, {
            where: {
                id: id
            }
        });
        ctx.body = {
            code: 1000,
            data: res,
            msg: '修改成功！'
        }

    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '修改失败，请确认后重试'
        }

    }
}

let fndelete =async (ctx, next) => {
    let id = ctx.request.params.id;
    let res = await Products.findByPk(id);
    if (res) {
       await Products.destroy({
            where:{
                id:id
            }
        })
        console.log(products);
        ctx.body = {
            code: 1000,
            data: { id: id },
            msg: '删除指定记录成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: '',
            msg: '删除失败，请确认后重试'
        }
    }
}

module.exports = {
    'get /product': fnlist,
    'get /product/:id': fnid,
    'post /product': fnpost,
    'put /product/:id': fnput,
    'delete /product/:id': fndelete
}