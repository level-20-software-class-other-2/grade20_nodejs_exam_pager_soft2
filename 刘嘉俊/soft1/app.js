'use strict'

const koa = require('koa');
const router = require('./router');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors');
const {sync} = require('./model');

sync();

let app = new koa();

app.use(cors());
app.use(bodyparser());
app.use(router());

let port = 8000;

app.listen(port);

console.log(`从这里进入 http://localhost:${port}`);