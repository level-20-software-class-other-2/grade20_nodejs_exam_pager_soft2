'use strict'

const Koa=require('koa')
const router=require('koa-router')()
const bodyparser=require('koa-bodyparser')
const cors=require('koa-cors')

const Sequelize=require('sequelize')
let sequelize=['demo_db','postgres','lhy0624',{
    host:'6alin.top',
    dialect:'postgres'
}]

let app= new Koa();

app.use(cors())
app.use(bodyparser())
app.use(router.routes())

app.listen(8000);
console.log(`当前服务运行在如下地址：http://localhost:8000`);
