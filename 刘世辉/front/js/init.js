'use strict'

$(function () {
    fn_list()
})

function fn_list(basedada) {
    $.get(`http://localhost:8000/product`, data => {
        if (basedada) {
            data = basedada
        }
        $('.row').remove()
        data.forEach(item => {
            let html = `
                <tr class="row">
                    <td>${item.id}</td>
                    <td>${item.title}</td>
                    <td>${item.digest}</td>
                    <td>${item.content}</td>
                    <td>${item.classify}</td>
                    <td>${item.author}</td>
                    <td>
                        <input type="button" value="编辑" onclick="update(${item.id})">
                        <input type="button" value="删除" onclick="del(${item.id})">
                    </td>
                </tr>`
            $('#tbData').append(html)
        });
    })
}

//查询
function query() {
    let keyword = $('#keyword').val();//1
    $.get(`http://localhost:8000/product?keyword=${keyword}`, data => {
        fn_list(data);//1
    })
}

// 删除
function del(id) {
    if (confirm('确认删除吗？')) {
        delProductById(id).then(data => {
            window.location.href = './index.html'
        })
    }
}

// 添加
function add(add) {
    window.location.href = './addOrEdit.html'
    var id =document.getElementsByName("id")[0].nodeValue;
    var ititled =document.getElementsByName("title").nodeValue;
    var digest =document.getElementsByName("digest").nodeValue;
}
// 编辑
function update(id) {
    window.location.href = `./addOrEdit.html`
}

function cancel(cancel) {
    
}