const router=require('koa-router')()
const fs=require('fs')

function a(Path) {
    let fPath=Path || __dirname
    let files=fs.readdirSync(fPath)
    let Data=files.filter(item=>{
        return item.endsWith('.js') && item!=='index.js'
    })
    return Data
}

function b(router,routefiles) {
    routefiles.forEach(item => {
        let a=__dirname+'/'+item
        let obj=require(a)
        for (let key in obj) {
            let b=key.split(' ')
            let a1=b[0]
            let a2=b[1]
            let a3=obj[key]
            if (a1==='get') {
                router.get(a2,a3)
            }else if (a1==='post') {
                router.post(a2,a3)
            }else if (a1==='put') {
                router.put(a2,a3)
            }else if (a1==='delete') {
                router.delete(a2,a3)
            }else{
                console.log('路径错误');
            }
        }
    });
    return router.routes()
}

module.exports=function () {
    let routefiles=a()
    let fn=b(router,routefiles)
    return fn
}