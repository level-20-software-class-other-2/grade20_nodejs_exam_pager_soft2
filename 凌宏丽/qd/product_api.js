let url='http://localhost:8000';

//返回所有的数据
function getproduceList(){
    return new Promise((resolve,reject)=>{
        $.get(`${url}/produce`,(data)=>{
            resolve(data)
        })
    })
}

//查找
function queryproduct(keyword){
    return new Promise((reslove,reject)=>{
        $.get(`${url}/produce?keyword=${keyword}`,(data)=>{
            reslove(data);
        })
    })
}

//删除
function delproduce(id){
    return new Promise((reslove,reject)=>{
        $.ajax({
            url:`${url}/produce/${id}`,
            type:"delete",
            success:(data)=>{
                reslove(data)
            }
        })
    })
}

//编辑
function enditproduce(id){
    return new Promise((reslove,reject)=>{
        $.get(`${url}/produce/${id}`,(data)=>{
            reslove(data)
        })
    })
}

//新增
function addproduce(obj){
    return new Promise((reslove,reject)=>{
        $.post(`${url}/produce`,obj,()=>{
            reslove();
        })
    })
}

//修改
function updateproduce(obj){
    return new Promise((reslove,reject)=>{
        $.ajax({
            url:`${url}/produce`,
            method:"put",
            data:obj,
            success:()=>{
                reslove();
            }
        })
    })
}