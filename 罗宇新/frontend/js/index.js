'use strict'

$(function () {
    $.get('http://localhost:8000/head', data => {
        $('#top').append(data)
        $.get('http://localhost:8000/index', data => {
            print(data)
        })
    })
})
function print(data) {
    let Data = data.filter(item => {
        return item.title !== undefined
    })
    Data.forEach(item => {
        let html = `
        <tr id="html">
        <td>${item.id}</td>
        <td>${item.title}</td>
        <td>${item.digest}</td>
        <td>${item.content}</td>
        <td>${item.type}</td>
        <td>${item.author}</td>
        <td>${item.createdAt}</td>
        <td>
            <input type="button" value="修改" onclick="edit(${item.id})">
            <input type="button" value="删除" onclick="del(${item.id})">
        </td>
    </tr>
        `
        $('#content').append(html)
    });
}

function query() {
    let keyword = $('[name=keyword]').val()
    $.get('http://localhost:8000/data?keyword=' + keyword, data => {
        let tr = $('tr').length
        for (let i = 0; i < tr; i++) {
            $('#html').remove()
        }
        print(data)
    })
}

function add() {
    $.get('http://localhost:8000/addHtml', data => {
        $('#all').remove()
        $('#html').remove()
        $('#top').append(data)
    })
}

function save(Id) {
    let id = $('[name=id]').val()
    let title = $('[name=title]').val()
    let digest = $('[name=digest]').val()
    let content = $('[name=content]').val()
    let type = $('[name=type]').val()
    let author = $('[name=author]').val()

    let obj = {
        id,
        title,
        digest,
        content,
        type,
        author
    }
    if (Id == undefined) {
        if (title == '') {
            alert('标题不能为空')
        } else {
            $.post('http://localhost:8000/indexAdd', obj, Data => {
                $('#add').remove()
                $.get('http://localhost:8000/head', data => {
                    $('#top').append(data)
                    print(Data)
                })
            })
        }
    } else {
        $.post('http://localhost:8000/indexEdit', obj, Data => {
            $('#add').remove()
            $.get('http://localhost:8000/head', data => {
                $('#top').append(data)
                print(Data)
            })
        })
    }
}

function edit(id) {
    $.get('http://localhost:8000/index', data => {
        let Data = data.filter(item => {
            return item.id == id
        })
        Data.forEach(item => {
            let newId = item['id']
            let newTitle = item['title']
            let newDigest = item['digest']
            let newContent = item['content']
            let newType = item['type']
            let newAuthor = item['author']

            let obj = {
                newId,
                newTitle,
                newDigest,
                newContent,
                newType,
                newAuthor
            }
            $.post('http://localhost:8000/addHtml',obj, data => {
                $('#all').remove()
                let tr = $('tr').length
                for (let i = 0; i < tr; i++) {
                    $('#html').remove()
                }
                $('#top').append(data)
            })
        })
    })
}

function del(id) {
    if (confirm('确定删除这条博客吗？')) {
        $.get('http://localhost:8000/delete?id=' + id, data => {
            let tr = $('tr').length
            for (let i = 0; i < tr; i++) {
                $('#html').remove()
            }
            print(data)
        })
    }
}

function cancel() {
    $('#add').remove()
    $.get('http://localhost:8000/head', data => {
        $('#top').append(data)
        $.get('http://localhost:8000/index', data => {
            print(data)
        })
    })
}