const router=require('koa-router')()
const fs=require('fs')
const files=fs.readdirSync(__dirname)
const path=require('path')
files.forEach(file=>{
    if(file!='index.js'){
        const newpath=path.join(__dirname,file)
         const obj=require(newpath)
         for(let tem in obj){
             let arr=tem.split(' ')
             let url=arr[1]
            let fn =obj=[tem]
            let math=arr[0]
            if(math=='get'){
                router.get(url,fn)

            }else if(math=='post'){
                router.post(url,fn)

            }
          }
    }
})

module.exports=router.routes()
