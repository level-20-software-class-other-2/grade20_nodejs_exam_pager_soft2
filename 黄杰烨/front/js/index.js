$(function () {
    $.get('http://127.0.0.1:3000/head', (data) => {
        $('#top').append(data)

        $.get('http://127.0.0.1:3000/index', (data) => {
            console.log(data);
            log(data)
        })
    })
})

function log(data) {
    data.forEach(item => {
        let html = `
        <tr id ="html">
        <td>${item.id}</td>
        <td>${item.headline}</td>
        <td>${item.abstract}</td>
        <td>${item.content}</td>
        <td>${item.classify}</td>
        <td>${item.author}</td>
        <td>${item.issuedate}</td>

        <td>
            <input type="button" value="编辑" onclick="Edit(${item.id})">
            <input type="button" value="删除" onclick="Delete(${item.id})">
        </td>
    </tr>
        `
        $('#content').append(html)
    });
}

function query() {

    let keyword = $('[name=keyword]').val()

    $.get('http://127.0.0.1:3000/data?keyword=' + keyword, (data) => {
        console.log(data);
        let tr = $('tr').length
        for (let i = 0; i < tr; i++) {
            $('#html').remove()
        }
        log(data)
    })
}

function add() {
    $.get('http://127.0.0.1:3000/addhtml', (data) => {

        $('#all').remove()

        $('#top').append(data)
    })
}

function save() {


    let headline = $('[name=headline]').val()
    let abstract = $('[name=abstract]').val()
    let content = $('[name=content]').val()
    let classify = $('[name=classify]').val()
    let author = $('[name=author]').val()
    let issuedate = $('[name=issuedate]').val()

    let ob = {
        headline,
        abstract,
        content,
        classify,
        author,
        issuedate
    }

    if (name ==''){
        alert('无法添加空值')
    } else {
        $.post('http://127.0.0.1:3000/indexadd', ob, (Data) => {

                $('#add').remove()

                $.get('http://127.0.0.1:3000/head', (data) => {

                $('#top').append(data)

                log(Data)
            })
        })
    }
}




function Delete(id){
    if (confirm('确认删除？删除后无法恢复')) {
        $.get('http://127.0.0.1:3000/indexDeleted?id='+id,(data)=>{
            let tr = $('tr').length
            for(let i = 0;i<tr;i++){
                $('#html').remove()
            }
            log(data)
        })
    }
}

function cancel(){
    $('#add').remove()

    $.get('http://127.0.0.1:3000/head',(data)=>{
        $('#top').append(data)

        $.get('http://127.0.0.1:3000/index',(data)=>{
            log(data)
        })
    })
}