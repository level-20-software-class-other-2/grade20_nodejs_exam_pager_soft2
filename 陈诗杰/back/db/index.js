'use strict';
const {
    db_dialect,
    db_user,
    db_pwd,
    db_host,
    db_port,
    db_database
} = require('../config/db');
const { Sequelize, Op, Model } = require('sequelize');
const db = new Sequelize(`${db_dialect}://${db_user}:${db_pwd}@${db_host}:${db_port}/${db_database}`);
const models = require('../models');
const dbObj = {
    db,
    Op
}

for (const modelName in models) {
    if (models.hasOwnProperty(modelName)) {
        const model = models[modelName];
        const tableName = modelName.toLowerCase();
        dbObj[modelName] = db.define(tableName, model)
    }
}


// 同步模型到数据库，并且初始化一些数据
(async () => {
    await db.sync({force:true})
    dbObj.Blog.bulkCreate(
        [
            {
                title: '论EF Core的自我修养1',
                digest: '论EF Core的自我修养1',
                content: '论EF Core的自我修养1',
                classification: '.Net',
                author: 'inCerry',
                publish_time: '2022-04-03 05:45'
            },
            {
                title: '论EF Core的自我修养2',
                digest: '论EF Core的自我修养2',
                content: '论EF Core的自我修养2',
                classification: '.Net',
                author: 'a',
                publish_time: '2022-04-03 08:15'
            },
            {
                title: '论EF Core的自我修养3',
                digest: '论EF Core的自我修养3',
                content: '论EF Core的自我修养3',
                classification: '.Net',
                author: 'b',
                publish_time: '2022-04-03 08:48'
            }
        ]
    );
})()

module.exports = dbObj;