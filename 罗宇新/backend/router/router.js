'use strict'

const fs = require('fs')
const path = require('path')
const router = require('koa-router')()

let file = fs.readdirSync(path.join(__dirname))

let tableFile = file.filter(item=>{
    return item.endsWith('.js') && item!=='router.js'
})

for(let file of tableFile){
    let js = require(path.join(__dirname,file))
    for(let key in js){
        if (key == 'get') {
            for(let i=0 ;i<js[key].length;i++){
                router.get(js[key][i][0],js[key][i][1])
            }
        }
        if (key == 'post') {
            for(let i =0;i<js[key].length;i++){
                router.post(js[key][i][0],js[key][i][1])
            }
        }
    }
}

module.exports = router