'use strict'
const Koa =require('koa');
const routerMiddleware=require('./router');
const bodyParser =require('koa-bodyparser');
const cors =require('koa-cors');

const {sync}=require('./model')
let app =new Koa();
sync();

app.use(cors());
app.use(bodyParser());
app.use(routerMiddleware());

app.listen(8000);
console.log(`服务器：http://localhost:8000`);



