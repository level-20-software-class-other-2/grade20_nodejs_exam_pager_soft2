const Koa=require("koa");
const cors=require("koa-cors");
const bodyparser=require("koa-bodyparser");
const routers=require("./router");


let app =new Koa();

app.use(cors());
app.use(bodyparser());
app.use(routers());


app.listen(8080)

console.log(`请访问:http://localhost:8080`);