'use strict'

$(() => {
    let url=location.search;
    console.log(url.split('='));
    if (url.length>0) {
        let id = (url.split('='))[1];
        $.get(`http://localhost:8080/blogs/${id}`, (data) => {
            $('[name=title]').val(data.title);
            $('[name=abstract]').val(data.abstract);
            $('[name=body]').val(data.body);
            $('[name=classify]').val(data.classify);
            $('[name=auter]').val(data.auter);
        })
    }

})

function btnCancel() {
    window.location.href = './index.html'
}

function btnSave() {
    
    let url=location.search;
    let obj = {
        title: $('[name=title]').val(),
        abstract: $('[name=abstract]').val(),
        body: $('[name=body]').val(),
        classify: $('[name=classify]').val(),
        auter: $('[name=auter]').val()
    }
    if (url.length>0) {
        let id = (url.split('='))[1]
        $.ajax({
            type: "put",
            url: `http://localhost:8080/blogs/${id}`,
            data: obj,
            success: (res) => {
                if (res==='修改成功') {
                    window.location.href = './index.html'
                }else{
                    alert('修改失败')
                }
            }
        })
    } else {
        $.post('http://localhost:8080/blogs', obj, (res) => {
            window.location.href = './index.html'
        })
    }
}

