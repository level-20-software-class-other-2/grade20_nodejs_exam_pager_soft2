'use strict'

const { DataTypes } = require('sequelize')

const model = {
    tit: {
        type: DataTypes.STRING
    },
    abs: {
        type: DataTypes.STRING
    },
    cont: {
        type: DataTypes.STRING
    },
    clas: {
        type: DataTypes.STRING
    },
    writer: {
        type: DataTypes.STRING
    },
    time: {
        type: DataTypes.STRING
    }
}

module.exports=model