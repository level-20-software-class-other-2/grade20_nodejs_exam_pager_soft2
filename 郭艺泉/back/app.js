'use strict';


const Koa = require('koa');
const routerMiddleware = require('./router');
const bodyParser = require('koa-bodyparser');
const cors = require('koa-cors');

const { sync } = require('./model');

let app = new Koa();

sync(); // 调用同步函数

app.use(cors());
app.use(bodyParser());
app.use(routerMiddleware());

let port = 8000;
app.listen(port);
console.log(`服务器运行在以下地址：http://localhost:${port}`);
