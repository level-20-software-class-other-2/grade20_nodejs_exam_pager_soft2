'use strict';

let fn_data;

$(function () {
    query();
})



function query() {
    let keyword = $('#keyword').val();
    getBolgList(keyword).then(data => {
        renderFn(data);
    })

}


function renderFn(arr) {
    let row = $('.rowData');
    let tb = $('#tbData');
    row.remove();
    arr.forEach(item => {
        let html = `
                <tr class="rowData" key="${item.id}">
                    <td>${item.id}</td>
                    <td>${item.tiele}</td>
                    <td>${item.tips}</td>
                    <td>${item.content}</td>
                    <td>${item.type}</td>
                    <td>${item.author}</td>
                    <td>${item.time}</td>
                    <td>${item.supplier}</td>
                    
                    <td>
                        <input type="button" value="编辑" onclick="update(${item.id})" >
                        <input type="button" value="删除" onclick="del(${item.id})" >
                    </td>
                </tr>
                `


        tb.append(html);
    })
}


function update(id) {
    location.href=`./addOrEdit.html?${id}`;
}

function del(id) {
    let delconfirm=confirm(`确定要删除${id}吗？`);
    if(delconfirm) 
    {
        delBolgById(id).then(data =>{
            let id =data.data.id;
            console.log(data);
            $(`[key=${id}]`).remove();
        })
    }
    
}


function add(obj) {
    location.href='./addOrEdit.html';
    postBolg(obj).then(data=>{
        console.log(data);
    });
}



