'use strict'

const router =require('koa-router')();
const fs=require('fs');

function getRouteFiles(filePath) {
    let tmpPath=filePath||__dirname

    let files=fs.readdirSync(tmpPath)

    let routeFiles=files.filter(item =>{
        return item.endsWith('.js') && item != 'index.js'
    })

    return routeFiles;

}
function registryRoute(router,routeFiles) {
    routeFiles.forEach(item => {

        let tmpPath=__dirname+'/'+item

        let obj =require(tmpPath);

        for(const key in obj){

            let tmpArry=key.split(' ');

            let rMethod=tmpArry[0];

            let rPath=tmpArry[1];
            
            let rFun=obj[key]


            if (rMethod=='get') {

                router.get(rPath,rFun)
                
            } else if (rMethod =='post') {



                router.post(rPath,rFun)
                
            } 
        }
    })
    

    return router.routes();
}

module.exports=function () {
    let routeFiles=getRouteFiles();
    let fun=registryRoute(router,routeFiles)

    return fun; 
}
