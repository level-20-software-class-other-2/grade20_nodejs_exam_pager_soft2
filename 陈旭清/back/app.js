const koa=require('koa')
const router=require('koa-router')()
const bodyParser=require('koa-bodyparser')
const cors=require('koa-cors')


let app=new koa()
let blog=[
    {
        id:1,
        title:'论EF Core的自我修养',
        zy:'论EF Core的自我修养',
        body:'论EF Core的自我修养',
        type:'.Net',
        author:InCerry,
        time:'2022-04-06 08:47'
    },
    {
        id:2,
        title:'DDD之我之见',
        zy:'DDD之我之见',
        body:'DDD之我之见',
        type:'编程技术',
        author:某大神,
        time:'2022-04-03 23:47'
    },
    {
        id:3,
        title:'nginx负载平衡的策略',
        zy:'nginx负载平衡的策略',
        body:'nginx负载平衡的策略',
        type:'服务器',
        author:InCerry,
        time:'2022-04-06 08:47'
    },
    {
        id:4,
        title:'linux用户的创建的学习研究',
        zy:'linux用户的创建的学习研究',
        body:'linux用户的创建的学习研究',
        type:'linux',
        author:某大神,
        time:'2022-04-06 08:47'
    },
    {
        id:5,
        title:'大数据仪表盘探讨',
        zy:'大数据仪表盘探讨',
        body:'大数据仪表盘探讨',
        type:'大数据',
        author:'居家博士',
        time:'2022-04-06 08:47'
    }
]
router.get('/blog',(ctx,next)=>{
    ctx.body=blog;
})
router.post('/blog',(ctx,next)=>{
   
        let obj = ctx.request.body;
        if (obj.length = 1 && obj.name !== undefined) {
            obj.id = blog[blog.length - 1].id + 1;
            blog.push(obj);
            ctx.body = {
                code: 1000,
                data: obj,
                msg: '添加成功！'
            }
        } else {
            ctx.body = {
                code: 404,
                data: null,
                msg: '添加内容不能为空！'
            }
        }
    
})
router.put('/blog',(ctx,next)=>{

})
router.delete('/blog/:id',(ctx,next)=>{
    let id=ctx.request.params.id;
    let arr=blog;
    if(arr.length>0){
        return 
    }
    ctx.body={
        code:1000,
        data:'',
        success:data=>{}
    }
})

app.use(cors())
app.use(bodyParser())
app.use(router.routes())

let port=3000;
app.listen(port);
console.log(`服务运行在以下地址：http://localhost:${port}`);