'use strict';

const fs = require('fs');
const { Sequelize,DataTypes,Op } = require('sequelize');
const { db_database,db_username,db_password,db_host,db_dialect } = require('../config/db');
const { time } = require('./Products');

const sequelize = new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
});

let files = fs.readdirSync(__dirname);
let resFiles = files.filter(item=>{
    return item.endsWith('.js')&&item!=='.js'
})

let resObj = {};

resFiles.forEach(item=>{
    let modelName = item.replace('.js','');
    let tmpObj = require(__dirname+'/'+item);
    let tableName = modelName.toLowerCase();
    resObj[modelName] = sequelize.define(tableName,tmpObj);
})

resObj.Op = Op;

resObj.sync=async()=>{
    sequelize.sync({force:true}).then(()=>{
        resObj.Products.bulkCreate([
            {
                title:'论EF Core的自我修养',
                digest:'论EF Core的自我修养',
                content:'论EF Core的自我修养',
                type:'.Net',
                author:'InCerry',
                time:'2022-04-06 08:47'
            },
            {
                title:'DDD之我见',
                digest:'DDD之我见',
                content:'DDD之我见',
                type:'编程技术',
                author:'某大神',
                time:'2022-04-03 23:47'
            },
            {
                title:'nginx负载平衡的几种策略',
                digest:'nginx负载平衡的几种策略',
                content:'nginx负载平衡的几种策略',
                type:'服务器',
                author:'老胡来也',
                time:'2022-04-06 08:47'
            },
            {
                title:'linux用户创建的学习研究',
                digest:'linux用户创建的学习研究',
                content:'linux用户创建的学习研究',
                type:'Linux',
                author:'某大神',
                time:'2022-04-06 08:47'
            },
            {
                title:'大数据仪表盘探讨',
                digest:'大数据仪表盘探讨',
                content:'大数据仪表盘探讨',
                type:'大数据',
                author:'居家博士',
                time:'2022-04-18 16:18'
            },
          
        ])
    })
}

module.exports=resObj;