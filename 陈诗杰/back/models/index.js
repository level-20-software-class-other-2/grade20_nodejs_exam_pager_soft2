'use strict';

const { getJsFiles, getBasename } = require('../utils');

function getModels(modelPath) {
    modelPath = modelPath || __dirname;
    let models = getJsFiles(modelPath).filter(item => item !== __filename);
    let modelNames = getBasename(models).map(item => item.replace('.js',''));
    let allModelsObj = {};

    for (let index = 0; index < models.length; index++) {
        const modelPath = models[index];
        const modelName = modelNames[index];
        allModelsObj[modelName] = require(modelPath);
    }

    return allModelsObj;
}

module.exports = getModels();