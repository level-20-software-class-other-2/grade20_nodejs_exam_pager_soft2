const {Book,Op}= new require('../sequelize/sequelize')

fn_index=async(ctx)=>{
    await Book.findAll({
        order:['id']
    }).then(res=>{
        ctx.body=res
    })
}
fn_data=async(ctx)=>{
let keyword = ctx.request.query.keyword

    if (keyword!=='') {

        if (isNaN(keyword)==false) {

            let data =await Book.findAll({

                where:{
                    [Op.or]:[
                        {id:keyword},
                        {name:keyword},
                        
                        
                    ]
                }
            })
            ctx.body=data
        }else{

            let data=await Book.findAll({
                where:{
                    [Op.or]:[
                        {id:keyword},
                        
                    ]
                }
            })
            ctx.body=data
        }
       
    } else {
        await Book.findAll({
            order:['id']
        }).then(res=>{
            ctx.body=res
        })

    }
}
fn_add=async(ctx)=>{

    let data = ctx.request.body
    await Book.create(data)

    await Book.findAll().then(res=>{
        ctx.body=res
    })

}
fn_Deleted=async(ctx)=>{

    let id = ctx.request.query.id
    await Book.destroy({
        where:{id:id}
    })

    await Book.findAll().then(res=>{
        ctx.body=res
    })
    
}
let get =[['/index',fn_index],['/data',fn_data],['/indexDeleted',fn_Deleted]]
let post =[['/indexadd',fn_add]]

module.exports={
    'GET':get,
    'POST':post
}