"use strict"
const koa = require('koa');
const routerMidd = require('./router');
const bodyParser = require('koa-bodyparser');
const cors = require('koa-cors');
const {sync}=require('./model')
let app =new koa();
sync();
app.use(cors());
app.use(bodyParser());
app.use(routerMidd());

let post =8000;
app.listen(post);

console.log(`服务器为：http://localhost:${post}`);


