'use strict';

const port = 3000;
const Koa = require('koa');
const cors = require('koa-cors');
const bodyparser = require('koa-bodyparser');
const routes = require('./router');
const app = new Koa();

app.use(cors());
app.use(bodyparser());
app.use(routes());

app.listen(port);
console.log(`Server is running at http://localhost:${port}`);