
const fs = require('fs');
const { normalize } = require('path');
const {Sequelize} = require('sequelize')
const {db_database,db_dialect,db_host,db_password,db_username}=require('../config/db')

const sequelize =new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
})


let fiels = fs.readdirSync(__dirname)
let readsync = fiels.filter(item=>{
    return item.endsWith('.js')&&item!=='index.js'
})
let res ={}

readsync.forEach(item=>{
    let modelname=item.replace('.js','');
    let tmpobj = require(__dirname+'/'+item);
    let tablename = modelname.toLowerCase();
    res[modelname]=sequelize.define(tablename,tmpobj)
})

res.sync=async()=>{
    sequelize.sync({force:true}).then(()=>{
         res.Product.bulkCreate([
            {
                id:1,
                headline:'论EF Core的自我修养',
                abstract:'论EF Core的自我修养',
                details:'论EF Core的自我修养',
                classify:'.Net',
                author:'InCerry',
            },
            {
                id:2,
                headline:'DDD之我只见',
                abstract:'DDD之我只见',
                details:'DDD之我只见',
                classify:'无',
                author:'asdf',
                // updata:'asf'
            },
            {
                id:3,
                headline:'nginx',
                abstract:'nginx',
                details:'nginx',
                classify:'无',
                author:'saf',
            },
            {
                id:4,
                headline:'Linux',
                abstract:'Linux',
                details:'Linux',
                classify:'无',
                author:'adf',
            },
        ])
    })
}

module.exports=res