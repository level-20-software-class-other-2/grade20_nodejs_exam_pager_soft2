'use strict'
const {DataTypes} =require('sequelize')

const blog =  {
    title:{//标题
        type:DataTypes.STRING,
        allowNull:false
    },
    abstract:{//摘要
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{//内容
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{//分类
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{//作者
        type:DataTypes.STRING,
        allowNull:false
    },
    postedTime:{//发表时间
        type:DataTypes.STRING,
        allowNull:false
    },
}

module.exports=blog;