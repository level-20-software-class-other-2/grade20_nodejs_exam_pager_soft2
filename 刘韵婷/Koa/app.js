'use strict';

const koa = require('koa');
const router = require('./router');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors');

//const { sync } = require('./model');
//sync();

let app = new koa();

app.use(cors());
app.use(bodyparser());
app.use(router.routes());

let port = 8000;

app.listen(port);

console.log(`运行地址：http://localhost:${port}`);