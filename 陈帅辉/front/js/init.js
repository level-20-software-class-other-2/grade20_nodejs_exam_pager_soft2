'use strict'



let baseurl='http://localhost:8000/product'
$(function(){
    $.get(`${baseurl}`,res=>{
        fn(res)
    })
})

function fn(res){
    $('.data').remove();
    res.forEach(item => {
        let html= `
        <tr class="data" >
            <td>${item.id}</td>
            <td>${item.biaoti}</td>
            <td>${item.zhaiyao}</td>
            <td>${item.neirong}</td>
            <td>${item.fenlei}</td>
            <td>${item.zuozhe}</td>
            <td>${item.time}</td>
            <td><input type="button" value="编辑" onclick="edit(${item.id})"></td>
            <td><input type="button" value="删除" onclick="del(${item.id})"></td>
        </tr>
        `
        $('#tb').append(html)
    });
}
function query(){
    let keyword=$('#keyword').val();
    $.get(`${baseurl}?keyword=${keyword}`,res=>{
        fn(res)
    })
}
function del(id){
    $.ajax({
        url:`${baseurl}/${id}`,
        type:'delete',
        success:res=>{
            location.href='./index.html'
        }
    })
}
function add(){
    location.href='./addOrEdit.html'
}
function edit(id){
    location.href=`./addOrEdit.html?${id}`
}
function save(){
    let id=$('#add_id').val();
    let obj={
        biaoti:$('[name=biaoti]').val(),
        zhaiyao:$('[name=zhaiyao]').val(),
        neirong:$('[name=neirong]').val(),
        fenlei:$('[name=fenlei]').val(),
        zuozhe:$('[name=zuozhe]').val()
    }
    if(id){
        obj.id=$('#add_id').val();
        $.ajax({
            url:`${baseurl}`,
            type:'put',
            data:obj,
            success:res=>{
                location.href='./index.html'
            }
        })
    }else{
        $.post(`${baseurl}`,obj,res=>{
            location.href='./index.html'
        })
    }
}
function cancel(){
    location.href='./index.html'
}

let id=location.href.split('?')[1]
if(id){
    let id=location.href.split('?')[1]

    $.get(`${baseurl}?keyword=${id}`,res=>{
        $('#add_id').val(id)
        $('[name=biaoti]').val(res[0].biaoti),
        $('[name=zhaiyao]').val(res[0].zhaiyao),
        $('[name=neirong]').val(res[0].neirong),
        $('[name=fenlei]').val(res[0].fenlei),
        $('[name=zuozhe]').val(res[0].zuozhe)
    })
}