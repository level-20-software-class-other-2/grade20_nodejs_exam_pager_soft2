const Koa=require('koa');
const router=require('koa-router')();
const bodyParser=require('koa-bodyparser');
const cors=require('koa-cors');
const {Sequelize}=require('sequelize')
const blog=require('./model/Blogs')


let sequelize=new Sequelize('soft2db','postgres','chy123..',{
    host:'chynb.top',
    dialect:'postgres'
});


let Blogs=sequelize.define('blogs',blog);

sequelize.sync({force:true}).then(()=>{
    Blogs.bulkCreate([
        {
            title:'论EF CORE的自我修养',
            zhaiyao:'论EF CORE的自我修养',
            neirong:'论EF CORE的自我修养',
            fenlei:'Net',
            author:'InCerry',
            time:'2022-04-06 08:47'
        },
        {
            title:'DDD之我见',
            zhaiyao:'DDD之我见',
            neirong:'DDD之我见',
            fenlei:'编程技术',
            author:'某大神',
            time:'2022-04-03 23:47'
        },
        {
            title:'ngix负载平衡的策略',
            zhaiyao:'ngix负载平衡的策略',
            neirong:'ngix负载平衡的策略',
            fenlei:'服务器',
            author:'老胡来也',
            time:'2022-04-06 08:47'
        },
        {
            title:'linux用户创建的学习研究',
            zhaiyao:'linux用户创建的学习研究',
            neirong:'linux用户创建的学习研究',
            fenlei:'linux',
            author:'某大神',
            time:'2022-04-06 08:47'
        },
        {
            title:'大数据仪表盘探讨',
            zhaiyao:'大数据仪表盘探讨',
            neirong:'大数据仪表盘探讨',
            fenlei:'大数据',
            author:'居家博士',
            time:'2022-04-18 16:18'
        },
        



    ])
})

router.get('/blog',async(ctx,next)=>{
    let list=await Blogs.findAll();
    ctx.body=list
})
router.get('/blog/:id',(ctx,next)=>{
    let id=ctx.request.params.id
   let arr= await Blogs.findByPk(id)
if (arr) {
    ctx.body={
        code:1000,
        data:arr,
        msg:'获取成功'
  };

  }else{
    ctx.body={
        code:400,
        data:null,
        msg:'失败'
  };
  }
})

router.post('/blog',(ctx,next)=>{
    let obj = ctx.request.body;

    console.log(obj);

    if (obj.hasOwnProperty('title') && obj.hasOwnProperty('zhaiyao') && obj.hasOwnProperty('neirong') && obj.hasOwnProperty('fenlei') && obj.hasOwnProperty('author')) {
       let tmpObj={
           title:obj.title,
           zhaiyao:obj.zhaiyao,
           neirong:obj.neirong,
           fenlei:obj.fenlei,
           author:obj.author
       }
       Blogs.create(tmpObj);
       console.log(tmpObj);
       ctx.body={
        code:1000,
        data:tmpObj,
        msg:'数据成功'
}
    }else{
        ctx.body={
            code:400,
            data:null,
            msg:'数据验证不成功，请重试'
    }
}
})

router.put('/blog/:id',(ctx,next)=>{
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    let blog=await Blogs.findByPk(id)
    
    if (blog) {
        await Blogs.update(obj,
            {where:{
               id:id
            }}
        );

        ctx.body={
            code:1000,
            data:blog,
            msg:'修改成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '指定的商品不存在，请重试！'
        }
    }
})
router.delete('/blog/:id',(ctx,next)=>{
    let id = ctx.request.params.id;
    let blog = await Blogs.findByPk(id);
   
    if (blog) {
        await Blogs.destroy({
            where:{
                id:id
            }
        })
        
        ctx.body = {
            code: 1000,
            data: '',
            msg: '删除成功！'
        }
    } else {
        ctx.body = {
            code: 400,
            data: '',
            msg: '你指定删除的记录有错误,请重试！'
        }
    }
})

let app=new Koa();

app.use(cors());
app.use(bodyParser());
app.use(router.routes())


let port=8000;
app.listen(port);
console.log(`当前网站运行于http://localhost:${port}`);