'use strict'

//路由
const koa=require('koa')
const bodyparser=require('koa-bodyparser');
const cors=require('koa-cors')

const router=require('./router')

let app=new koa();

app.use(cors());
app.use(bodyparser());
app.use(router.routes());

const{sync}=require('./model')
sync();

let port=8000
app.listen(port)

console.log(`运行成功 http://localhost/blog:${port}`);