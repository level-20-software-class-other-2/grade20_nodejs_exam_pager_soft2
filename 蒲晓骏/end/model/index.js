'use strict '

const fs = require('fs')

const { Sequelize, Op } = require('sequelize')

const { db_database, db_dialect, db_host, db_password, db_username } = require('../config/db')

const sequelize = new Sequelize(db_database, db_username, db_password, {
    host: db_host,
    dialect: db_dialect
})
let files = fs.readdirSync(__dirname)

let resFiles = files.filter(item => {
    return item.endsWith('.js') && item !== 'index.js'
})

let resObj = {}

let tmpObj = require('./blog');

resObj.blog = sequelize.define('blog', tmpObj);

resObj.Op = Op;

resObj.sync = async () => {
    sequelize.sync({ force: true }).then(() => {
        resObj.blog.bulkCreate([
            {
                title: '论EF Core的自我修养',
                abstract: '论EF Core的自我修养',
                content: '论EF Core的自我修养',
                classify: '.Net',
                author: 'InCerry',
                postedTime: '2022-04-06 08:47'
            },
            {
                title: 'DDD之我见',
                abstract: 'DDD之我见',
                content: 'DDD之我见',
                classify: '编程技术',
                author: '某大神',
                postedTime: '2022-04-03 23:47'
            },
            {
                title: 'nginx负载平衡的几种策略',
                abstract: 'nginx负载平衡的几种策略',
                content: 'nginx负载平衡的几种策略',
                classify: '服务器',
                author: '老胡来也',
                postedTime: '2022-04-06 08:47'
            },
            {
                title: 'Linux用户创建的学习研究',
                abstract: 'Linux用户创建的学习研究',
                content: 'Linux用户创建的学习研究',
                classify: 'Linux',
                author: '某大神',
                postedTime: '2022-04-06 08:47'
            },
            {
                title: '大数据仪表盘探讨',
                abstract: '大数据仪表盘探讨',
                content: '大数据仪表盘探讨',
                classify: '大数据',
                author: '居家博士',
                postedTime: '2022-04-18 16:18'
            }
    ])

})
}

module.exports = resObj;
