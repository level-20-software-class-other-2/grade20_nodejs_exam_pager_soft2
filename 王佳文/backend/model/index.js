'use strict'


//封装数据库
const{Sequelize,Op}=require('sequelize')

const fs=require('fs')
const { db_database, db_username, db_password, db_host, db_dialect } = require('../config/db')
const model = require('./Blog')

let sequelize=new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
})

let filePath = __dirname;
let Files = fs.readdirSync(filePath)

let resFiles = Files.filter(item => {
    return item.endsWith('.js') && item !== 'idenx.js'
})


let resObj={};

resFiles.forEach(item=>{
    let modelName=item.replace('.js','')
    let tmpobj=require(__dirname+'/'+modelName)
    let tableName=modelName.toLowerCase();
    resObj[modelName]=sequelize.define(tableName,tmpobj)
})

resObj.Op=Op

resObj.sync=async()=>{
    sequelize.sync({force:true}).then(()=>{
        resObj.Blog.bulkCreate([
            {
                title:'论Ef core的自我修养',
                get:'论Ef core的自我修养',
                text:'论Ef core的自我修养',
                type:'.net',
                author:'11111',
                time:'2022-04-06'
            }
        ])
    })
}

module.exports=resObj